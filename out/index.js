"use strict"
var Oa = Object.create
var Le = Object.defineProperty
var Ta = Object.getOwnPropertyDescriptor
var ba = Object.getOwnPropertyNames
var xa = Object.getPrototypeOf,
    Pa = Object.prototype.hasOwnProperty
var _e = (e, t) => {
        for (var r in t) Le(e, r, { get: t[r], enumerable: !0 })
    },
    Ue = (e, t, r, o) => {
        if ((t && typeof t == "object") || typeof t == "function")
            for (let a of ba(t))
                !Pa.call(e, a) &&
                    a !== r &&
                    Le(e, a, {
                        get: () => t[a],
                        enumerable: !(o = Ta(t, a)) || o.enumerable,
                    })
        return e
    },
    b = (e, t, r) => (Ue(e, t, "default"), r && Ue(r, t, "default")),
    y = (e, t, r) => (
        (r = e != null ? Oa(xa(e)) : {}),
        Ue(
            t || !e || !e.__esModule
                ? Le(r, "default", { value: e, enumerable: !0 })
                : r,
            e
        )
    ),
    _a = (e) => Ue(Le({}, "__esModule", { value: !0 }), e)
var eo = {}
_e(eo, { appRouter: () => St })
module.exports = _a(eo)
var ca = y(require("dotenv")),
    da = y(require("path")),
    ma = y(require("cors")),
    Nt = y(require("express")),
    la = y(require("http")),
    pa = y(require("morgan")),
    ha = y(require("ws")),
    ga = y(require("@trpc/server/adapters/express")),
    fa = require("@trpc/server/adapters/ws")
var w = require("zod"),
    vt = "Please enter a valid mail",
    Aa = "First Name must contain at least 3 characters",
    Ia = "Last Name must contain at least 3 characters",
    rt = "Password must be atleast 6 characters",
    Ce = "Wrong Email or Password",
    kt = "Email has already been used",
    zt = "Failed to generate OTP",
    Na = "This field is required",
    at = "Number must be at least 5 Numbers",
    Mt = "Passwords do not match",
    Ut = "Your password has been changed",
    Lt = "Your new password was not saved",
    L = w.z.object({
        email: w.z
            .string()
            .trim()
            .email({ message: vt })
            .refine((e) => e.toLocaleLowerCase()),
        password: w.z.string().min(6, { message: rt }),
        confirmPassword: w.z.string(),
        otp: w.z.string().length(4),
        fullName: w.z.string(),
        phone: w.z.string().min(5, { message: at }).trim(),
        street: w.z
            .string()
            .trim()
            .min(3, "Street field must contain at least 3 letters")
            .transform((e) => e.toLocaleLowerCase()),
        state: w.z
            .string()
            .trim()
            .min(3, "State field must contain at least 3 letters")
            .transform((e) => e.toLocaleLowerCase()),
        city: w.z
            .string()
            .trim()
            .min(3, "City field must contain at least 3 letters")
            .transform((e) => e.toLocaleLowerCase()),
        country: w.z
            .string()
            .trim()
            .min(3, "Country field must contain at least 3 letters")
            .transform((e) => e.toLocaleLowerCase()),
        lat: w.z.number(),
        lng: w.z.number(),
    }),
    me = L.pick({ email: !0 }),
    le = L.pick({ email: !0, password: !0 }),
    pe = L.pick({ email: !0, otp: !0 }),
    ne = L.pick({ email: !0 }).extend({
        password: w.z.string().min(1, { message: Na }),
    }),
    he = L.pick({ email: !0 }),
    ge = L.pick({ email: !0 }),
    je = L.pick({
        city: !0,
        country: !0,
        email: !0,
        fullName: !0,
        password: !0,
        phone: !0,
        state: !0,
        street: !0,
        lng: !0,
        lat: !0,
    }),
    Ye = L.pick({
        city: !0,
        country: !0,
        email: !0,
        fullName: !0,
        phone: !0,
        state: !0,
        street: !0,
        lng: !0,
        lat: !0,
    }),
    ro = L.pick({ password: !0, confirmPassword: !0 }).refine(
        (e) => e.password === e.confirmPassword,
        { path: ["confirmPassword"], message: Mt }
    ),
    ao = L.pick({ phone: !0 }).extend({
        firstName: w.z
            .string()
            .min(3, { message: Aa })
            .trim()
            .transform((e) => e.toLocaleLowerCase()),
        lastName: w.z
            .string()
            .min(3, { message: Ia })
            .trim()
            .transform((e) => e.toLocaleLowerCase()),
    }),
    oo = L.pick({
        street: !0,
        city: !0,
        state: !0,
        country: !0,
        lat: !0,
        lng: !0,
    }),
    so = L.pick({ email: !0, password: !0, confirmPassword: !0 }).refine(
        (e) => e.password === e.confirmPassword,
        { path: ["confirmPassword"], message: Mt }
    ),
    no = w.z.object({
        pin1: w.z.string().length(1),
        pin2: w.z.string().length(1),
        pin3: w.z.string().length(1),
        pin4: w.z.string().length(1),
    }),
    io = L.pick({ email: !0 })
var Be = L.pick({
        city: !0,
        country: !0,
        state: !0,
        street: !0,
        phone: !0,
        lat: !0,
        lng: !0,
    }).extend({
        userFullName: w.z
            .string()
            .min(3, "This field requires at least 3 characters"),
        logo: w.z.string().min(1, "This field is required"),
        name: w.z.string().min(3, "This field requires at least 3 characters"),
        alwaysOpen: w.z.boolean(),
        closingTime: w.z.string().optional(),
        openingTime: w.z.string().optional(),
    }),
    Ct = Be.refine(
        (e) => !(!e.alwaysOpen && (!e.closingTime || e.closingTime.length < 1)),
        { message: "This field is required", path: ["closingTime"] }
    ).refine(
        (e) => !(!e.alwaysOpen && (!e.openingTime || e.openingTime.length < 1)),
        { message: "This field is required", path: ["openingTime"] }
    ),
    jt = ne
        .merge(Be)
        .refine(
            (e) =>
                !(
                    !e.alwaysOpen &&
                    (!e.closingTime || e.closingTime.length < 1)
                ),
            { message: "This field is required", path: ["closingTime"] }
        )
        .refine(
            (e) =>
                !(
                    !e.alwaysOpen &&
                    (!e.openingTime || e.openingTime.length < 1)
                ),
            { message: "This field is required", path: ["openingTime"] }
        ),
    co = Be.extend({
        lat: w.z
            .string()
            .min(1, "This field is required")
            .refine((e) => !Number.isNaN(parseFloat(e)), {
                message: "Please provide a valid latitude value",
                path: ["lat"],
            }),
        lng: w.z
            .string()
            .min(1, "This field is required")
            .refine((e) => !Number.isNaN(parseFloat(e)), {
                message: "Please provide a valid longitude value",
                path: ["lng"],
            }),
    })
        .refine(
            (e) =>
                !(
                    !e.alwaysOpen &&
                    (!e.closingTime || e.closingTime.length < 1)
                ),
            { message: "This field is required", path: ["closingTime"] }
        )
        .refine(
            (e) =>
                !(
                    !e.alwaysOpen &&
                    (!e.openingTime || e.openingTime.length < 1)
                ),
            { message: "This field is required", path: ["openingTime"] }
        ),
    Yt = Be.merge(
        w.z.object({
            email: w.z
                .string()
                .trim()
                .email({ message: vt })
                .refine((e) => e.toLocaleLowerCase()),
        })
    )
        .refine(
            (e) =>
                !(
                    !e.alwaysOpen &&
                    (!e.closingTime || e.closingTime.length < 1)
                ),
            { message: "This field is required", path: ["closingTime"] }
        )
        .refine(
            (e) =>
                !(
                    !e.alwaysOpen &&
                    (!e.openingTime || e.openingTime.length < 1)
                ),
            { message: "This field is required", path: ["openingTime"] }
        )
var C = y(require("zod")),
    Bt = C.object({
        restaurantId: C.string(),
        quantity: C.number(),
        foodId: C.string(),
        basketId: C.string().nullish(),
        basketFoodId: C.string().nullish(),
    }),
    qt = C.object({ restaurantId: C.string() }),
    Vt = C.object({ basketFoodId: C.string(), basketId: C.string() })
var Ae = y(require("zod")),
    $t = Ae.object({
        limit: Ae.number().default(10),
        page: Ae.number().min(1).default(1),
    })
var qe = y(require("zod")),
    Ht = qe.object({ period: qe.enum(["This Year", "Last Year"]) })
var ie = y(require("zod")),
    ot = ie.object({ country: ie.string(), state: ie.string() }),
    Ie = ie.object({ id: ie.string() })
var Ne = y(require("zod")),
    Wt = Ne.object({
        imageType: Ne.enum(["Logo", "Food", "Drink"]).nullish(),
        category: Ne.enum(["Foods", "Logo"]).nullish(),
    })
var A = y(require("zod")),
    Sa = A.object({
        street: A.string()
            .trim()
            .min(3, "Street field must contain at least 3 letters")
            .transform((e) => e.toLocaleLowerCase()),
        state: A.string()
            .trim()
            .min(3, "State field must contain at least 3 letters")
            .transform((e) => e.toLocaleLowerCase()),
        city: A.string()
            .trim()
            .min(3, "City field must contain at least 3 letters")
            .transform((e) => e.toLocaleLowerCase()),
        country: A.string()
            .trim()
            .min(3, "Country field must contain at least 3 letters")
            .transform((e) => e.toLocaleLowerCase()),
        lat: A.number(),
        lng: A.number(),
    }),
    Zt = Sa.pick({ city: !0, country: !0, state: !0, street: !0 }),
    Jt = A.object({
        courierLat: A.number(),
        courierLng: A.number(),
        restaurantLat: A.number(),
        restaurantLng: A.number(),
        customerLat: A.number(),
        customerLng: A.number(),
        deliveryStatus: A.enum(["TO_PICK_UP", "TO_DELIVER"]).optional(),
    })
var E = y(require("zod")),
    Gt = E.object({
        limit: E.number().default(10),
        page: E.number().min(1).default(1),
    }),
    st = E.object({
        image: E.string().min(3, 'image must contain at least 3 characters"'),
        name: E.string().min(3, 'name must contain at least 3 characters"'),
        price: E.number(),
        discountPercentage: E.number(),
        description: E.string().min(
            10,
            'description must contain at least 10 characters"'
        ),
        type: E.enum(["Food", "Logo", "Drink"]),
    }),
    wo = E.object({
        image: E.string().min(3, 'image must contain at least 3 characters"'),
        name: E.string().min(3, 'name must contain at least 3 characters"'),
        price: E.string()
            .or(E.number().min(1))
            .refine((e) => +e != 0, {
                message: "The price must be greater than 0",
            }),
        discountPercentage: E.string().or(E.number()),
        description: E.string().min(
            10,
            'description must contain at least 10 characters"'
        ),
        type: E.enum(["Food", "Logo", "Drink"]),
    }),
    nt = E.object({ id: E.string().nullish() }),
    Kt = st.extend({ id: E.string() }),
    Qt = nt
var R = y(require("zod")),
    Xt = "Your order has been created",
    er = R.enum([
        "New",
        "Cooking",
        "ReadyForPickUp",
        "OnRoute",
        "Delivered",
        "Rejected",
        "Accepted",
    ]),
    tr = R.object({
        limit: R.number().min(5).max(100).default(7),
        cursor: R.string().nullish(),
    }),
    rr = R.object({
        basketId: R.string(),
        restaurantId: R.string(),
        paymentMethod: R.enum(["Card", "Ondelivery"]),
        deliveryFee: R.number(),
        totalPrice: R.number(),
        deliveryLat: R.number(),
        deliveryLng: R.number(),
        pushToken: R.string(),
    }),
    Q = R.object({ id: R.string() }),
    ar = R.object({
        limit: R.number().min(5).max(100).default(10),
        status: er.nullish(),
        period: R.enum([
            "Today",
            "This Week",
            "This Month",
            "This Year",
            "All",
            "Last Year",
        ]),
        page: R.number().default(0),
    }),
    or = R.object({ id: R.string(), status: er }),
    sr = R.object({ state: R.string(), country: R.string() })
var T = y(require("zod"))
var Ve = T.object({
        fullName: T.string(),
        phone: T.string().min(5, { message: at }).trim(),
    }),
    nr = T.object({ id: T.string(), defaultId: T.string() }),
    Se = T.object({
        street: T.string()
            .trim()
            .transform((e) => e.toLocaleLowerCase()),
        state: T.string()
            .trim()
            .transform((e) => e.toLocaleLowerCase()),
        city: T.string()
            .trim()
            .transform((e) => e.toLocaleLowerCase()),
        country: T.string()
            .trim()
            .transform((e) => e.toLocaleLowerCase()),
        lat: T.number(),
        lng: T.number(),
    }),
    ir = Se.extend({ id: T.string() }),
    cr = T.object({ id: T.string() }),
    $e = T.object({ password: T.string().min(6, { message: rt }) }),
    dr = T.object({
        limit: T.number().min(5).max(100).default(7),
        cursor: T.string().nullish(),
    })
var x = y(require("zod")),
    ur = x.object({
        id: x.string(),
        rating: x.number().nullish(),
        limit: x.number().min(5).max(100).default(7),
        cursor: x.string().nullish(),
    }),
    mr = x.object({
        id: x.string().nullish(),
        ratings: x.number(),
        comment: x.string().nullish(),
        restaurantId: x.string(),
    }),
    lr = x.object({ commentId: x.string() }),
    pr = x.object({ commentId: x.string(), restaurantId: x.string() }),
    hr = x.object({
        rating: x.number().nullish(),
        limit: x.number().default(10),
        page: x.number().min(1).default(1),
    })
var gr = y(require("superjson")),
    fr = require("@trpc/server"),
    Da = gr.default,
    Fa = fr.initTRPC.context().create({ transformer: Da }),
    { router: l, middleware: He, mergeRouters: zo, procedure: h } = Fa
var wr = l({
    allImages: h
        .input(Wt)
        .query(({ ctx: e, input: t }) =>
            e.prisma.images.findMany({
                where: {
                    type: t.imageType ?? void 0,
                    category: t.category ?? void 0,
                },
                orderBy: [{ name: "asc" }],
            })
        ),
})
var ct = require("cloudinary"),
    dt = y(require("multer")),
    yr = y(require("path"))
var D = require("zod"),
    va = (e) =>
        Object.entries(e)
            .map(([t, r]) => {
                if (r && "_errors" in r)
                    return `${t}: ${r._errors.join(", ")}
`
            })
            .filter(Boolean),
    ka = D.z.object({
        DATABASE_URL: D.z.string().url(),
        PORT: D.z.string(),
        JWT_SECRET: D.z.string(),
        API_URL: D.z.string().url().optional(),
        MAIL_USER_EMAIL: D.z.string(),
        MAIL_APP_PASSWORD: D.z.string(),
        MAIL_HOST: D.z.string(),
        MAIL_PORT: D.z.string(),
        MAIL_SERVICE: D.z.string(),
        CLOUDINARY_API_KEY: D.z.string(),
        CLOUDINARY_SECRET: D.z.string(),
        CLOUDINARY_CLOUD_NAME: D.z.string(),
        MAP_BOX_TOKEN: D.z.string(),
        EXPO_PUSH_ACCESS_TOKEN: D.z.string(),
    }),
    it = ka.safeParse(process.env)
if (!it.success)
    throw (
        (console.error(
            `Environment Variables missing:
`,
            ...va(it.error.format())
        ),
        new Error("Invalid Environment Variables"))
    )
var P = it.data
var za = dt.default.diskStorage({}),
    Ma = (e, t, r) => {
        let o = /jpg|jpeg|png/,
            a = o.test(yr.default.extname(t.originalname))
        return o.test(t.mimetype) && a
            ? r(null, !0)
            : r({
                  message:
                      "Only images with file ext. jpg, jpeg or png are accepted",
                  name: "Image upload failed",
              })
    },
    Rr = (0, dt.default)({ storage: za, fileFilter: Ma })
ct.v2.config({
    cloud_name: P.CLOUDINARY_CLOUD_NAME,
    api_key: P.CLOUDINARY_API_KEY,
    api_secret: P.CLOUDINARY_SECRET,
})
var Er = async (e, t) => {
    let { type: r } = e.body
    if (e.file && r) {
        let a = (
            await ct.v2.uploader.upload(e.file.path, { folder: `/Mealy/${r}` })
        ).secure_url.split("/image")[1]
        return t.send({ link: `/image${a}` })
    }
    throw new Error("Image upload failed")
}
var We = {}
_e(We, { prisma: () => De })
var H = {}
_e(H, { prisma: () => De })
var Or = require("dotenv"),
    Tr = require("@prisma/client")
b(H, require("@prisma/client"))
;(0, Or.config)({ path: "../../../.env" })
var De =
    global.prisma ||
    new Tr.PrismaClient({
        log:
            process.env.NODE_ENV === "development"
                ? ["query", "error", "warn"]
                : ["error"],
    })
process.env.NODE_ENV !== "production" && (global.prisma = De)
b(We, H)
var ut = y(require("jsonwebtoken"))
var M = (e) => ut.default.sign(e, P.JWT_SECRET),
    br = (e) => ut.default.verify(e, P.JWT_SECRET)
var xr = y(require("events")),
    mt = class {
        constructor() {
            this.ee = new xr.default()
        }
        emit(t, ...r) {
            this.ee.emit(t, ...r)
        }
        on(t, r) {
            this.ee.on(t, r)
        }
        off(t, r) {
            this.ee.off(t, r)
        }
    },
    Pr = mt
var Ua = new Pr(),
    lt = ({ req: e, res: t }) => {
        let r,
            o = () => {
                let i = e.headers.authorization
                if (i && i.startsWith("Bearer"))
                    try {
                        return (r = i.split(" ")), br(r[1]).id
                    } catch {
                        return (e.headers.authorization = ""), null
                    }
                return null
            },
            a,
            s
        return {
            prisma: De,
            req: e,
            res: t,
            restaurant: s,
            ee: Ua,
            user: a,
            userId: o(),
        }
    }
var Ar = require("crypto")
var Ze = y(require("bcrypt")),
    F = async (e) => {
        try {
            let r = await Ze.default.genSalt(10)
            return await Ze.default.hash(e, r)
        } catch {
            throw new Error("Failed to hash password")
        }
    },
    fe = async (e, t) => {
        try {
            return await Ze.default.compare(e, t)
        } catch {
            throw new Error("Failed to compare password")
        }
    }
var v = require("@trpc/server")
var _r = y(require("nodemailer"))
var we = async ({ from: e, to: t, otp: r }) => {
    let o = _r.default.createTransport({
            host: P.MAIL_HOST,
            port: +P.MAIL_PORT,
            secure: !0,
            service: P.MAIL_SERVICE,
            tls: { rejectUnauthorized: !1 },
            auth: { user: P.MAIL_USER_EMAIL, pass: P.MAIL_APP_PASSWORD },
        }),
        a = {
            from: `"${e}" <mealy@group.com>`,
            to: t,
            subject: `${e} Account Recovery`,
            html: `<h3>Hello ${t}</h3>
      <p>Please use this OTP code: ${r} to complete your password reset</p>
      <p>If you are not trying to reset your password, please ignore this mail</P>`,
        }
    await o.sendMail(a)
}
var ye = () => {
        let e = "0123456789",
            t = ""
        for (let r = 0; r < 4; r++) t += e[Math.floor(Math.random() * 10)]
        return t
    },
    Re = () => {
        let e = new Date(),
            t = e
        return t.setTime(e.getTime() + 9e5), t
    },
    Ee = (e) => new Date() > e
var Ir = l({
    changePassword: h.input(le).mutation(async ({ ctx: e, input: t }) => {
        let { email: r } = t,
            { password: o } = t
        o = await F(o)
        try {
            return (
                await e.prisma.courier.update({
                    where: { email: r },
                    data: { courierAuth: { update: { password: o } } },
                }),
                { message: "Your password has been changed" }
            )
        } catch {
            throw new v.TRPCError({
                code: "UNAUTHORIZED",
                message: "Your new password was not saved",
            })
        }
    }),
    confirmOTP: h.input(pe).mutation(async ({ ctx: e, input: t }) => {
        var a
        let { email: r, otp: o } = t
        try {
            let s = await e.prisma.courier.findUnique({
                where: { email: r },
                include: { courierAuth: !0 },
            })
            if (
                !s ||
                !((a = s.courierAuth) != null && a.OTP) ||
                !s.courierAuth.OTPExpires
            )
                throw new v.TRPCError({
                    code: "BAD_REQUEST",
                    message: "OTP Error",
                })
            if (Ee(s.courierAuth.OTPExpires))
                throw new v.TRPCError({
                    code: "BAD_REQUEST",
                    message: "OTP has expired please generate a new one",
                })
            if (!(s.courierAuth.OTP.toString() === o))
                throw new v.TRPCError({
                    code: "BAD_REQUEST",
                    message:
                        "OTP does not match, please check the values provided",
                })
            return !0
        } catch {
            throw new v.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "OTP verification failed",
            })
        }
    }),
    emailExist: h.input(me).mutation(async ({ ctx: e, input: t }) => {
        try {
            let { email: r } = t
            return {
                emailUsed: !!(await e.prisma.courier.findUnique({
                    where: { email: r },
                })),
            }
        } catch {
            throw new v.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to create your account",
            })
        }
    }),
    login: h.input(ne).mutation(async ({ ctx: e, input: t }) => {
        let { email: r, password: o } = t
        try {
            let a = await e.prisma.courier.findUnique({ where: { email: r } })
            if (!a)
                throw new v.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "Wrong username or password",
                })
            let s = await e.prisma.courierAuth.findUnique({
                where: { courierId: a.id },
            })
            if (!s || !s.password)
                throw new v.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "Wrong username or password",
                })
            if (!(await fe(o, s.password)))
                throw new v.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "Wrong username or password",
                })
            let n = M({ id: a.id })
            return { ...a, token: n }
        } catch {
            throw new v.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to login",
            })
        }
    }),
    loginOauth: h.input(he).mutation(async ({ ctx: e, input: t }) => {
        let { email: r } = t,
            { prisma: o } = e
        try {
            let a = await o.courier.findFirst({ where: { email: r } })
            if (!a)
                throw new v.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "Account with that email does not exist",
                })
            let s = M({ id: a.id })
            return { ...a, token: s }
        } catch {
            throw new v.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to login",
            })
        }
    }),
    sendOtp: h.input(ge).mutation(async ({ ctx: e, input: t }) => {
        let { email: r } = t,
            o = await e.prisma.courier.findUnique({ where: { email: r } })
        if (o) {
            let a = ye()
            try {
                await e.prisma.courierAuth.update({
                    where: { courierId: o.id },
                    data: { OTP: a, OTPExpires: Re() },
                }),
                    await we({ from: "Mealy Driver", to: r, otp: a })
            } catch {
                throw new v.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "OTP ERROR",
                })
            }
        }
    }),
    signup: h.input(je).mutation(async ({ ctx: e, input: t }) => {
        let {
                city: r,
                country: o,
                email: a,
                fullName: s,
                phone: i,
                state: n,
                street: c,
                lat: d,
                lng: u,
            } = t,
            { password: m } = t
        try {
            m = await F(m)
            let p = await e.prisma.courier.create({
                    data: {
                        city: r,
                        country: o,
                        email: a,
                        fullName: s,
                        phone: i,
                        state: n,
                        lat: d,
                        lng: u,
                        street: c,
                        courierAuth: { create: { password: m } },
                    },
                }),
                g = M({ id: p.id })
            return { ...p, token: g }
        } catch {
            throw new v.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to create your account",
            })
        }
    }),
    signUpOauth: h.input(Ye).mutation(async ({ ctx: e, input: t }) => {
        let {
                city: r,
                country: o,
                email: a,
                fullName: s,
                phone: i,
                state: n,
                street: c,
                lat: d,
                lng: u,
            } = t,
            { prisma: m } = e
        try {
            let p = (0, Ar.randomBytes)(16),
                g = await F(p.toString("hex")),
                f = await m.courier.create({
                    data: {
                        city: r,
                        country: o,
                        email: a,
                        fullName: s,
                        phone: i,
                        state: n,
                        lat: d,
                        lng: u,
                        street: c,
                        courierAuth: { create: { password: g } },
                    },
                }),
                I = M({ id: f.id })
            return { ...f, token: I }
        } catch {
            throw new v.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to create your account",
            })
        }
    }),
})
var Nr = y(require("axios"))
var Sr = require("url"),
    pt = require("@trpc/server")
var K = require("@trpc/server")
var La = He(async ({ ctx: e, next: t }) => {
        let { prisma: r, userId: o } = e
        if (!o)
            throw new K.TRPCError({
                code: "UNAUTHORIZED",
                message: "Not authorized, please login",
            })
        try {
            let a = await r.user.findFirst({
                where: { id: o },
                include: { addresses: { where: { isDefault: !0 } } },
            })
            if (!a)
                throw new K.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "Not authorized, please login",
                })
            return await t({ ctx: { ...e, user: a } })
        } catch {
            throw new K.TRPCError({
                code: "UNAUTHORIZED",
                message: "Not authorized, please login",
            })
        }
    }),
    Ca = He(async ({ ctx: e, next: t }) => {
        let { prisma: r, userId: o } = e
        if (!o)
            throw new K.TRPCError({
                code: "UNAUTHORIZED",
                message: "Not authorized, please login",
            })
        try {
            let a = await r.restaurant.findFirst({ where: { id: o } })
            if (!a)
                throw new K.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "Not authorized, please login",
                })
            return await t({ ctx: { ...e, restaurant: a } })
        } catch {
            throw new K.TRPCError({
                code: "UNAUTHORIZED",
                message: "Not authorized, please login",
            })
        }
    }),
    ja = He(async ({ ctx: e, next: t }) => {
        let { prisma: r, userId: o } = e
        if (!o)
            throw new K.TRPCError({
                code: "UNAUTHORIZED",
                message: "Not authorized, please login",
            })
        try {
            let a = await r.courier.findFirst({ where: { id: o } })
            if (!a)
                throw new K.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "Not authorized, please login",
                })
            return await t({ ctx: { ...e, courier: a } })
        } catch {
            throw new K.TRPCError({
                code: "UNAUTHORIZED",
                message: "Not authorized, please login",
            })
        }
    }),
    O = h.use(La),
    _ = h.use(Ca),
    j = h.use(ja)
var { MAP_BOX_TOKEN: Ya } = P,
    Dr = l({
        getRoute: j.input(Jt).query(async ({ input: e }) => {
            var m, p, g
            let {
                    courierLat: t,
                    courierLng: r,
                    customerLat: o,
                    customerLng: a,
                    restaurantLat: s,
                    restaurantLng: i,
                    deliveryStatus: n,
                } = e,
                c = new Sr.URLSearchParams({
                    geometries: "geojson",
                    access_token: Ya,
                }).toString(),
                d = "https://api.mapbox.com/directions/v5/mapbox/driving/",
                u = `${d}${r},${t};${i},${s};${a},${o}`
            n === "TO_DELIVER" && (u = `${d}${r},${t};${a},${o}`),
                n === "TO_PICK_UP" && (u = `${d}${r},${t};${i},${s}`)
            try {
                let { data: f } = await Nr.default.get(`${u}?${c}`)
                if (
                    (g =
                        (p =
                            (m = f == null ? void 0 : f.routes) == null
                                ? void 0
                                : m[0].geometry) == null
                            ? void 0
                            : p.coordinates) != null &&
                    g[0]
                )
                    return { route: f.routes[0].geometry }
                throw new pt.TRPCError({
                    code: "NOT_FOUND",
                    message: "Failed to find a route for your delivery",
                })
            } catch {
                throw new pt.TRPCError({
                    code: "NOT_FOUND",
                    message: "Failed to find a route for your delivery",
                })
            }
        }),
    })
var Fe = require("@trpc/server"),
    vr = require("@trpc/server/observable")
var ht = require("expo-server-sdk")
var { EXPO_PUSH_ACCESS_TOKEN: Ba } = P,
    Fr = new ht.Expo({ accessToken: Ba }),
    ee = async ({ pushToken: e, body: t, title: r, orderId: o }) => {
        if (ht.Expo.isExpoPushToken(e)) {
            let a = [
                    {
                        to: e,
                        sound: "default",
                        body: t,
                        title: r,
                        data: { url: `mealy://food/orders/${o}` },
                    },
                ],
                s = Fr.chunkPushNotifications(a)
            try {
                await Fr.sendPushNotificationsAsync(s[0])
            } catch (i) {
                console.error(i)
            }
        }
    }
var kr = l({
    acceptOrder: j.input(Q).mutation(async ({ ctx: e, input: t }) => {
        let { courier: r, prisma: o } = e,
            { id: a } = t
        try {
            if (
                await o.order.findFirst({
                    where: { id: a, status: "ReadyForPickUp", courierId: null },
                    include: { restaurant: { select: { name: !0 } } },
                })
            )
                return (
                    await o.order.update({
                        where: { id: a },
                        data: { courierId: r.id },
                    }),
                    { message: "You have accepted to deliver this order" }
                )
            throw new Fe.TRPCError({
                code: "NOT_FOUND",
                message:
                    "This order does not exist or has been picked by another courier",
            })
        } catch {
            throw new Fe.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to accept the order",
            })
        }
    }),
    deliverOrder: j.input(Q).mutation(async ({ ctx: e, input: t }) => {
        let { prisma: r } = e,
            { id: o } = t
        try {
            let a = await r.order.update({
                where: { id: o },
                data: { status: "Delivered" },
                include: { restaurant: { select: { name: !0 } } },
            })
            return (
                await ee({
                    pushToken: a.pushToken ?? "",
                    orderId: a.id,
                    title: a.restaurant.name,
                    body: "Your order has been delivered",
                }),
                { message: "You have delivered this order" }
            )
        } catch {
            throw new Fe.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to update the order",
            })
        }
    }),
    getAllOrders: j.query(async ({ ctx: e }) => {
        let { courier: t, prisma: r } = e,
            { country: o, state: a } = t
        return await r.restaurant.findMany({
            where: {
                state: a,
                country: o,
                orders: { some: { status: "ReadyForPickUp", courierId: null } },
            },
            include: {
                _count: {
                    select: {
                        orders: {
                            where: {
                                status: "ReadyForPickUp",
                                courierId: null,
                            },
                        },
                    },
                },
                orders: {
                    where: { status: "ReadyForPickUp", courierId: null },
                    select: { deliveryAddress: !0, deliveryFee: !0, id: !0 },
                },
            },
        })
    }),
    getOrderDetails: j.input(Q).query(async ({ ctx: e, input: t }) => {
        let { prisma: r, courier: o } = e,
            { id: a } = t
        return await r.order.findFirst({
            where: { id: a, OR: [{ courierId: null }, { courierId: o.id }] },
            include: {
                restaurant: {
                    select: {
                        city: !0,
                        state: !0,
                        country: !0,
                        street: !0,
                        name: !0,
                        lat: !0,
                        lng: !0,
                        phone: !0,
                    },
                },
                foodOrdered: {
                    select: { quantity: !0, food: { select: { name: !0 } } },
                },
                user: { select: { fullName: !0, phone: !0 } },
            },
        })
    }),
    pickUpOrder: j.input(Q).mutation(async ({ ctx: e, input: t }) => {
        let { prisma: r } = e,
            { id: o } = t
        try {
            let a = await r.order.update({
                where: { id: o },
                data: { status: "OnRoute" },
                include: { restaurant: { select: { name: !0 } } },
            })
            return (
                await ee({
                    pushToken: a.pushToken ?? "",
                    orderId: a.id,
                    title: a.restaurant.name,
                    body: "Your order has been picked up by a courier",
                }),
                { message: "Order status has successfully been update" }
            )
        } catch {
            throw new Fe.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to update the order status",
            })
        }
    }),
    newPickUp: h.input(sr).subscription(({ ctx: e, input: t }) => {
        let { ee: r } = e,
            { country: o, state: a } = t
        return (0, vr.observable)((s) => {
            let i = (n, c) => {
                n === a &&
                    c === o &&
                    s.next({ message: "You have a new pick up order" })
            }
            return (
                r.on("ORDER_READY", i),
                () => {
                    r.off("ORDER_READY", i)
                }
            )
        })
    }),
})
var Oe = require("@trpc/server")
var zr = l({
    allDeliveries: j.input(dr).query(async ({ ctx: e, input: t }) => {
        let { limit: r, cursor: o } = t,
            { prisma: a, courier: s } = e
        try {
            let i = await a.order.findMany({
                    where: { courierId: s.id, status: "Delivered" },
                    take: r + 1,
                    cursor: o ? { id: o } : void 0,
                    select: {
                        id: !0,
                        deliveryFee: !0,
                        deliveryAddress: !0,
                        restaurant: {
                            select: {
                                name: !0,
                                logo: !0,
                                state: !0,
                                street: !0,
                                country: !0,
                                city: !0,
                            },
                        },
                    },
                    orderBy: [{ createdAt: "desc" }],
                }),
                n
            return (
                i.length > r && (n = i.pop().id),
                { deliveries: i, nextCursor: n }
            )
        } catch {
            throw new Oe.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to get all your orders",
            })
        }
    }),
    changePassword: j.input($e).mutation(async ({ ctx: e, input: t }) => {
        let { password: r } = t,
            { courier: o, prisma: a } = e
        r = await F(r)
        try {
            return (
                await a.courierAuth.update({
                    where: { courierId: o.id },
                    data: { password: r },
                }),
                { message: "Your password has been change successfully" }
            )
        } catch {
            throw new Oe.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to change your password",
            })
        }
    }),
    deleteAccount: j.mutation(async ({ ctx: e }) => {
        let { courier: t, prisma: r } = e
        try {
            return (
                await r.courier.delete({ where: { id: t.id } }),
                { message: "Your account has successfully been deleted" }
            )
        } catch {
            throw new Oe.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to delete your account",
            })
        }
    }),
    editAddress: j.input(Se).mutation(async ({ ctx: e, input: t }) => {
        let { courier: r, prisma: o } = e,
            { city: a, country: s, lat: i, lng: n, state: c, street: d } = t
        try {
            return (
                await o.courier.update({
                    where: { id: r.id },
                    data: {
                        city: a,
                        country: s,
                        lat: i,
                        lng: n,
                        state: c,
                        street: d,
                    },
                }),
                { message: "Your address details were updated" }
            )
        } catch {
            throw new Oe.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to update your details",
            })
        }
    }),
    editProfile: j.input(Ve).mutation(async ({ ctx: e, input: t }) => {
        let { courier: r, prisma: o } = e,
            { fullName: a, phone: s } = t
        try {
            return (
                await o.courier.update({
                    where: { id: r.id },
                    data: { fullName: a, phone: s },
                }),
                { message: "Your account has successfully been updated" }
            )
        } catch {
            throw new Oe.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to update your account",
            })
        }
    }),
})
var Mr = l({ auth: Ir, location: Dr, orders: kr, profile: zr })
var Ur = require("crypto")
var Y = require("@trpc/server")
var Lr = l({
    changePassword: h.input(le).mutation(async ({ ctx: e, input: t }) => {
        let { email: r } = t,
            { password: o } = t
        o = await F(o)
        try {
            return (
                await e.prisma.user.update({
                    where: { email: r },
                    data: {
                        userAuth: {
                            update: {
                                password: o,
                                OTP: null,
                                OTPExpires: null,
                            },
                        },
                    },
                }),
                { message: Ut }
            )
        } catch {
            throw new Y.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: Lt,
            })
        }
    }),
    confirmOTP: h.input(pe).mutation(async ({ ctx: e, input: t }) => {
        var n
        let { email: r, otp: o } = t,
            a = await e.prisma.user.findUnique({
                where: { email: r },
                include: { userAuth: !0 },
            })
        if (
            !a ||
            !((n = a.userAuth) != null && n.OTP) ||
            !a.userAuth.OTPExpires
        )
            throw new Y.TRPCError({ code: "BAD_REQUEST", message: "OTP Error" })
        if (Ee(a.userAuth.OTPExpires))
            throw new Y.TRPCError({
                code: "UNAUTHORIZED",
                message: "OTP has expired, please generate a new one",
            })
        if (!(a.userAuth.OTP.toString() === o))
            throw new Y.TRPCError({
                code: "UNAUTHORIZED",
                message: "OTP does not match, please check the values provided",
            })
        return !0
    }),
    emailExist: h.input(me).mutation(async ({ ctx: e, input: t }) => {
        if (await e.prisma.user.findUnique({ where: { email: t.email } }))
            throw new Y.TRPCError({ code: "UNAUTHORIZED", message: kt })
        return { emailUsed: !1 }
    }),
    login: h.input(ne).mutation(async ({ input: e, ctx: t }) => {
        let r = await t.prisma.user.findFirst({
            where: { email: e.email },
            include: { addresses: !0 },
        })
        if (!r) throw new Y.TRPCError({ code: "UNAUTHORIZED", message: Ce })
        let o = await t.prisma.userAuth.findUnique({ where: { userId: r.id } })
        if (!o || !o.password)
            throw new Y.TRPCError({ code: "NOT_FOUND", message: Ce })
        if (!(await fe(e.password, o.password)))
            throw new Y.TRPCError({ code: "NOT_FOUND", message: Ce })
        let s = M({ id: r.id })
        return { ...r, token: s }
    }),
    loginOauth: h.input(he).mutation(async ({ ctx: e, input: t }) => {
        let { email: r } = t,
            { prisma: o } = e,
            a = await o.user.findFirst({
                where: { email: r },
                include: { addresses: !0 },
            })
        if (!a)
            throw new Y.TRPCError({
                code: "NOT_FOUND",
                message: "Account with that email does not exist",
            })
        let s = M({ id: a.id })
        return { ...a, token: s }
    }),
    sendOTP: h.input(ge).mutation(async ({ ctx: e, input: t }) => {
        let { email: r } = t,
            o = await e.prisma.user.findUnique({ where: { email: r } })
        if (o) {
            let a = ye()
            try {
                await e.prisma.userAuth.update({
                    where: { userId: o.id },
                    data: { OTP: a, OTPExpires: Re() },
                }),
                    await we({ from: "Mealy Food", to: o.email, otp: a })
            } catch {
                throw new Y.TRPCError({ code: "BAD_REQUEST", message: zt })
            }
        }
    }),
    signUp: h.input(je).mutation(async ({ input: e, ctx: t }) => {
        let { password: r } = e,
            {
                city: o,
                country: a,
                email: s,
                fullName: i,
                phone: n,
                state: c,
                street: d,
                lat: u,
                lng: m,
            } = e
        try {
            r = await F(r)
            let p = await t.prisma.user.create({
                    data: {
                        email: s,
                        fullName: i,
                        phone: n,
                        userAuth: { create: { password: r } },
                        addresses: {
                            create: {
                                city: o,
                                country: a,
                                state: c,
                                street: d,
                                isDefault: !0,
                                lat: u,
                                lng: m,
                            },
                        },
                    },
                    include: { addresses: !0 },
                }),
                g = M({ id: p.id })
            return { ...p, token: g }
        } catch {
            throw new Y.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to create your account",
            })
        }
    }),
    signUpOauth: h.input(Ye).mutation(async ({ ctx: e, input: t }) => {
        let {
            city: r,
            country: o,
            email: a,
            fullName: s,
            phone: i,
            state: n,
            street: c,
            lat: d,
            lng: u,
        } = t
        try {
            let m = (0, Ur.randomBytes)(16),
                p = await F(m.toString("hex")),
                g = await e.prisma.user.create({
                    data: {
                        email: a,
                        fullName: s,
                        phone: i,
                        userAuth: { create: { password: p } },
                        addresses: {
                            create: {
                                city: r,
                                country: o,
                                state: n,
                                street: c,
                                isDefault: !0,
                                lat: d,
                                lng: u,
                            },
                        },
                    },
                    include: { addresses: !0 },
                })
            if (!g)
                throw new Y.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to create your account",
                })
            let f = M({ id: g.id })
            return { ...g, token: f }
        } catch {
            throw new Y.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to create your account",
            })
        }
    }),
})
var gt = require("@trpc/server")
var Cr = l({
    addToBasket: O.input(Bt).mutation(async ({ ctx: e, input: t }) => {
        let { prisma: r, user: o } = e,
            {
                quantity: a,
                restaurantId: s,
                foodId: i,
                basketId: n,
                basketFoodId: c,
            } = t
        try {
            return (
                await r.basket.upsert({
                    where: { id: n ?? "0" },
                    create: {
                        restaurantId: s,
                        userId: o.id,
                        basketFood: {
                            create: {
                                quantity: a,
                                food: { connect: { id: i } },
                            },
                        },
                    },
                    update: {
                        basketFood: {
                            upsert: {
                                where: { id: c ?? "0" },
                                create: {
                                    quantity: a,
                                    food: { connect: { id: i } },
                                },
                                update: {
                                    quantity: a,
                                    food: { connect: { id: i } },
                                },
                            },
                        },
                    },
                }),
                { message: "Food Successfully added to basket" }
            )
        } catch {
            throw new gt.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to add food item to basket",
            })
        }
    }),
    getBasket: O.input(qt).query(async ({ ctx: e, input: t }) => {
        let { restaurantId: r } = t,
            { prisma: o, user: a } = e
        return await o.basket.findFirst({
            where: { restaurantId: r, userId: a.id },
            include: {
                basketFood: {
                    include: {
                        food: {
                            select: {
                                id: !0,
                                name: !0,
                                price: !0,
                                discountPrice: !0,
                                image: !0,
                            },
                        },
                    },
                    orderBy: [{ createdAt: "asc" }],
                },
            },
        })
    }),
    removeFromBasket: O.input(Vt).mutation(async ({ ctx: e, input: t }) => {
        let { basketFoodId: r, basketId: o } = t,
            { prisma: a } = e
        try {
            return (
                (await a.basketFood.findMany({ where: { basketId: o } }))
                    .length > 1
                    ? await a.basketFood.delete({ where: { id: r } })
                    : await a.basket.delete({ where: { id: o } }),
                { message: "Food Successfully removed from basket" }
            )
        } catch {
            throw new gt.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to remove food from basket",
            })
        }
    }),
})
var te = require("@trpc/server")
var jr = l({
    addComment: O.input(mr).mutation(async ({ ctx: e, input: t }) => {
        let { ratings: r, comment: o, id: a, restaurantId: s } = t,
            { prisma: i, user: n } = e
        try {
            return await i.$transaction(async (d) => {
                var p
                await d.restaurantRatings.upsert({
                    where: { id: a ?? "" },
                    create: {
                        ratings: r,
                        comment: o,
                        restaurantId: s,
                        userId: n.id,
                    },
                    update: { ratings: r, comment: o },
                })
                let m =
                    (p = (
                        await d.restaurantRatings.groupBy({
                            where: { restaurantId: s },
                            by: ["restaurantId"],
                            _avg: { ratings: !0 },
                        })
                    )[0]._avg.ratings) == null
                        ? void 0
                        : p.toFixed(1)
                return (
                    m ? (m = +m) : (m = 0),
                    await d.restaurant.update({
                        where: { id: s },
                        data: { ratings: m },
                    }),
                    { message: "Your ratings been updated" }
                )
            })
        } catch {
            throw new te.TRPCError({
                code: "NOT_FOUND",
                message: "Failed to add your rating",
            })
        }
    }),
    allRestaurants: O.input(ot).query(async ({ ctx: e, input: t }) => {
        let { prisma: r } = e,
            { state: o, country: a } = t
        try {
            return await r.restaurant.findMany({
                where: { country: a, state: o },
            })
        } catch {
            throw new te.TRPCError({
                code: "NOT_FOUND",
                message: "Failed to get all Restaurants",
            })
        }
    }),
    deleteComment: O.input(pr).mutation(async ({ ctx: e, input: t }) => {
        let { commentId: r, restaurantId: o } = t
        try {
            return await e.prisma.$transaction(async (s) => {
                var c
                await s.restaurantRatings.delete({ where: { id: r } })
                let n =
                    (c = (
                        await s.restaurantRatings.groupBy({
                            where: { restaurantId: o },
                            by: ["restaurantId"],
                            _avg: { ratings: !0 },
                        })
                    )[0]._avg.ratings) == null
                        ? void 0
                        : c.toFixed(1)
                return (
                    n ? (n = +n) : (n = 0),
                    await s.restaurant.update({
                        where: { id: o },
                        data: { ratings: n },
                    }),
                    { message: "Your comment has been deleted successfully" }
                )
            })
        } catch {
            throw new te.TRPCError({
                code: "NOT_FOUND",
                message: "Failed to delete your comment",
            })
        }
    }),
    getComment: O.input(lr).query(
        async ({ ctx: e, input: t }) =>
            await e.prisma.restaurantRatings.findFirst({
                where: { id: t.commentId },
            })
    ),
    getRestaurantRatings: O.input(Ie).query(async ({ ctx: e, input: t }) => {
        let { id: r } = t,
            { prisma: o, user: a } = e
        try {
            let s = await o.restaurant.findFirst({
                    where: { id: r },
                    select: {
                        ratings: !0,
                        _count: { select: { userRatings: !0 } },
                    },
                }),
                i = await o.restaurantRatings.groupBy({
                    by: ["ratings"],
                    where: { restaurantId: r },
                    _count: { ratings: !0 },
                    orderBy: { ratings: "desc" },
                }),
                n = await o.restaurantRatings.findFirst({
                    where: { restaurantId: r, userId: a.id },
                    select: { id: !0 },
                })
            return { RestaurantRatings: s, ratingsAndCount: i, userComment: n }
        } catch {
            throw new te.TRPCError({
                code: "NOT_FOUND",
                message: "Failed to get all Restaurant Ratings",
            })
        }
    }),
    getUsersComments: O.input(ur).query(async ({ ctx: e, input: t }) => {
        let { prisma: r } = e,
            { id: o, limit: a, cursor: s, rating: i } = t
        try {
            let n = await r.restaurantRatings.findMany({
                    where: { restaurantId: o, ratings: i ?? void 0 },
                    take: a + 1,
                    cursor: s ? { id: s } : void 0,
                    orderBy: [{ updatedAt: "desc" }],
                    include: { user: { select: { fullName: !0 } } },
                }),
                c
            return (
                n.length > a && (c = n.pop().id), { comments: n, nextCursor: c }
            )
        } catch {
            throw new te.TRPCError({
                code: "NOT_FOUND",
                message: "Failed to get all Restaurant Comments",
            })
        }
    }),
    restaurantDetails: O.input(Ie).query(async ({ ctx: e, input: t }) => {
        let { id: r } = t
        try {
            return await e.prisma.restaurant.findFirst({
                where: { id: r },
                include: { foods: !0 },
            })
        } catch {
            throw new te.TRPCError({
                code: "NOT_FOUND",
                message: "Failed to get all restaurant details",
            })
        }
    }),
    specialOffers: O.input(ot).query(async ({ ctx: e, input: t }) => {
        let { prisma: r } = e,
            { country: o, state: a } = t
        try {
            return await r.food.findMany({
                where: {
                    discountPercentage: { gt: 0 },
                    restaurant: { country: o, state: a },
                },
                orderBy: [
                    { discountPercentage: "desc" },
                    { discountPrice: "desc" },
                ],
                take: 6,
                include: { restaurant: !0 },
            })
        } catch {
            throw new te.TRPCError({
                code: "NOT_FOUND",
                message: "Failed to find all special offers",
            })
        }
    }),
})
var re = require("@trpc/server")
var Yr = l({
    allOrders: O.input(tr).query(async ({ ctx: e, input: t }) => {
        let { limit: r, cursor: o } = t,
            { prisma: a, user: s } = e
        try {
            let i = await a.order.findMany({
                    where: { userId: s.id },
                    take: r + 1,
                    cursor: o ? { id: o } : void 0,
                    select: {
                        id: !0,
                        status: !0,
                        createdAt: !0,
                        restaurant: !0,
                        totalPriceWithDelivery: !0,
                    },
                    orderBy: [{ createdAt: "desc" }],
                }),
                n
            return (
                i.length > r && (n = i.pop().id), { orders: i, nextCursor: n }
            )
        } catch {
            throw new re.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to get all your orders",
            })
        }
    }),
    createOrder: O.input(rr).mutation(async ({ ctx: e, input: t }) => {
        let {
                restaurantId: r,
                basketId: o,
                deliveryFee: a,
                totalPrice: s,
                paymentMethod: i,
                deliveryLat: n,
                deliveryLng: c,
                pushToken: d,
            } = t,
            { prisma: u, user: m, ee: p } = e,
            g = await u.addresses.findFirst({
                where: { userId: m.id, isDefault: !0 },
            })
        if (!g)
            throw new re.TRPCError({
                code: "NOT_FOUND",
                message: "Your address was not found, please select an address",
            })
        let f = await u.restaurant.findFirst({ where: { id: r } })
        if (!f)
            throw new re.TRPCError({
                code: "NOT_FOUND",
                message: "The restaurant you are ordering from does not exist",
            })
        let I = !1
        if (f.alwaysOpen) I = !0
        else if (f.closingTime && f.openingTime) {
            let se = new Date().getHours(),
                ue = parseInt(f.closingTime.split(":")[0], 10) + 12
            I = parseInt(f.openingTime.split(":")[0], 10) <= se && se < ue
        } else I = !1
        if (!I)
            throw new re.TRPCError({
                code: "FORBIDDEN",
                message:
                    "This restaurant is not open please try again during their open hours",
            })
        let { city: V, country: J, state: S, street: z } = g
        if (!(g.state === f.state && g.country === f.country))
            throw new re.TRPCError({
                code: "FORBIDDEN",
                message:
                    "This restaurant does not operate in the address you provided",
            })
        let q = await u.basket.findFirst({
            where: { id: o },
            include: { basketFood: { include: { food: !0 } } },
        })
        if (!q)
            throw new re.TRPCError({
                code: "NOT_FOUND",
                message: "Your basket details was not found",
            })
        let Pe = u.order.create({
                data: {
                    restaurantId: r,
                    deliveryFee: a,
                    deliveryLat: n,
                    deliveryLng: c,
                    totalPriceWithDelivery: s,
                    totalPrice: s - a,
                    isPaid: !0,
                    pushToken: d,
                    deliveryAddress: `${z}, ${V}, ${S}, ${J}`,
                    payment: i,
                    userId: m.id,
                    foodOrdered: {
                        createMany: {
                            data: q.basketFood.map((se) => {
                                let { quantity: ue, foodId: Dt, food: ya } = se,
                                    {
                                        price: Ft,
                                        discountPrice: tt,
                                        discountPercentage: Ra,
                                    } = ya,
                                    Ea = tt > 0 ? tt : Ft
                                return {
                                    quantity: ue,
                                    foodId: Dt,
                                    orderDiscountPrice: tt,
                                    orderPrice: Ft,
                                    orderDiscountPercentage: Ra,
                                    foodTotalPrice: ue * Ea,
                                }
                            }),
                        },
                    },
                },
            }),
            G = u.basket.delete({ where: { id: o } }),
            $ = await u.$transaction([Pe, G])
        return p.emit("ORDER_CREATED", f.id, $[0].id), { message: Xt }
    }),
    orderDetails: O.input(Q).query(async ({ ctx: e, input: t }) => {
        let { id: r } = t,
            { prisma: o, userId: a } = e
        try {
            return await o.order.findFirst({
                where: { id: r, userId: a },
                include: {
                    foodOrdered: {
                        include: { food: { select: { name: !0 } } },
                    },
                    courier: { select: { fullName: !0 } },
                    user: { select: { fullName: !0, phone: !0 } },
                    restaurant: !0,
                },
            })
        } catch {
            throw new re.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to get the order details",
            })
        }
    }),
})
var X = require("@trpc/server")
var Br = l({
    addAddress: O.input(Se).mutation(async ({ ctx: e, input: t }) => {
        let { city: r, country: o, state: a, street: s, lat: i, lng: n } = t,
            { prisma: c, user: d } = e
        try {
            return {
                message: "Your new address has been saved",
                address: await c.addresses.create({
                    data: {
                        city: r,
                        country: o,
                        state: a,
                        street: s,
                        lat: i,
                        lng: n,
                        user: { connect: { id: d.id } },
                    },
                }),
            }
        } catch {
            throw new X.TRPCError({
                message: "Failed to add a new address",
                code: "INTERNAL_SERVER_ERROR",
            })
        }
    }),
    changePassword: O.input($e).mutation(async ({ ctx: e, input: t }) => {
        let { password: r } = t,
            { prisma: o, user: a } = e
        r = await F(r)
        try {
            return (
                await o.userAuth.update({
                    where: { userId: a.id },
                    data: { password: r },
                }),
                { message: "Your password has been changed successfully" }
            )
        } catch {
            throw new X.TRPCError({
                message: "Failed to update your password",
                code: "INTERNAL_SERVER_ERROR",
            })
        }
    }),
    defaultAddress: O.input(nr).mutation(async ({ ctx: e, input: t }) => {
        let { id: r, defaultId: o } = t,
            { prisma: a } = e
        try {
            return (
                await a.$transaction([
                    a.addresses.update({
                        where: { id: o },
                        data: { isDefault: !1 },
                    }),
                    a.addresses.update({
                        where: { id: r },
                        data: { isDefault: !0 },
                    }),
                ]),
                { message: "Your default location has been updated" }
            )
        } catch {
            throw new X.TRPCError({
                message: "Failed to update your default address",
                code: "INTERNAL_SERVER_ERROR",
            })
        }
    }),
    deleteAccount: O.mutation(async ({ ctx: e }) => {
        let { prisma: t, user: r } = e
        try {
            return (
                await t.user.delete({ where: { id: r.id } }),
                { message: "Your account has been deleted successfully" }
            )
        } catch {
            throw new X.TRPCError({
                message: "Failed to delete your account",
                code: "INTERNAL_SERVER_ERROR",
            })
        }
    }),
    deleteAddress: O.input(cr).mutation(async ({ ctx: e, input: t }) => {
        let { id: r } = t,
            { prisma: o } = e
        try {
            let a = await o.addresses.findFirst({ where: { id: r } })
            if (a != null && a.isDefault)
                throw new X.TRPCError({
                    message: "You cannot delete your default address",
                    code: "FORBIDDEN",
                })
            return (
                await o.addresses.delete({ where: { id: r } }),
                { message: "Your address has been deleted" }
            )
        } catch {
            throw new X.TRPCError({
                message: "Failed to delete your address",
                code: "INTERNAL_SERVER_ERROR",
            })
        }
    }),
    editAddress: O.input(ir).mutation(async ({ ctx: e, input: t }) => {
        let {
                city: r,
                country: o,
                id: a,
                state: s,
                street: i,
                lat: n,
                lng: c,
            } = t,
            { prisma: d } = e
        try {
            return {
                message: "Your address has successfully been updated",
                address: await d.addresses.update({
                    where: { id: a },
                    data: {
                        city: r,
                        country: o,
                        state: s,
                        street: i,
                        lat: n,
                        lng: c,
                    },
                }),
            }
        } catch {
            throw new X.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to update your address",
            })
        }
    }),
    editProfile: O.input(Ve).mutation(async ({ ctx: e, input: t }) => {
        let { fullName: r, phone: o } = t
        try {
            return (
                await e.prisma.user.update({
                    where: { id: e.user.id },
                    data: { fullName: r, phone: o },
                }),
                { message: "Your profile has been updated successfully" }
            )
        } catch {
            throw new X.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to update your profile",
            })
        }
    }),
})
var qr = l({ auth: Lr, basket: Cr, main: jr, order: Yr, profile: Br })
var Vr = require("crypto")
var k = require("@trpc/server")
var $r = l({
    changePassword: h.input(le).mutation(async ({ ctx: e, input: t }) => {
        let { email: r } = t,
            { password: o } = t
        o = await F(o)
        try {
            return (
                await e.prisma.restaurant.update({
                    where: { email: r },
                    data: {
                        restaurantAuth: {
                            update: {
                                password: o,
                                OTP: null,
                                OTPExpires: null,
                            },
                        },
                    },
                }),
                { message: "Your password has been changed" }
            )
        } catch {
            throw new k.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Your new password was not saved",
            })
        }
    }),
    confirmOTP: h.input(pe).mutation(async ({ ctx: e, input: t }) => {
        var a
        let { email: r, otp: o } = t
        try {
            let s = await e.prisma.restaurant.findUnique({
                where: { email: r },
                include: { restaurantAuth: !0 },
            })
            if (
                !s ||
                !((a = s.restaurantAuth) != null && a.OTP) ||
                !s.restaurantAuth.OTPExpires
            )
                throw new k.TRPCError({
                    code: "BAD_REQUEST",
                    message: "OTP Error",
                })
            if (Ee(s.restaurantAuth.OTPExpires))
                throw new k.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "OTP has expired, please generate a new one",
                })
            if (!(s.restaurantAuth.OTP.toString() === o))
                throw new k.TRPCError({
                    code: "UNAUTHORIZED",
                    message:
                        "OTP does not match, please check the values provided",
                })
            return !0
        } catch {
            throw new k.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "OTP error",
            })
        }
    }),
    emailExist: h.input(me).mutation(async ({ ctx: e, input: t }) => {
        try {
            return {
                emailUsed: !!(await e.prisma.restaurant.findUnique({
                    where: { email: t.email },
                })),
            }
        } catch {
            throw new k.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to login",
            })
        }
    }),
    login: h.input(ne).mutation(async ({ input: e, ctx: t }) => {
        try {
            let r = await t.prisma.restaurant.findFirst({
                where: { email: e.email },
            })
            if (!r)
                throw new k.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "Wrong Email or Password",
                })
            let o = await t.prisma.restaurantAuth.findUnique({
                where: { restaurantId: r.id },
            })
            if (!o || !o.password)
                throw new k.TRPCError({
                    code: "NOT_FOUND",
                    message: "Wrong Email or Password",
                })
            if (!(await fe(e.password, o.password)))
                throw new k.TRPCError({
                    code: "NOT_FOUND",
                    message: "Wrong Email or Password",
                })
            let s = M({ id: r.id })
            return { ...r, token: s }
        } catch {
            throw new k.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to login",
            })
        }
    }),
    loginOauth: h.input(he).mutation(async ({ ctx: e, input: t }) => {
        let { prisma: r } = e,
            { email: o } = t
        try {
            let a = await r.restaurant.findFirst({ where: { email: o } })
            if (!a)
                throw new k.TRPCError({
                    code: "NOT_FOUND",
                    message: "An account with this email was not found",
                })
            let s = M({ id: a.id })
            return { ...a, token: s }
        } catch {
            throw new k.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to login",
            })
        }
    }),
    sendOTP: h.input(ge).mutation(async ({ ctx: e, input: t }) => {
        let { email: r } = t,
            o = await e.prisma.restaurant.findUnique({ where: { email: r } })
        if (o) {
            let a = ye()
            try {
                await e.prisma.restaurantAuth.update({
                    where: { restaurantId: o.id },
                    data: { OTP: a, OTPExpires: Re() },
                }),
                    await we({ from: "Mealy Restaurant", to: o.email, otp: a })
            } catch {
                throw new k.TRPCError({
                    code: "BAD_REQUEST",
                    message: "Failed to generate OTP",
                })
            }
        }
    }),
    signUp: h.input(jt).mutation(async ({ ctx: e, input: t }) => {
        let {
                city: r,
                country: o,
                email: a,
                userFullName: s,
                password: i,
                phone: n,
                state: c,
                street: d,
                lat: u,
                lng: m,
                logo: p,
                name: g,
                alwaysOpen: f,
                closingTime: I,
                openingTime: V,
            } = t,
            { prisma: J } = e
        try {
            let S = await F(i),
                z = await J.restaurant.create({
                    data: {
                        city: r,
                        country: o,
                        email: a,
                        phone: n,
                        state: c,
                        street: d,
                        lat: u,
                        lng: m,
                        userFullName: s,
                        logo: p,
                        name: g,
                        alwaysOpen: f,
                        closingTime: I,
                        openingTime: V,
                        ratings: 0,
                        restaurantAuth: { create: { password: S } },
                    },
                }),
                B = M({ id: z.id })
            return { ...z, token: B }
        } catch {
            throw new k.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to create your account",
            })
        }
    }),
    signUpOauth: h.input(Yt).mutation(async ({ ctx: e, input: t }) => {
        let {
                city: r,
                country: o,
                email: a,
                userFullName: s,
                phone: i,
                state: n,
                street: c,
                lat: d,
                lng: u,
                logo: m,
                name: p,
                alwaysOpen: g,
                closingTime: f,
                openingTime: I,
            } = t,
            { prisma: V } = e
        try {
            let J = (0, Vr.randomBytes)(16),
                S = await F(J.toString("hex")),
                z = await V.restaurant.create({
                    data: {
                        city: r,
                        country: o,
                        email: a,
                        phone: i,
                        state: n,
                        street: c,
                        lat: d,
                        lng: u,
                        userFullName: s,
                        logo: m,
                        name: p,
                        alwaysOpen: g,
                        closingTime: f,
                        openingTime: I,
                        ratings: 0,
                        restaurantAuth: { create: { password: S } },
                    },
                }),
                B = M({ id: z.id })
            return { ...z, token: B }
        } catch {
            throw new k.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to create your account",
            })
        }
    }),
})
var Hr = l({
    allCustomers: _.input($t).query(async ({ ctx: e, input: t }) => {
        let { prisma: r, restaurant: o } = e,
            { page: a, limit: s } = t,
            n = (
                await r.user.findMany({
                    where: { orders: { some: { restaurantId: o.id } } },
                    select: {
                        createdAt: !0,
                        fullName: !0,
                        addresses: {
                            where: { isDefault: !0 },
                            select: {
                                city: !0,
                                country: !0,
                                state: !0,
                                street: !0,
                            },
                        },
                        orders: {
                            orderBy: { createdAt: "desc" },
                            where: { restaurantId: o.id },
                            select: { createdAt: !0, totalPrice: !0 },
                        },
                    },
                })
            ).map((p) => {
                let { addresses: g, createdAt: f, fullName: I, orders: V } = p,
                    { city: J, country: S, state: z, street: B } = g[0],
                    q = +V.reduce((G, $) => G + $.totalPrice, 0).toFixed(2),
                    Pe = `${B} ${J} ${z} ${S}`
                return {
                    totalSpending: q,
                    location: Pe,
                    joinDate: f,
                    name: I,
                    lastOrder: V[0],
                }
            })
        n.sort((p, g) => {
            let f = p.lastOrder.createdAt,
                I = g.lastOrder.createdAt
            return f < I ? 1 : f > I ? -1 : 0
        })
        let c = a - 1,
            d = n.slice(c * s, a * s),
            u = n.length,
            m = Math.ceil(u / s)
        return { customerPerPage: d, numberOfPages: m, total: u }
    }),
})
var N = {}
_e(N, {
    allCouriers: () => Za,
    allFood: () => Rt,
    allFoodOrdered: () => Ot,
    allOrders: () => Et,
    allRatings: () => xt,
    allRestaurants: () => Ga,
    allUserRatings: () => bt,
    allUsers: () => Ka,
    averageRestaurantRating: () => At,
    clearData: () => Wr,
    drink: () => yt,
    getDeliveryFee: () => Ke,
    getTotalPrice: () => Ge,
    meal: () => wt,
    randomFloatBy2_5: () => _t,
    randomItem: () => ce,
    randomNumber: () => be,
    randomNumber1N0: () => Pt,
    restaurantData: () => Ja,
    shuffleAndReduce: () => Je,
    shuffleArray: () => Qe,
    subtractAYear: () => W,
})
var U = {}
_e(U, { clearData: () => Wr })
b(U, require("@prisma/client"))
var Wr = async (e) => {
    await e.restaurant.deleteMany({}),
        await e.courier.deleteMany({}),
        await e.user.deleteMany({}),
        await e.images.deleteMany({})
}
b(N, U)
var ve = require("@faker-js/faker")
var Jr = y(require("random-location"))
var Te = y(require("zod")),
    qa = (e) =>
        Object.entries(e)
            .map(([t, r]) => {
                if (r && "_errors" in r)
                    return `${t}: ${r._errors.join(", ")}
`
            })
            .filter(Boolean),
    Va = Te.object({
        DATABASE_URL: Te.string().url(),
        RANDOM_LAT: Te.string(),
        RANDOM_LNG: Te.string(),
    }),
    ft = Va.safeParse(process.env)
if (!ft.success)
    throw (
        (console.error(
            `Environment Variables missing:
`,
            ...qa(ft.error.format())
        ),
        new Error("Invalid Environment Variables"))
    )
var Zr = ft.data
var { RANDOM_LAT: $a, RANDOM_LNG: Ha } = Zr,
    Wa = 1e4,
    ae = (e) => {
        let t = { latitude: parseFloat($a), longitude: parseFloat(Ha) }
        return Jr.default.randomCircumferencePoint(e ?? t, Wa)
    }
var Za = async (e, t) =>
    await Promise.all(
        Array.from({ length: 5 }, async (r, o) => {
            let a = ve.faker.name.fullName(),
                { latitude: s, longitude: i } = ae()
            return await t.courier.create({
                data: {
                    city: "ikeja",
                    country: "nigeria",
                    state: "lagos",
                    street: ve.faker.address.streetAddress(),
                    phone: ve.faker.phone.number(),
                    fullName: a,
                    email:
                        o === 0 ? "test@mail.com" : ve.faker.internet.email(a),
                    lat: s,
                    lng: i,
                    createdAt: W(new Date()),
                    courierAuth: { create: { password: e } },
                    isDummyAccount: { create: { isDummy: !0 } },
                },
            })
        })
    )
var Ja = [
        {
            name: "Eatery works",
            logo: "/image/upload/v1670077269/Mealy/logo/logo5_antpcr.png",
        },
        {
            name: "Feast Palace",
            logo: "/image/upload/v1670077269/Mealy/logo/logo8_s3fz9x.png",
        },
        {
            name: "Deli Divine",
            logo: "/image/upload/v1670077268/Mealy/logo/logo4_gir0vo.png",
        },
        {
            name: "Grub lord",
            logo: "/image/upload/v1670077267/Mealy/logo/logo2_zzg8yb.png",
        },
        {
            name: "Island Grill",
            logo: "/image/upload/v1670077267/Mealy/logo/logo3_zkcxdh.png",
        },
        {
            name: "Pancake World",
            logo: "/image/upload/v1670077267/Mealy/logo/logo7_mbz4rt.png",
        },
        {
            name: "Munchies",
            logo: "/image/upload/v1670077267/Mealy/logo/logo6_rnywea.png",
        },
        {
            name: "Mediterra Seafood",
            logo: "/image/upload/v1670077267/Mealy/logo/logo1_rvjrvm.png",
        },
    ],
    wt = [
        {
            name: "Timmy Pizza",
            price: 13.5,
            type: "Food",
            description:
                "This infused arrowroot stew is a shade slimy with a crispy texture. It is balanced with chia with cicely and has a large amount of sorrel. It smells foul with limes. It is piquant and spicy. You can really feel how high in vitamin B and how all natural it is.",
            image: "/image/upload/v1669934272/Mealy/food/timmy_pizza_nvj4yy.png",
        },
        {
            name: "Beefy Burger",
            price: 33,
            type: "Food",
            description:
                "This charred grapes eggroll is thoroughly honeyed with a delicate texture. It is similar to hemp seeds with cotula and has a touch of laurel. It smells fusty with tapioca. It is peculiarly grateful. You can really feel how locally sourced and how paleo-friendly it is.",
            image: "/image/upload/v1669934273/Mealy/food/wepik-photo-mode-2022927-145744_llitra.png",
        },
        {
            name: "Full Shrimps",
            price: 25,
            type: "Food",
            description:
                "This seared soybeans quiche is barely blackened with a crunchy texture. It resembles corn (maize) with garlic and has a great deal of chamomile. It smells like a cemetery with a bit of chrysanthemum leaves. It is decidedly literary. You can really feel how naturally sourced and how certified organic it is.",
            image: "/image/upload/v1669934272/Mealy/food/full_shrimps_a5pupe.png",
        },
        {
            name: "Spagetti with Tomato Stew",
            price: 15,
            type: "Food",
            description:
                "This fermented butternut squash lasagna is a shade syrupy with a hearty texture. It has an undertone of kumquat with aralia and has an overwhelming amount of hibiscus. It smells savory with an overwhelming amount of melon balls. It is pleasantly seductive. You can really feel how high fiber and how free trade it is.",
            image: "/image/upload/v1669934271/Mealy/food/spagetti_with_tomato_stew_evpyay.png",
        },
        {
            name: "Jollof Rice with Beef",
            price: 23,
            type: "Food",
            description:
                "This sauteed ac\xE9rola tapas is sort of dense with a silky texture. It bursts with the flavor of mulberries with southernwood and has a great deal of coriander-blair. It smells like licorice with just the right amount of prairie turnips. It is slightly scorched. You can really feel how no artificial colors and how vegan it is.",
            image: "/image/upload/v1669934272/Mealy/food/jollof_rice_with_beef_kx2tde.png",
        },
        {
            name: "Lunch Combo",
            price: 40,
            type: "Food",
            description:
                "This breaded pistachios hummus is a touch fluffy with a gooey texture. It is enhanced by pearl millet with lemon balm and has cumin. It smells like cotton candy with a touch of sugar-apples. It is healthy and hearty. You can really feel how gluten free and how third party tested it is.",
            image: "/image/upload/v1669934271/Mealy/food/wepik-photo-mode-2022927-145125_lonzng.png",
        },
        {
            name: "Macaroni Delight",
            price: 17.5,
            type: "Food",
            description:
                "This smoked iceberg lettuce eggroll is utterly juicy with a burned texture. It has an undertone of pomme with star anise and has a large amount of dill. It smells rank with a lot of peaches. It is vaguely Moorish. You can really feel how organic and how whole food it is.",
            image: "/image/upload/v1669934270/Mealy/food/wepik-photo-mode-2022927-145916_b649ab.png",
        },
        {
            name: "Full Chicken With Stew",
            price: 13.5,
            type: "Food",
            description:
                "This fried sugar snap peas paella is sort of electric with a dense texture. It is infused with black-eyed peas with southernwood and has arnika. It smells like acetone with a dash of peanuts. It is bitter and acrid. You can really feel how high in calcium and how antioxidant rich it is.",
            image: "/image/upload/v1669934270/Mealy/food/full_chicken_with_stew_lwe9so.png",
        },
        {
            name: "Veggies Buffet",
            price: 35,
            type: "Food",
            description:
                "This sauteed broccoli lunch is a shade grainy with a silky texture. It builds depth and complexity with cannellini beans with black pepper and has a touch of cardamom. It smells like a hospital with cherimoya. It is metallic and medicinal. You can really feel how fresh cut and how nutrient rich it is.",
            image: "/image/upload/v1669934270/Mealy/food/veggies_buffet_nxy2ep.png",
        },
        {
            name: "Pasta Frivoli",
            price: 11,
            type: "Food",
            description:
                "This whipped buckwheat quesadilla is quite blackened with an airy texture. It is redolent with blackberries with chamomile and has a hint of horseradish. It smells like perfume with pokeberry shoots. It is particularly new. You can really feel how high in probiotics and how antioxidant rich it is.",
            image: "/image/upload/v1669933261/Mealy/food/pasta-frivoli_jssrig.png",
        },
        {
            name: "Chocolate Pancakes",
            price: 25.5,
            type: "Food",
            description:
                "This blackened onion roast is a little sugary with a syrupy texture. It tastes strongly of sweet red pepper with mullein and has a dash of tarragon. It smells acrid with an overwhelming amount of nuts. It is strongly literary. You can really feel how high in omega 3s and how locally sourced it is.",
            image: "/image/upload/v1669933261/Mealy/food/chocolate_pancake_njtvf1.png",
        },
        {
            name: "Seasoned Stake",
            price: 26,
            type: "Food",
            description:
                "This blackened finger millet taco is wholly mushy with a crispy texture. It has characteristics of olives with bee balm and has angelica. It smells ambrosial with a dash of butterbur. It is especially luscious. You can really feel how organic and how third party tested it is.",
            image: "/image/upload/v1669933260/Mealy/food/seasoned_stake_lbskbo.png",
        },
        {
            name: "Potato Soup",
            price: 20,
            type: "Food",
            description:
                "This charred guava brunch is sort of hearty with a gooey texture. It embodies the essence of wheat with savory and has a dash of cuban oregano. It smells like scented soap with a great deal of vermicelli. It is racy and delicious. You can really feel how high in omega 3s and how all natural it is.",
            image: "/image/upload/v1669933259/Mealy/food/potato_soup_d8iekj.png",
        },
        {
            name: "Pork Stew",
            price: 13.5,
            type: "Food",
            description:
                "This broiled Asian rice stew is scarcely mushy with a dense texture. It has a tinge of papaya with scullcap and has cinnamon. It smells like it is decomposing with a hint of balsam-pear (bitter gourd). It is faintly sweet. You can really feel how all natural and how high in magnesium it is.",
            image: "/image/upload/v1669933259/Mealy/food/pork_stew_czgzuz.jpg",
        },
        {
            name: "Bread and Egg Breakfast",
            price: 13.5,
            type: "Food",
            description:
                "This fermented quinoa dessert is altogether hearty with a succulent texture. It is similar to sorana beans with rosemary and has a dash of bayberry. It smells delicate with a dash of fireweed. It is syrupy sweet. You can really feel how high in probiotics and how high in iron it is.",
            image: "/image/upload/v1669933258/Mealy/food/bread_and_egg_breakfast_hw78sy.png",
        },
        {
            name: "Dumplins",
            price: 16,
            type: "Food",
            description:
                "This breaded mangosteen eggroll is absolutely intoxicating with a sticky texture. It has an undertone of carrot with paprika and has mugwort. It smells like mold with a lot of jew's ear. It is has variety. You can really feel how premium and how high in riboflavin it is",
            image: "/image/upload/v1669933257/Mealy/food/dumplins_w5rqdj.jpg",
        },
        {
            name: "Noodles and Shrimp",
            price: 13.5,
            type: "Food",
            description:
                "This breaded Asian rice wrap is comparatively honeyed with a buttery texture. It compares to pomme with hyssop and has a touch of rue. It smells like chicken broth with a lot of melon balls. It is slightly minerally. You can really feel how paleo-friendly and how high in probiotics it is.",
            image: "/image/upload/v1669822848/Mealy/food/noodles_and_shrimp_vothmr.jpg",
        },
        {
            name: "Freevolies",
            price: 10,
            type: "Food",
            description:
                "This blanched banana appetizer is a little velvety with a fizzy texture. It has characteristics of finger millet with nettle and has just the right amount of celery seed. It smells clean with a large amount of ginger root. It is truly German. You can really feel how high in omega 3s and how fresh cut it is.",
            image: "/image/upload/v1669933258/Mealy/food/freevolies_lrdxtw.jpg",
        },
        {
            name: "Grilled Ribs",
            price: 35,
            type: "Food",
            description:
                "This glazed apple wrap is totally succulent with a tough texture. It is redolent with goji berries with mace and has an overwhelming amount of dill. It smells squalid with a tiny bit of longans. It is pleasantly sweet. You can really feel how third party tested and how high in healthy fats it is.",
            image: "/image/upload/v1669822847/Mealy/food/grilled_ribs_djpixw.jpg",
        },
        {
            name: "Meat and Veggie Bowl",
            price: 30.5,
            type: "Food",
            description:
                "This marinated black-eyed peas sushi is fairly soft with an intoxicating texture. It is infused with adzuki beans with mullein and has just the right amount of arnika. It smells like maple sugar with just the right amount of plums. It is acrid and pungent. You can really feel how fresh cut and how soy free it is.",
            image: "/image/upload/v1669822847/Mealy/food/meat_and_veggie_bowl_ety9di.jpg",
        },
    ],
    yt = [
        {
            name: "Chocolate Milkshake",
            price: 18,
            type: "Drink",
            description: "A very rich in content chocolate milkshake",
            image: "/image/upload/v1669935762/Mealy/drink/chocolate_milkshake_m3omy9.png",
        },
        {
            name: "Lemonade",
            price: 10,
            type: "Drink",
            description: "Lemonade drink made with 100% lemon",
            image: "/image/upload/v1669935760/Mealy/drink/lemonade_b2n3bp.png",
        },
        {
            name: "Creme crame",
            price: 15,
            type: "Drink",
            description: "Cold creammy malt",
            image: "/image/upload/v1669935758/Mealy/drink/creme_crame_bfwcpz.png",
        },
        {
            name: "Coca cola",
            price: 13,
            type: "Drink",
            description: "A coca cola drink",
            image: "/image/upload/v1669935758/Mealy/drink/coca_cola_g7alx5.png",
        },
        {
            name: "Coffee",
            price: 15,
            type: "Drink",
            description: "Coffee drink to boost energy",
            image: "/image/upload/v1669935760/Mealy/drink/lemonade_b2n3bp.png",
        },
        {
            name: "Iced Tea",
            price: 16,
            type: "Drink",
            description: "Chilled ice tea",
            image: "/image/upload/v1669935756/Mealy/drink/iced_tea_wjzfi8.png",
        },
        {
            name: "Orange Juice",
            price: 20,
            type: "Drink",
            description: "Freshly squeezed orange juice",
            image: "/image/upload/v1669935756/Mealy/drink/orange_juice_kxaubc.png",
        },
        {
            name: "Chupa Chups",
            price: 12,
            type: "Drink",
            description: "Chupa chubs flavored drink",
            image: "/image/upload/v1669935755/Mealy/drink/chupa_chups_xfezk5.png",
        },
        {
            name: "Mundet",
            price: 15,
            type: "Drink",
            description: "Cool mundet drink",
            image: "/image/upload/v1669935755/Mealy/drink/mundet_bk6ed9.png",
        },
        {
            name: "Fanta",
            price: 14,
            type: "Drink",
            description: "Chilled fanta drink",
            image: "/image/upload/v1669935755/Mealy/drink/fanta_gick5i.png",
        },
        {
            name: "Tomato Juice",
            price: 11,
            type: "Drink",
            description: "Freshly squeezed tomato",
            image: "/image/upload/v1669822847/Mealy/drink/tomato_juice_wg7cfn.jpg",
        },
    ]
var Rt = async (e, t) => {
    let r = [],
        o = [5, 10, 15, 20, 25, 30, 35, 40]
    return (
        await Promise.all(
            e.map(async (a) => {
                let s = [...Je(wt, 10), ...Je(yt, 5)]
                await Promise.all(
                    s.map(async (i) => {
                        let n = 0,
                            c = 0
                        if (be() === 1) {
                            let m = ce(o)
                            n = m
                            let p = parseFloat((i.price * (m / 100)).toFixed(2))
                            c = +(i.price - p).toFixed(2)
                        }
                        let u = await t.food.create({
                            data: {
                                description: i.description,
                                image: i.image,
                                name: i.name,
                                price: i.price,
                                type: i.type,
                                restaurantId: a.id,
                                discountPercentage: n,
                                discountPrice: c,
                                createdAt: new Date(2022, 1, 1, 0, 0, 0),
                            },
                        })
                        r.push(u)
                    })
                )
            })
        ),
        r
    )
}
var Gr = require("@faker-js/faker")
var Et = async (e, t, r, o) => {
        let a = []
        return (
            await Promise.all(
                e.map(
                    async (s) =>
                        await Promise.all(
                            Array.from({ length: 100 }, async () => {
                                let i = ce(r).id,
                                    n = await o.addresses.findFirst({
                                        where: { userId: i, isDefault: !0 },
                                    }),
                                    c = ""
                                if (n) {
                                    let {
                                        city: u,
                                        country: m,
                                        street: p,
                                        state: g,
                                    } = n
                                    c = `${p}, ${u}, ${g}, ${m}`
                                }
                                let d = await o.order.create({
                                    data: {
                                        isPaid: !0,
                                        courierId: ce(t).id,
                                        restaurantId: s.id,
                                        payment: ce(["Card", "Ondelivery"]),
                                        status: "Delivered",
                                        userId: i,
                                        deliveryAddress: c,
                                        deliveryLat:
                                            (n == null ? void 0 : n.lat) ?? 0,
                                        deliveryLng:
                                            (n == null ? void 0 : n.lng) ?? 0,
                                        deliveryFee: 0,
                                        totalPriceWithDelivery: 0,
                                        totalPrice: 0,
                                        createdAt: Gr.faker.date.between(
                                            W(new Date()),
                                            Date.now()
                                        ),
                                    },
                                })
                                a.push(d)
                            })
                        )
                )
            ),
            a
        )
    },
    Ot = async (e, t, r) => {
        await Promise.all(
            e.map(async (o) => {
                let a = be(8),
                    s = t.filter((c) => c.restaurantId === o.restaurantId),
                    i = Qe(s),
                    n = await Promise.all(
                        Array.from({ length: a }, async (c, d) => {
                            let u = i[d],
                                m = be(3),
                                p = u.price,
                                g = u.discountPrice
                            return (
                                g && g > 0 && (p = g),
                                await r.foodOrdered.create({
                                    data: {
                                        quantity: m,
                                        orderId: o.id,
                                        foodTotalPrice: m * p,
                                        orderDiscountPercentage:
                                            u.discountPercentage ?? 0,
                                        orderDiscountPrice:
                                            u.discountPrice ?? 0,
                                        orderPrice: u.price ?? 0,
                                        foodId: u.id,
                                        createdAt: o.createdAt,
                                    },
                                })
                            )
                        })
                    )
                await r.order.update({
                    where: { id: o.id },
                    data: {
                        totalPrice: Ge(n),
                        totalPriceWithDelivery: Ge(n) + Ke(n),
                        deliveryFee: Ke(n),
                    },
                })
            })
        )
    }
var Tt = require("@faker-js/faker"),
    bt = async (e, t, r) => {
        await Promise.all(
            e.map(async (o) => {
                await Promise.all(
                    t.map(async (a) => {
                        let s = Pt(),
                            i = Math.round(Math.random() * 5) + 2,
                            n
                        s && (n = Tt.faker.lorem.sentences(i))
                        let c = Tt.faker.date.between(W(new Date()), Date.now())
                        await r.restaurantRatings.create({
                            data: {
                                user: { connect: { id: o.id } },
                                restaurant: { connect: { id: a.id } },
                                ratings: _t(),
                                comment: n,
                                createdAt: c,
                                updatedAt: c,
                            },
                        })
                    })
                )
            })
        )
    },
    xt = async (e, t) => {
        let r
        t
            ? (r = [t])
            : (r = await e.restaurant.findMany({
                  include: { userRatings: !0 },
              })),
            await Promise.all(
                r.map(async (o) => {
                    await e.restaurant.update({
                        where: { id: o.id },
                        data: { ratings: At(o.userRatings) },
                    })
                })
            )
    }
var ke = require("@faker-js/faker")
var Ga = async (e, t, r) =>
    await Promise.all(
        Array.from({ length: 5 }, async (o, a) => {
            let { latitude: s, longitude: i } = ae()
            return await r.restaurant.create({
                data: {
                    name: t[a].name,
                    city: "ikeja",
                    country: "nigeria",
                    state: "lagos",
                    street: ke.faker.address.streetAddress(),
                    phone: ke.faker.phone.number(),
                    userFullName: ke.faker.name.fullName(),
                    closingTime: "05:00 PM",
                    openingTime: "09:00 AM",
                    alwaysOpen: a === 0,
                    ratings: 0,
                    email:
                        a === 0
                            ? "test@mail.com"
                            : ke.faker.internet.email(t[a].name),
                    lat: s,
                    lng: i,
                    logo: t[a].logo,
                    createdAt: W(new Date()),
                    restaurantAuth: { create: { password: e } },
                },
            })
        })
    )
var xe = require("@faker-js/faker")
var Ka = async (e, t) =>
    await Promise.all(
        Array.from({ length: 21 }, async (r, o) => {
            let { latitude: a, longitude: s } = ae(),
                i = xe.faker.name.firstName(),
                n = xe.faker.name.lastName()
            return await t.user.create({
                data: {
                    fullName: `${i} ${n}`,
                    email:
                        o === 0
                            ? "test@mail.com"
                            : xe.faker.internet.email(i, n).toLowerCase(),
                    phone: xe.faker.phone.number(),
                    createdAt: W(new Date()),
                    addresses: {
                        create: {
                            city: "ikeja",
                            country: "nigeria",
                            state: "lagos",
                            street: xe.faker.address.streetAddress(),
                            isDefault: !0,
                            lat: a,
                            lng: s,
                        },
                    },
                    userAuth: { create: { password: e } },
                    isDummyAccount: { create: { isDummy: !0 } },
                },
            })
        })
    )
var Qe = (e) => {
        for (let t = 0; t < e.length; t++) {
            let r = Math.round(Math.random() * (1 + t)),
                o = e[t]
            ;(e[t] = e[r]), (e[r] = o)
        }
        return e
    },
    Je = (e, t) => Qe(e).slice(0, t),
    ce = (e) => e[Math.floor(Math.random() * e.length)],
    be = (e) => Math.floor(Math.random() * (e || 5)) + 1,
    Pt = () => Math.round(Math.random() * 10),
    _t = (e) => {
        let t = Math.floor(Math.random() * (e || 5)) + 2
        return t > 5 && (t = 5), t
    },
    At = (e) =>
        parseFloat(
            (e.reduce((t, r) => t + r.ratings, 0) / e.length).toFixed(1)
        ),
    W = (e) => {
        let t = new Date(e)
        return t.setFullYear(e.getFullYear() - 1), t
    },
    Ge = (e) =>
        parseFloat(e.reduce((t, r) => t + r.foodTotalPrice, 0).toFixed(2)),
    Ke = (e) => Math.floor(e.reduce((t, r) => t + r.foodTotalPrice, 0) * 0.1)
var ze = require("@trpc/server")
var Z = y(require("dayjs")),
    Xe = (e) => {
        if (e === "Today") {
            let t = (0, Z.default)().endOf("day").toDate()
            return {
                startOfDay: (0, Z.default)().startOf("day").toDate(),
                endOfDay: t,
            }
        }
        if (e === "This Week") {
            let t = (0, Z.default)().endOf("day").toDate()
            return {
                startOfDay: (0, Z.default)()
                    .startOf("week")
                    .startOf("day")
                    .toDate(),
                endOfDay: t,
            }
        }
        if (e === "This Month") {
            let t = (0, Z.default)().endOf("day").toDate()
            return {
                startOfDay: (0, Z.default)()
                    .startOf("month")
                    .startOf("day")
                    .toDate(),
                endOfDay: t,
            }
        }
        if (e === "This Year") {
            let t = (0, Z.default)().endOf("year").endOf("day").toDate()
            return {
                startOfDay: (0, Z.default)()
                    .startOf("year")
                    .startOf("day")
                    .toDate(),
                endOfDay: t,
            }
        }
        if (e === "Last Year") {
            let t = (0, Z.default)()
                .subtract(1, "year")
                .endOf("year")
                .endOf("day")
                .toDate()
            return {
                startOfDay: (0, Z.default)()
                    .subtract(1, "year")
                    .startOf("year")
                    .startOf("day")
                    .toDate(),
                endOfDay: t,
            }
        }
    }
var Kr = l({
    getData: _.input(Ht).query(async ({ ctx: e, input: t }) => {
        let { prisma: r, restaurant: o } = e,
            { period: a } = t,
            s = Xe(a),
            i = {
                gte: s == null ? void 0 : s.startOfDay,
                lte: s == null ? void 0 : s.endOfDay,
            },
            n = await r.restaurant.findFirst({
                where: { id: o.id },
                select: {
                    orders: {
                        where: { createdAt: i },
                        select: { totalPrice: !0, createdAt: !0, userId: !0 },
                    },
                    _count: {
                        select: {
                            foods: !0,
                            orders: { where: { createdAt: i } },
                        },
                    },
                },
            })
        if (!n)
            throw new ze.TRPCError({
                code: "NOT_FOUND",
                message: "Restaurant details not found",
            })
        let c = 0,
            d = [],
            m = {
                ...n.orders.reduce(
                    (S, z) => {
                        let { totalPrice: B, userId: q, createdAt: Pe } = z,
                            G = Pe.getMonth()
                        ;(c = +(c + B).toFixed(2)), d.push(q)
                        let $ = { ...S },
                            se = $.revenue[G],
                            ue = $.customers[G]
                        return (
                            se
                                ? ($.revenue[G] = +(se + B).toFixed(2))
                                : ($.revenue[G] = B),
                            ue ? ($.customers[G] += 1) : ($.customers[G] = 1),
                            $
                        )
                    },
                    { revenue: [], customers: [] }
                ),
                total: { revenue: c, customers: d.length },
            }
        d = [...new Set(d)]
        let p = {
                totalOrders: n._count.orders,
                totalRevenue: c,
                uniqueCustomers: d.length,
                totalMenu: n._count.foods,
            },
            f = (
                await r.food.findMany({
                    orderBy: { foodOrdered: { _count: "desc" } },
                    include: {
                        _count: {
                            select: {
                                foodOrdered: { where: { createdAt: i } },
                            },
                        },
                    },
                    where: { restaurantId: o.id },
                })
            )
                .sort((S, z) => z._count.foodOrdered - S._count.foodOrdered)
                .slice(0, 4),
            V = (
                await r.order.groupBy({
                    by: ["status"],
                    where: { restaurantId: o.id, createdAt: i },
                    _count: { status: !0 },
                })
            ).reduce(
                (S, z) => {
                    let { _count: B, status: q } = z
                    return q === "Accepted" ||
                        q === "Cooking" ||
                        q === "New" ||
                        q === "ReadyForPickUp"
                        ? { ...S, Current: S.Current + B.status }
                        : { ...S, [q]: S[q] + B.status }
                },
                { Current: 0, OnRoute: 0, Delivered: 0, Rejected: 0 }
            )
        return {
            middleData: m,
            bottomData: { trending: f, orderStatus: V },
            topData: p,
        }
    }),
    randomData: _.mutation(async ({ ctx: e }) => {
        let { prisma: t, restaurant: r } = e
        if (r.usedData)
            throw new ze.TRPCError({
                code: "NOT_FOUND",
                message: "This restaurant has already used the dummy data",
            })
        let o = [r]
        try {
            return (
                await t.$transaction(async (a) => {
                    let s = await a.user.findMany({
                            where: { isDummyAccount: { isDummy: !0 } },
                        }),
                        i = await a.courier.findMany({
                            where: { isDummyAccount: { isDummy: !0 } },
                        }),
                        n = await Rt(o, a),
                        c = await Et(o, i, s, a)
                    await Ot(c, n, a), await bt(s, o, a)
                    let d = await a.restaurant.findFirst({
                        where: { id: r.id },
                        include: { userRatings: !0 },
                    })
                    if (d)
                        await xt(a, d),
                            await a.restaurant.update({
                                where: { id: r.id },
                                data: { usedData: !0 },
                            })
                    else
                        throw new ze.TRPCError({
                            code: "INTERNAL_SERVER_ERROR",
                            message: "Failed to use dummy data",
                        })
                }),
                { message: "Dummy data has successfully been used" }
            )
        } catch {
            throw new ze.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to use dummy data",
            })
        }
    }),
})
var Qr = y(require("axios"))
var Xr = require("@trpc/server")
var { MAP_BOX_TOKEN: Qa } = P,
    ea = l({
        getGeocode: h.input(Zt).mutation(async ({ input: e }) => {
            let { city: t, country: r, state: o, street: a } = e,
                s = encodeURI(`${a} ${t} ${o} ${r}`)
            try {
                let { data: i } = await Qr.default.get(
                        `https://api.mapbox.com/geocoding/v5/mapbox.places/${s}.json?access_token=${Qa}`
                    ),
                    [n, c] = i.features[0].geometry.coordinates,
                    { latitude: d, longitude: u } = ae({
                        latitude: n,
                        longitude: c,
                    })
                return { lat: d, lng: u }
            } catch {
                throw new Xr.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to fetch restaurant location",
                })
            }
        }),
    })
var oe = require("@trpc/server")
var ta = l({
    allMenu: _.input(Gt).query(async ({ ctx: e, input: t }) => {
        let { prisma: r, restaurant: o } = e,
            { page: a, limit: s } = t,
            i = a - 1,
            n = await r.food.count({ where: { restaurantId: o.id } }),
            c = await r.food.findMany({
                where: { restaurantId: o.id },
                orderBy: [{ updatedAt: "desc" }],
                take: s,
                skip: i * s,
            }),
            d = Math.ceil(n / s)
        return { menu: c, total: n, numberOfPages: d }
    }),
    createMenu: _.input(st).mutation(async ({ ctx: e, input: t }) => {
        let {
                description: r,
                discountPercentage: o,
                image: a,
                name: s,
                price: i,
                type: n,
            } = t,
            { prisma: c, restaurant: d } = e,
            u = 0
        if (o) {
            let m = +((i * o) / 100).toFixed(2)
            u = +(i - m).toFixed(2)
        }
        try {
            return (
                await c.food.create({
                    data: {
                        description: r,
                        image: a,
                        name: s,
                        price: i,
                        type: n,
                        discountPercentage: o,
                        discountPrice: u,
                        restaurantId: d.id,
                    },
                }),
                { message: "Your menu has been created successfully" }
            )
        } catch {
            throw new oe.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to create your menu",
            })
        }
    }),
    deleteMenu: _.input(nt).mutation(async ({ ctx: e, input: t }) => {
        let { prisma: r } = e,
            { id: o } = t
        if (!o)
            throw new oe.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to delete menu item",
            })
        try {
            return (
                await r.food.delete({ where: { id: o } }),
                { message: "Menu has been deleted successfully" }
            )
        } catch {
            throw new oe.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to delete menu item",
            })
        }
    }),
    editMenu: _.input(Kt).mutation(async ({ ctx: e, input: t }) => {
        let {
                description: r,
                discountPercentage: o,
                id: a,
                image: s,
                name: i,
                price: n,
                type: c,
            } = t,
            { prisma: d } = e,
            u = 0
        if (o) {
            let m = +((n * o) / 100).toFixed(2)
            u = +(n - m).toFixed(2)
        }
        try {
            return (
                await d.food.update({
                    where: { id: a },
                    data: {
                        name: i,
                        description: r,
                        discountPercentage: o,
                        discountPrice: u,
                        image: s,
                        price: n,
                        type: c,
                    },
                }),
                { message: "Menu has been updated successfully" }
            )
        } catch {
            throw new oe.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to create your menu",
            })
        }
    }),
    getMenu: _.input(Qt).query(async ({ ctx: e, input: t }) => {
        let { prisma: r } = e,
            { id: o } = t
        if (!o)
            throw new oe.TRPCError({
                code: "NOT_FOUND",
                message: "The menu you requested for does not exist",
            })
        try {
            let a = await r.food.findFirst({ where: { id: o } })
            if (!a)
                throw new oe.TRPCError({
                    code: "NOT_FOUND",
                    message: "The menu you requested for does not exist",
                })
            return a
        } catch {
            throw new oe.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed get your menu",
            })
        }
    }),
})
var et = require("@trpc/server"),
    ra = require("@trpc/server/observable")
var aa = l({
    changeOrderStatus: _.input(or).mutation(async ({ ctx: e, input: t }) => {
        let { id: r, status: o } = t,
            { prisma: a, ee: s, restaurant: i } = e
        try {
            let n = await a.order.update({
                where: { id: r },
                data: { status: o },
            })
            return o === "Accepted"
                ? (await ee({
                      pushToken: n.pushToken ?? "",
                      orderId: n.id,
                      title: i.name,
                      body: "Your order has been accepted",
                  }),
                  { message: "Order has been accepted" })
                : o === "Rejected"
                ? (await ee({
                      pushToken: n.pushToken ?? "",
                      orderId: n.id,
                      title: i.name,
                      body: "Your order has been rejected",
                  }),
                  { message: "Order has been rejected" })
                : o === "Cooking"
                ? (await ee({
                      pushToken: n.pushToken ?? "",
                      orderId: n.id,
                      title: i.name,
                      body: "Your order is cooking",
                  }),
                  { message: "Order has been marked as cooking" })
                : o === "ReadyForPickUp"
                ? (await ee({
                      pushToken: n.pushToken ?? "",
                      orderId: n.id,
                      title: i.name,
                      body: "Your order is ready",
                  }),
                  s.emit("ORDER_READY", i.state, i.country),
                  { message: "Order has been made ready for pick up" })
                : { message: "Order status has been updated" }
        } catch {
            throw new et.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to update the order",
            })
        }
    }),
    getAllOrders: _.input(ar).query(async ({ ctx: e, input: t }) => {
        let { limit: r, period: o, status: a, page: s } = t,
            { prisma: i, restaurant: n } = e,
            c = Xe(o),
            d = s - 1,
            u = await i.order.count({
                where: {
                    restaurantId: n.id,
                    status: a ?? void 0,
                    createdAt: c
                        ? { gte: c.startOfDay, lte: c.endOfDay }
                        : void 0,
                },
            }),
            m = await i.order.findMany({
                where: {
                    restaurantId: n.id,
                    status: a ?? void 0,
                    createdAt: c
                        ? { gte: c.startOfDay, lte: c.endOfDay }
                        : void 0,
                },
                include: { user: { select: { fullName: !0 } } },
                take: r + 1,
                skip: d * r,
                orderBy: { createdAt: "desc" },
            }),
            p = Math.ceil(u / r)
        return { orders: m, numberOfPages: p, total: u }
    }),
    getOrderDetails: _.input(Q).query(async ({ ctx: e, input: t }) => {
        let { id: r } = t,
            { prisma: o } = e
        try {
            let a = await o.order.findFirst({
                where: { id: r },
                include: {
                    courier: { select: { fullName: !0, phone: !0 } },
                    foodOrdered: {
                        include: { food: { select: { image: !0, name: !0 } } },
                    },
                    user: { select: { fullName: !0, phone: !0 } },
                },
            })
            if (!a)
                throw new et.TRPCError({
                    code: "NOT_FOUND",
                    message: "The order you are looking for does not exist",
                })
            return a
        } catch {
            throw new et.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to get your header",
            })
        }
    }),
    newOrder: h.input(Ie).subscription(({ ctx: e, input: t }) => {
        let { ee: r } = e,
            { id: o } = t
        return (0, ra.observable)((a) => {
            let s = (i, n) => {
                o === i &&
                    a.next({ message: "You have a new order", orderId: n })
            }
            return (
                r.on("ORDER_CREATED", s),
                () => {
                    r.off("ORDER_CREATED", s)
                }
            )
        })
    }),
})
var oa = require("@trpc/server")
var sa = l({
    getAllReviews: _.input(hr).query(async ({ ctx: e, input: t }) => {
        let { prisma: r, restaurant: o } = e,
            { limit: a, rating: s, page: i } = t,
            n = i - 1
        try {
            let c = await r.restaurantRatings.findMany({
                    where: { restaurantId: o.id, ratings: s ?? void 0 },
                    include: { user: { select: { fullName: !0 } } },
                    take: a,
                    skip: n * a,
                    orderBy: [{ updatedAt: "desc" }],
                }),
                d = await r.restaurantRatings.count({
                    where: { restaurantId: o.id, ratings: s ?? void 0 },
                }),
                u = Math.ceil(d / a)
            return { reviews: c, reviewsCount: d, numberOfPages: u }
        } catch {
            throw new oa.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to get all reviews",
            })
        }
    }),
})
var It = require("@trpc/server")
var na = l({
    deleteAccount: _.mutation(async ({ ctx: e }) => {
        let { prisma: t, restaurant: r } = e
        try {
            return (
                await t.restaurant.delete({ where: { id: r.id } }),
                { message: "Your account has been deleted" }
            )
        } catch {
            throw new It.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to delete your account",
            })
        }
    }),
    updateAccount: _.input(Ct).mutation(async ({ ctx: e, input: t }) => {
        let {
                alwaysOpen: r,
                city: o,
                country: a,
                lat: s,
                lng: i,
                logo: n,
                name: c,
                phone: d,
                state: u,
                street: m,
                userFullName: p,
                closingTime: g,
                openingTime: f,
            } = t,
            { prisma: I, restaurant: V } = e
        try {
            return (
                await I.restaurant.update({
                    where: { id: V.id },
                    data: {
                        alwaysOpen: r,
                        city: o,
                        country: a,
                        logo: n,
                        lat: s,
                        lng: i,
                        name: c,
                        phone: d,
                        state: u,
                        street: m,
                        userFullName: p,
                        closingTime: g,
                        openingTime: f,
                    },
                }),
                { message: "Your account has successfully been updated" }
            )
        } catch {
            throw new It.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to update your account",
            })
        }
    }),
})
var ia = l({
    auth: $r,
    customers: Hr,
    dashboard: Kr,
    location: ea,
    menu: ta,
    orders: aa,
    review: sa,
    settings: na,
})
var ua
process.env.NODE_ENV !== "production" &&
    (ua = { path: da.default.join(__dirname, "../../../.env") })
ca.config(ua)
var de = (0, Nt.default)()
de.use(Nt.default.json())
de.use((0, ma.default)())
var wa = la.default.createServer(de),
    Me = new ha.default.Server({ server: wa }),
    St = l({
        courier: Mr,
        food: qr,
        home: h.query(() => ({ message: "Welcome to Mealy" })),
        image: wr,
        restaurant: ia,
    }),
    Xa = (0, fa.applyWSSHandler)({ wss: Me, router: St, createContext: lt })
Me.on("connection", (e) => {
    console.log(`++ Connection (${Me.clients.size})`),
        e.once("close", () => {
            console.log(`-- Connection (${Me.clients.size})`)
        })
})
process.on("SIGTERM", () => {
    Xa.broadcastReconnectNotification(), Me.close()
})
process.env.NODE_ENV !== "production" && de.use((0, pa.default)("dev"))
de.get("/", (e, t) => {
    t.status(200).json({ message: "Welcome to Mealy" })
})
de.use(
    "/api/trpc",
    ga.createExpressMiddleware({ router: St, createContext: lt })
)
de.post("/api/image/upload", Rr.single("image"), Er)
wa.listen(P.PORT, () => {
    console.log(`api is listening on port ${P.PORT}`)
})
0 && (module.exports = { appRouter })
