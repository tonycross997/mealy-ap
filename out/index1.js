"use strict"
var __create = Object.create
var __defProp = Object.defineProperty
var __getOwnPropDesc = Object.getOwnPropertyDescriptor
var __getOwnPropNames = Object.getOwnPropertyNames
var __getProtoOf = Object.getPrototypeOf
var __hasOwnProp = Object.prototype.hasOwnProperty
var __export = (target, all) => {
    for (var name in all)
        __defProp(target, name, { get: all[name], enumerable: true })
}
var __copyProps = (to, from, except, desc) => {
    if ((from && typeof from === "object") || typeof from === "function") {
        for (let key of __getOwnPropNames(from))
            if (!__hasOwnProp.call(to, key) && key !== except)
                __defProp(to, key, {
                    get: () => from[key],
                    enumerable:
                        !(desc = __getOwnPropDesc(from, key)) ||
                        desc.enumerable,
                })
    }
    return to
}
var __reExport = (target, mod, secondTarget) => (
    __copyProps(target, mod, "default"),
    secondTarget && __copyProps(secondTarget, mod, "default")
)
var __toESM = (mod, isNodeMode, target) => (
    (target = mod != null ? __create(__getProtoOf(mod)) : {}),
    __copyProps(
        // If the importer is in node compatibility mode or this is not an ESM
        // file that has been converted to a CommonJS file using a Babel-
        // compatible transform (i.e. "__esModule" has not been set), then set
        // "default" to the CommonJS "module.exports" for node compatibility.
        isNodeMode || !mod || !mod.__esModule
            ? __defProp(target, "default", { value: mod, enumerable: true })
            : target,
        mod
    )
)
var __toCommonJS = (mod) =>
    __copyProps(__defProp({}, "__esModule", { value: true }), mod)

// src/index.ts
var src_exports2 = {}
__export(src_exports2, {
    appRouter: () => appRouter,
})
module.exports = __toCommonJS(src_exports2)
var dotenv = __toESM(require("dotenv"))
var envFile
dotenv.config(envFile)
var import_cors = __toESM(require("cors"))
var import_express = __toESM(require("express"))
var import_http = __toESM(require("http"))
var import_morgan = __toESM(require("morgan"))
var import_ws = __toESM(require("ws"))
var trpcExpress = __toESM(require("@trpc/server/adapters/express"))
var import_ws2 = require("@trpc/server/adapters/ws")

// ../../packages/schema/src/auth/index.ts
var import_zod = require("zod")
var wrongEmailMessage = "Please enter a valid mail"
var wrongFirstNameMessage = "First Name must contain at least 3 characters"
var wrongLastNameMessage = "Last Name must contain at least 3 characters"
var wrongPasswordMessage = "Password must be atleast 6 characters"
var wrongEmailOrPassword = "Wrong Email or Password"
var emailAlreadyUsed = "Email has already been used"
var failedOTP = "Failed to generate OTP"
var requiredPasswordMessage = "This field is required"
var wrongPhoneMessage = "Number must be at least 5 Numbers"
var passwordNotMatchMessage = "Passwords do not match"
var passwordChangedMessage = "Your password has been changed"
var passwordNotChangedMessage = "Your new password was not saved"
var schemaItems = import_zod.z.object({
    email: import_zod.z
        .string()
        .trim()
        .email({ message: wrongEmailMessage })
        .refine((val) => val.toLocaleLowerCase()),
    password: import_zod.z.string().min(6, { message: wrongPasswordMessage }),
    confirmPassword: import_zod.z.string(),
    otp: import_zod.z.string().length(4),
    fullName: import_zod.z.string(),
    phone: import_zod.z.string().min(5, { message: wrongPhoneMessage }).trim(),
    street: import_zod.z
        .string()
        .trim()
        .min(3, "Street field must contain at least 3 letters")
        .transform((arg) => arg.toLocaleLowerCase()),
    state: import_zod.z
        .string()
        .trim()
        .min(3, "State field must contain at least 3 letters")
        .transform((arg) => arg.toLocaleLowerCase()),
    city: import_zod.z
        .string()
        .trim()
        .min(3, "City field must contain at least 3 letters")
        .transform((arg) => arg.toLocaleLowerCase()),
    country: import_zod.z
        .string()
        .trim()
        .min(3, "Country field must contain at least 3 letters")
        .transform((arg) => arg.toLocaleLowerCase()),
    lat: import_zod.z.number(),
    lng: import_zod.z.number(),
})
var emailExistSchema = schemaItems.pick({ email: true })
var changePasswordSchema = schemaItems.pick({
    email: true,
    password: true,
})
var confirmOTPSchema = schemaItems.pick({ email: true, otp: true })
var loginSchema = schemaItems.pick({ email: true }).extend({
    password: import_zod.z
        .string()
        .min(1, { message: requiredPasswordMessage }),
})
var oauthSchema = schemaItems.pick({ email: true })
var sendOTPSchema = schemaItems.pick({ email: true })
var signUpSchema = schemaItems.pick({
    city: true,
    country: true,
    email: true,
    fullName: true,
    password: true,
    phone: true,
    state: true,
    street: true,
    lng: true,
    lat: true,
})
var signUpOauthSchema = schemaItems.pick({
    city: true,
    country: true,
    email: true,
    fullName: true,
    phone: true,
    state: true,
    street: true,
    lng: true,
    lat: true,
})
var changePasswordFormSchema = schemaItems
    .pick({ password: true, confirmPassword: true })
    .refine((data) => data.password === data.confirmPassword, {
        path: ["confirmPassword"],
        message: passwordNotMatchMessage,
    })
var detailsFormSchema = schemaItems.pick({ phone: true }).extend({
    firstName: import_zod.z
        .string()
        .min(3, { message: wrongFirstNameMessage })
        .trim()
        .transform((arg) => arg.toLocaleLowerCase()),
    lastName: import_zod.z
        .string()
        .min(3, { message: wrongLastNameMessage })
        .trim()
        .transform((arg) => arg.toLocaleLowerCase()),
})
var locationFormSchema = schemaItems.pick({
    street: true,
    city: true,
    state: true,
    country: true,
    lat: true,
    lng: true,
})
var signUpFormSchema = schemaItems
    .pick({ email: true, password: true, confirmPassword: true })
    .refine((data) => data.password === data.confirmPassword, {
        path: ["confirmPassword"],
        message: passwordNotMatchMessage,
    })
var otpFormSchema = import_zod.z.object({
    pin1: import_zod.z.string().length(1),
    pin2: import_zod.z.string().length(1),
    pin3: import_zod.z.string().length(1),
    pin4: import_zod.z.string().length(1),
})
var resetPasswordFormSchema = schemaItems.pick({ email: true })
var restaurantDetails = schemaItems
    .pick({
        city: true,
        country: true,
        state: true,
        street: true,
        phone: true,
        lat: true,
        lng: true,
    })
    .extend({
        userFullName: import_zod.z
            .string()
            .min(3, "This field requires at least 3 characters"),
        logo: import_zod.z.string().min(1, "This field is required"),
        name: import_zod.z
            .string()
            .min(3, "This field requires at least 3 characters"),
        alwaysOpen: import_zod.z.boolean(),
        closingTime: import_zod.z.string().optional(),
        openingTime: import_zod.z.string().optional(),
    })
var restaurantDetailsSchema = restaurantDetails
    .refine(
        (data) => {
            if (!data.alwaysOpen) {
                if (!data.closingTime || data.closingTime.length < 1)
                    return false
            }
            return true
        },
        { message: "This field is required", path: ["closingTime"] }
    )
    .refine(
        (data) => {
            if (!data.alwaysOpen) {
                if (!data.openingTime || data.openingTime.length < 1)
                    return false
            }
            return true
        },
        { message: "This field is required", path: ["openingTime"] }
    )
var restaurantRegisterSchema = loginSchema
    .merge(restaurantDetails)
    .refine(
        (data) => {
            if (!data.alwaysOpen) {
                if (!data.closingTime || data.closingTime.length < 1)
                    return false
            }
            return true
        },
        { message: "This field is required", path: ["closingTime"] }
    )
    .refine(
        (data) => {
            if (!data.alwaysOpen) {
                if (!data.openingTime || data.openingTime.length < 1)
                    return false
            }
            return true
        },
        { message: "This field is required", path: ["openingTime"] }
    )
var restaurantDetailsFormSchema = restaurantDetails
    .extend({
        lat: import_zod.z
            .string()
            .min(1, "This field is required")
            .refine((v) => !Number.isNaN(parseFloat(v)), {
                message: "Please provide a valid latitude value",
                path: ["lat"],
            }),
        lng: import_zod.z
            .string()
            .min(1, "This field is required")
            .refine((v) => !Number.isNaN(parseFloat(v)), {
                message: "Please provide a valid longitude value",
                path: ["lng"],
            }),
    })
    .refine(
        (data) => {
            if (!data.alwaysOpen) {
                if (!data.closingTime || data.closingTime.length < 1)
                    return false
            }
            return true
        },
        { message: "This field is required", path: ["closingTime"] }
    )
    .refine(
        (data) => {
            if (!data.alwaysOpen) {
                if (!data.openingTime || data.openingTime.length < 1)
                    return false
            }
            return true
        },
        { message: "This field is required", path: ["openingTime"] }
    )
var restaurantRegisterOauth = restaurantDetails
    .merge(
        import_zod.z.object({
            email: import_zod.z
                .string()
                .trim()
                .email({ message: wrongEmailMessage })
                .refine((val) => val.toLocaleLowerCase()),
        })
    )
    .refine(
        (data) => {
            if (!data.alwaysOpen) {
                if (!data.closingTime || data.closingTime.length < 1)
                    return false
            }
            return true
        },
        { message: "This field is required", path: ["closingTime"] }
    )
    .refine(
        (data) => {
            if (!data.alwaysOpen) {
                if (!data.openingTime || data.openingTime.length < 1)
                    return false
            }
            return true
        },
        { message: "This field is required", path: ["openingTime"] }
    )

// ../../packages/schema/src/basket/index.ts
var z2 = __toESM(require("zod"))
var addToBasketSchema = z2.object({
    restaurantId: z2.string(),
    quantity: z2.number(),
    foodId: z2.string(),
    basketId: z2.string().nullish(),
    basketFoodId: z2.string().nullish(),
})
var getBasketSchema = z2.object({
    restaurantId: z2.string(),
})
var removeFromBasket = z2.object({
    basketFoodId: z2.string(),
    basketId: z2.string(),
})

// ../../packages/schema/src/customers/index.ts
var z3 = __toESM(require("zod"))
var allCustomersSchema = z3.object({
    limit: z3.number().default(10),
    page: z3.number().min(1).default(1),
})

// ../../packages/schema/src/dashboard/index.ts
var z4 = __toESM(require("zod"))
var dashboardDataSchema = z4.object({
    period: z4.enum(["This Year", "Last Year"]),
})

// ../../packages/schema/src/home/index.ts
var z5 = __toESM(require("zod"))
var homeDataSchema = z5.object({
    country: z5.string(),
    state: z5.string(),
})
var restaurantIdSchema = z5.object({
    id: z5.string(),
})

// ../../packages/schema/src/image/index.ts
var z6 = __toESM(require("zod"))
var allImagesTypeSchema = z6.object({
    imageType: z6.enum(["Logo", "Food", "Drink"]).nullish(),
    category: z6.enum(["Foods", "Logo"]).nullish(),
})

// ../../packages/schema/src/location/index.ts
var z7 = __toESM(require("zod"))
var restaurantLocationSchema = z7.object({
    street: z7
        .string()
        .trim()
        .min(3, "Street field must contain at least 3 letters")
        .transform((arg) => arg.toLocaleLowerCase()),
    state: z7
        .string()
        .trim()
        .min(3, "State field must contain at least 3 letters")
        .transform((arg) => arg.toLocaleLowerCase()),
    city: z7
        .string()
        .trim()
        .min(3, "City field must contain at least 3 letters")
        .transform((arg) => arg.toLocaleLowerCase()),
    country: z7
        .string()
        .trim()
        .min(3, "Country field must contain at least 3 letters")
        .transform((arg) => arg.toLocaleLowerCase()),
    lat: z7.number(),
    lng: z7.number(),
})
var gpsSchema = restaurantLocationSchema.pick({
    city: true,
    country: true,
    state: true,
    street: true,
})
var getRouteSchema = z7.object({
    courierLat: z7.number(),
    courierLng: z7.number(),
    restaurantLat: z7.number(),
    restaurantLng: z7.number(),
    customerLat: z7.number(),
    customerLng: z7.number(),
    deliveryStatus: z7.enum(["TO_PICK_UP", "TO_DELIVER"]).optional(),
})

// ../../packages/schema/src/menu/index.ts
var z8 = __toESM(require("zod"))
var allMenuSchema = z8.object({
    limit: z8.number().default(10),
    page: z8.number().min(1).default(1),
})
var createMenuSchema = z8.object({
    image: z8.string().min(3, 'image must contain at least 3 characters"'),
    name: z8.string().min(3, 'name must contain at least 3 characters"'),
    price: z8.number(),
    discountPercentage: z8.number(),
    description: z8
        .string()
        .min(10, 'description must contain at least 10 characters"'),
    type: z8.enum(["Food", "Logo", "Drink"]),
})
var createMenuFormSchema = z8.object({
    image: z8.string().min(3, 'image must contain at least 3 characters"'),
    name: z8.string().min(3, 'name must contain at least 3 characters"'),
    price: z8
        .string()
        .or(z8.number().min(1))
        .refine((arg) => +arg !== 0, {
            message: "The price must be greater than 0",
        }),
    discountPercentage: z8.string().or(z8.number()),
    description: z8
        .string()
        .min(10, 'description must contain at least 10 characters"'),
    type: z8.enum(["Food", "Logo", "Drink"]),
})
var deleteMenuSchema = z8.object({
    id: z8.string().nullish(),
})
var editMenuSchema = createMenuSchema.extend({
    id: z8.string(),
})
var getMenuSchema = deleteMenuSchema

// ../../packages/schema/src/order/index.ts
var z9 = __toESM(require("zod"))
var orderCreatedMessage = "Your order has been created"
var status = z9.enum([
    "New",
    "Cooking",
    "ReadyForPickUp",
    "OnRoute",
    "Delivered",
    "Rejected",
    "Accepted",
])
var allOrdersSchema = z9.object({
    limit: z9.number().min(5).max(100).default(7),
    cursor: z9.string().nullish(),
})
var createOrderSchema = z9.object({
    basketId: z9.string(),
    restaurantId: z9.string(),
    paymentMethod: z9.enum(["Card", "Ondelivery"]),
    deliveryFee: z9.number(),
    totalPrice: z9.number(),
    deliveryLat: z9.number(),
    deliveryLng: z9.number(),
    pushToken: z9.string(),
})
var orderDetailsSchema = z9.object({
    id: z9.string(),
})
var allOrdersRestaurantSchema = z9.object({
    limit: z9.number().min(5).max(100).default(10),
    status: status.nullish(),
    period: z9.enum([
        "Today",
        "This Week",
        "This Month",
        "This Year",
        "All",
        "Last Year",
    ]),
    page: z9.number().default(0),
})
var changeOrderStatusSchema = z9.object({
    id: z9.string(),
    status,
})
var newOrderSub = z9.object({
    state: z9.string(),
    country: z9.string(),
})

// ../../packages/schema/src/profile/index.ts
var z10 = __toESM(require("zod"))
var userProfileSchema = z10.object({
    fullName: z10.string(),
    phone: z10.string().min(5, { message: wrongPhoneMessage }).trim(),
})
var defaultAddressSchema = z10.object({
    id: z10.string(),
    defaultId: z10.string(),
})
var newAddressSchema = z10.object({
    street: z10
        .string()
        .trim()
        .transform((arg) => arg.toLocaleLowerCase()),
    state: z10
        .string()
        .trim()
        .transform((arg) => arg.toLocaleLowerCase()),
    city: z10
        .string()
        .trim()
        .transform((arg) => arg.toLocaleLowerCase()),
    country: z10
        .string()
        .trim()
        .transform((arg) => arg.toLocaleLowerCase()),
    lat: z10.number(),
    lng: z10.number(),
})
var editAddressSchema = newAddressSchema.extend({
    id: z10.string(),
})
var deleteAddressSchema = z10.object({
    id: z10.string(),
})
var changeUserPasswordSchema = z10.object({
    password: z10.string().min(6, { message: wrongPasswordMessage }),
})
var allDeliveriesSchema = z10.object({
    limit: z10.number().min(5).max(100).default(7),
    cursor: z10.string().nullish(),
})

// ../../packages/schema/src/review/index.ts
var z11 = __toESM(require("zod"))
var getRestaurantCommentsSchema = z11.object({
    id: z11.string(),
    rating: z11.number().nullish(),
    limit: z11.number().min(5).max(100).default(7),
    cursor: z11.string().nullish(),
})
var addCommentSchema = z11.object({
    id: z11.string().nullish(),
    ratings: z11.number(),
    comment: z11.string().nullish(),
    restaurantId: z11.string(),
})
var getUserComment = z11.object({
    commentId: z11.string(),
})
var deleteCommentSchema = z11.object({
    commentId: z11.string(),
    restaurantId: z11.string(),
})
var allRestaurantReviewsSchema = z11.object({
    rating: z11.number().nullish(),
    limit: z11.number().default(10),
    page: z11.number().min(1).default(1),
})

// src/trpc/index.ts
var import_superjson = __toESM(require("superjson"))
var import_server = require("@trpc/server")
var transformer = import_superjson.default
var t = import_server.initTRPC.context().create({ transformer })
var { router, middleware, mergeRouters, procedure } = t

// src/image/imageRouter.ts
var imageRouter = router({
    allImages: procedure.input(allImagesTypeSchema).query(({ ctx, input }) =>
        ctx.prisma.images.findMany({
            where: {
                type: input.imageType ?? void 0,
                category: input.category ?? void 0,
            },
            orderBy: [{ name: "asc" }],
        })
    ),
})

// src/image/upload.ts
var import_cloudinary = require("cloudinary")
var import_multer = __toESM(require("multer"))
var import_path = __toESM(require("path"))

// src/env/index.ts
var import_zod2 = require("zod")
var formatErrors = (errors) =>
    Object.entries(errors)
        .map(([name, value]) => {
            if (value && "_errors" in value)
                return `${name}: ${value._errors.join(", ")}
`
            return
        })
        .filter(Boolean)
var variables = import_zod2.z.object({
    DATABASE_URL: import_zod2.z.string().url(),
    PORT: import_zod2.z.string(),
    JWT_SECRET: import_zod2.z.string(),
    API_URL: import_zod2.z.string().url().optional(),
    MAIL_USER_EMAIL: import_zod2.z.string(),
    MAIL_APP_PASSWORD: import_zod2.z.string(),
    MAIL_HOST: import_zod2.z.string(),
    MAIL_PORT: import_zod2.z.string(),
    MAIL_SERVICE: import_zod2.z.string(),
    CLOUDINARY_API_KEY: import_zod2.z.string(),
    CLOUDINARY_SECRET: import_zod2.z.string(),
    CLOUDINARY_CLOUD_NAME: import_zod2.z.string(),
    MAP_BOX_TOKEN: import_zod2.z.string(),
    EXPO_PUSH_ACCESS_TOKEN: import_zod2.z.string(),
})
var getVariables = variables.safeParse(process.env)
if (!getVariables.success) {
    console.error(
        "Environment Variables missing:\n",
        ...formatErrors(getVariables.error.format())
    )
    throw new Error("Invalid Environment Variables")
}
var env = getVariables.data

// src/image/upload.ts
var storage = import_multer.default.diskStorage({})
var fileFilter = (_, file, cb) => {
    const acceptedTypes = /jpg|jpeg|png/
    const matchesType = acceptedTypes.test(
        import_path.default.extname(file.originalname)
    )
    const matchesMime = acceptedTypes.test(file.mimetype)
    if (matchesMime && matchesType) {
        return cb(null, true)
    }
    return cb({
        message: "Only images with file ext. jpg, jpeg or png are accepted",
        name: "Image upload failed",
    })
}
var upload = (0, import_multer.default)({ storage, fileFilter })
import_cloudinary.v2.config({
    cloud_name: env.CLOUDINARY_CLOUD_NAME,
    api_key: env.CLOUDINARY_API_KEY,
    api_secret: env.CLOUDINARY_SECRET,
})
var imageUploader = async (req, res) => {
    const { type } = req.body
    if (req.file && type) {
        const image = await import_cloudinary.v2.uploader.upload(
            req.file.path,
            {
                folder: `/Mealy/${type}`,
            }
        )
        const link = image.secure_url.split("/image")[1]
        return res.send({ link: `/image${link}` })
    }
    throw new Error("Image upload failed")
}

// ../../packages/db/src/index.ts
var src_exports = {}
__export(src_exports, {
    prisma: () => prisma,
})

// ../../packages/db/src/client.ts
var client_exports = {}
__export(client_exports, {
    prisma: () => prisma,
})
var import_dotenv = require("dotenv")
var import_client = require("@prisma/client")
__reExport(client_exports, require("@prisma/client"))
;(0, import_dotenv.config)({ path: "../../../.env" })
var prisma =
    global.prisma ||
    new import_client.PrismaClient({
        log:
            process.env.NODE_ENV === "development"
                ? ["query", "error", "warn"]
                : ["error"],
    })
if (process.env.NODE_ENV !== "production") global.prisma = prisma

// ../../packages/db/src/index.ts
__reExport(src_exports, client_exports)

// src/utils/jwt.ts
var import_jsonwebtoken = __toESM(require("jsonwebtoken"))
var signJwt = (payload) =>
    import_jsonwebtoken.default.sign(payload, env.JWT_SECRET)
var verifyJwt = (token) =>
    import_jsonwebtoken.default.verify(token, env.JWT_SECRET)

// src/trpc/events.ts
var import_events = __toESM(require("events"))
var TypedEventEmitterClass = class {
    constructor() {
        this.ee = new import_events.default()
    }
    emit(eventName, ...eventArg) {
        this.ee.emit(eventName, ...eventArg)
    }
    on(eventName, handler2) {
        this.ee.on(eventName, handler2)
    }
    off(eventName, handler2) {
        this.ee.off(eventName, handler2)
    }
}
var TypedEventEmitter = TypedEventEmitterClass

// src/trpc/context.ts
var ee = new TypedEventEmitter()
var createContext = ({ req, res }) => {
    let token
    const getUser = () => {
        const hasToken = req.headers.authorization
        if (hasToken && hasToken.startsWith("Bearer")) {
            try {
                token = hasToken.split(" ")
                const decodedToken = verifyJwt(token[1])
                return decodedToken.id
            } catch (error) {
                req.headers.authorization = ""
                return null
            }
        }
        return null
    }
    let user
    let restaurant
    return {
        prisma,
        req,
        res,
        restaurant,
        ee,
        user,
        userId: getUser(),
    }
}

// src/courier/auth/index.ts
var import_crypto = require("crypto")

// ../../packages/db/src/encrypt.ts
var import_bcrypt = __toESM(require("bcrypt"))
var encryptPassword = async (password) => {
    const saltRounds = 10
    try {
        const salt = await import_bcrypt.default.genSalt(saltRounds)
        const hashedPassword = await import_bcrypt.default.hash(password, salt)
        return hashedPassword
    } catch (error) {
        throw new Error("Failed to hash password")
    }
}
var comparePasswords = async (password, hashedPassword) => {
    try {
        return await import_bcrypt.default.compare(password, hashedPassword)
    } catch (error) {
        throw new Error("Failed to compare password")
    }
}

// src/courier/auth/index.ts
var import_server2 = require("@trpc/server")

// src/utils/mail.ts
var import_nodemailer = __toESM(require("nodemailer"))
var recoverPasswordMail = async ({ from, to, otp }) => {
    const transporter = import_nodemailer.default.createTransport({
        host: env.MAIL_HOST,
        port: +env.MAIL_PORT,
        secure: true,
        service: env.MAIL_SERVICE,
        tls: {
            // do not fail on invalid certs
            rejectUnauthorized: false,
        },
        auth: {
            user: env.MAIL_USER_EMAIL,
            pass: env.MAIL_APP_PASSWORD,
        },
    })
    const mailOptions = {
        from: `"${from}" <mealy@group.com>`,
        to,
        subject: `${from} Account Recovery`,
        html: `<h3>Hello ${to}</h3>
      <p>Please use this OTP code: ${otp} to complete your password reset</p>
      <p>If you are not trying to reset your password, please ignore this mail</P>`,
    }
    await transporter.sendMail(mailOptions)
}

// src/utils/otp.ts
var generateOTP = () => {
    const digits = "0123456789"
    let OTP = ""
    for (let i = 0; i < 4; i++) {
        OTP += digits[Math.floor(Math.random() * 10)]
    }
    return OTP
}
var otpExpires = () => {
    const oldDateObj = /* @__PURE__ */ new Date()
    const newDateObj = oldDateObj
    newDateObj.setTime(oldDateObj.getTime() + 15 * 60 * 1e3)
    return newDateObj
}
var hasOtpExpired = (expireTime) => {
    const time = /* @__PURE__ */ new Date()
    return time > expireTime
}

// src/courier/auth/index.ts
var authRouter = router({
    changePassword: procedure
        .input(changePasswordSchema)
        .mutation(async ({ ctx, input }) => {
            const { email } = input
            let { password } = input
            password = await encryptPassword(password)
            try {
                await ctx.prisma.courier.update({
                    where: { email },
                    data: { courierAuth: { update: { password } } },
                })
                return { message: "Your password has been changed" }
            } catch (error) {
                throw new import_server2.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "Your new password was not saved",
                })
            }
        }),
    confirmOTP: procedure
        .input(confirmOTPSchema)
        .mutation(async ({ ctx, input }) => {
            var _a
            const { email, otp } = input
            try {
                const courier = await ctx.prisma.courier.findUnique({
                    where: { email },
                    include: { courierAuth: true },
                })
                if (
                    !courier ||
                    !((_a = courier.courierAuth) == null ? void 0 : _a.OTP) ||
                    !courier.courierAuth.OTPExpires
                )
                    throw new import_server2.TRPCError({
                        code: "BAD_REQUEST",
                        message: "OTP Error",
                    })
                const hasExpired = hasOtpExpired(courier.courierAuth.OTPExpires)
                if (hasExpired)
                    throw new import_server2.TRPCError({
                        code: "BAD_REQUEST",
                        message: "OTP has expired please generate a new one",
                    })
                const otpMatches = courier.courierAuth.OTP.toString() === otp
                if (!otpMatches)
                    throw new import_server2.TRPCError({
                        code: "BAD_REQUEST",
                        message:
                            "OTP does not match, please check the values provided",
                    })
                return true
            } catch (error) {
                throw new import_server2.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "OTP verification failed",
                })
            }
        }),
    emailExist: procedure
        .input(emailExistSchema)
        .mutation(async ({ ctx, input }) => {
            try {
                const { email } = input
                const emailUsed = await ctx.prisma.courier.findUnique({
                    where: { email },
                })
                return { emailUsed: !!emailUsed }
            } catch (error) {
                throw new import_server2.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to create your account",
                })
            }
        }),
    login: procedure.input(loginSchema).mutation(async ({ ctx, input }) => {
        const { email, password } = input
        try {
            const courier = await ctx.prisma.courier.findUnique({
                where: { email },
            })
            if (!courier)
                throw new import_server2.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "Wrong username or password",
                })
            const courierAuth = await ctx.prisma.courierAuth.findUnique({
                where: { courierId: courier.id },
            })
            if (!courierAuth || !courierAuth.password)
                throw new import_server2.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "Wrong username or password",
                })
            const passwordMatches = await comparePasswords(
                password,
                courierAuth.password
            )
            if (!passwordMatches)
                throw new import_server2.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "Wrong username or password",
                })
            const token = signJwt({ id: courier.id })
            return { ...courier, token }
        } catch (error) {
            throw new import_server2.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to login",
            })
        }
    }),
    loginOauth: procedure
        .input(oauthSchema)
        .mutation(async ({ ctx, input }) => {
            const { email } = input
            const { prisma: prisma2 } = ctx
            try {
                const courier = await prisma2.courier.findFirst({
                    where: { email },
                })
                if (!courier)
                    throw new import_server2.TRPCError({
                        code: "UNAUTHORIZED",
                        message: "Account with that email does not exist",
                    })
                const token = signJwt({ id: courier.id })
                return { ...courier, token }
            } catch (error) {
                throw new import_server2.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to login",
                })
            }
        }),
    sendOtp: procedure.input(sendOTPSchema).mutation(async ({ ctx, input }) => {
        const { email } = input
        const courier = await ctx.prisma.courier.findUnique({
            where: { email },
        })
        if (courier) {
            const otp = generateOTP()
            try {
                await ctx.prisma.courierAuth.update({
                    where: { courierId: courier.id },
                    data: {
                        OTP: otp,
                        OTPExpires: otpExpires(),
                    },
                })
                await recoverPasswordMail({
                    from: "Mealy Driver",
                    to: email,
                    otp,
                })
            } catch (error) {
                throw new import_server2.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "OTP ERROR",
                })
            }
        }
    }),
    signup: procedure.input(signUpSchema).mutation(async ({ ctx, input }) => {
        const {
            city,
            country,
            email,
            fullName,
            phone,
            state,
            street,
            lat,
            lng,
        } = input
        let { password } = input
        try {
            password = await encryptPassword(password)
            const courier = await ctx.prisma.courier.create({
                data: {
                    city,
                    country,
                    email,
                    fullName,
                    phone,
                    state,
                    lat,
                    lng,
                    street,
                    courierAuth: { create: { password } },
                },
            })
            const token = signJwt({ id: courier.id })
            return { ...courier, token }
        } catch (error) {
            throw new import_server2.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to create your account",
            })
        }
    }),
    signUpOauth: procedure
        .input(signUpOauthSchema)
        .mutation(async ({ ctx, input }) => {
            const {
                city,
                country,
                email,
                fullName,
                phone,
                state,
                street,
                lat,
                lng,
            } = input
            const { prisma: prisma2 } = ctx
            try {
                const randomString = (0, import_crypto.randomBytes)(16)
                const password = await encryptPassword(
                    randomString.toString("hex")
                )
                const courier = await prisma2.courier.create({
                    data: {
                        city,
                        country,
                        email,
                        fullName,
                        phone,
                        state,
                        lat,
                        lng,
                        street,
                        courierAuth: { create: { password } },
                    },
                })
                const token = signJwt({ id: courier.id })
                return { ...courier, token }
            } catch (error) {
                throw new import_server2.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to create your account",
                })
            }
        }),
})

// src/courier/location/index.ts
var import_axios = __toESM(require("axios"))
var import_url = require("url")
var import_server4 = require("@trpc/server")

// src/trpc/middleware.ts
var import_server3 = require("@trpc/server")
var userLoggedIn = middleware(async ({ ctx, next }) => {
    const { prisma: prisma2, userId } = ctx
    if (!userId)
        throw new import_server3.TRPCError({
            code: "UNAUTHORIZED",
            message: "Not authorized, please login",
        })
    try {
        const user = await prisma2.user.findFirst({
            where: { id: userId },
            include: { addresses: { where: { isDefault: true } } },
        })
        if (!user)
            throw new import_server3.TRPCError({
                code: "UNAUTHORIZED",
                message: "Not authorized, please login",
            })
        return await next({ ctx: { ...ctx, user } })
    } catch (error) {
        throw new import_server3.TRPCError({
            code: "UNAUTHORIZED",
            message: "Not authorized, please login",
        })
    }
})
var restaurantLoggedIn = middleware(async ({ ctx, next }) => {
    const { prisma: prisma2, userId } = ctx
    if (!userId)
        throw new import_server3.TRPCError({
            code: "UNAUTHORIZED",
            message: "Not authorized, please login",
        })
    try {
        const restaurant = await prisma2.restaurant.findFirst({
            where: { id: userId },
        })
        if (!restaurant)
            throw new import_server3.TRPCError({
                code: "UNAUTHORIZED",
                message: "Not authorized, please login",
            })
        return await next({ ctx: { ...ctx, restaurant } })
    } catch (error) {
        throw new import_server3.TRPCError({
            code: "UNAUTHORIZED",
            message: "Not authorized, please login",
        })
    }
})
var courierLoggedIn = middleware(async ({ ctx, next }) => {
    const { prisma: prisma2, userId } = ctx
    if (!userId)
        throw new import_server3.TRPCError({
            code: "UNAUTHORIZED",
            message: "Not authorized, please login",
        })
    try {
        const courier = await prisma2.courier.findFirst({
            where: { id: userId },
        })
        if (!courier)
            throw new import_server3.TRPCError({
                code: "UNAUTHORIZED",
                message: "Not authorized, please login",
            })
        return await next({ ctx: { ...ctx, courier } })
    } catch (error) {
        throw new import_server3.TRPCError({
            code: "UNAUTHORIZED",
            message: "Not authorized, please login",
        })
    }
})
var userAuthProcedure = procedure.use(userLoggedIn)
var restaurantAuthProcedure = procedure.use(restaurantLoggedIn)
var courierAuthProcedure = procedure.use(courierLoggedIn)

// src/courier/location/index.ts
var { MAP_BOX_TOKEN } = env
var locationRouter = router({
    getRoute: courierAuthProcedure
        .input(getRouteSchema)
        .query(async ({ input }) => {
            var _a, _b, _c
            const {
                courierLat,
                courierLng,
                customerLat,
                customerLng,
                restaurantLat,
                restaurantLng,
                deliveryStatus,
            } = input
            const params = new import_url.URLSearchParams({
                geometries: "geojson",
                access_token: MAP_BOX_TOKEN,
            }).toString()
            const baseUrl =
                "https://api.mapbox.com/directions/v5/mapbox/driving/"
            let url = `${baseUrl}${courierLng},${courierLat};${restaurantLng},${restaurantLat};${customerLng},${customerLat}`
            if (deliveryStatus === "TO_DELIVER") {
                url = `${baseUrl}${courierLng},${courierLat};${customerLng},${customerLat}`
            }
            if (deliveryStatus === "TO_PICK_UP") {
                url = `${baseUrl}${courierLng},${courierLat};${restaurantLng},${restaurantLat}`
            }
            try {
                const { data } = await import_axios.default.get(
                    `${url}?${params}`
                )
                if (
                    (_c =
                        (_b =
                            (_a = data == null ? void 0 : data.routes) == null
                                ? void 0
                                : _a[0].geometry) == null
                            ? void 0
                            : _b.coordinates) == null
                        ? void 0
                        : _c[0]
                ) {
                    return { route: data.routes[0].geometry }
                }
                throw new import_server4.TRPCError({
                    code: "NOT_FOUND",
                    message: "Failed to find a route for your delivery",
                })
            } catch (error) {
                throw new import_server4.TRPCError({
                    code: "NOT_FOUND",
                    message: "Failed to find a route for your delivery",
                })
            }
        }),
})

// src/courier/orders/index.ts
var import_server5 = require("@trpc/server")
var import_observable = require("@trpc/server/observable")

// src/utils/expo-push.ts
var import_expo_server_sdk = require("expo-server-sdk")
var { EXPO_PUSH_ACCESS_TOKEN } = env
var expo = new import_expo_server_sdk.Expo({
    accessToken: EXPO_PUSH_ACCESS_TOKEN,
})
var sendPushMessage = async ({ pushToken, body, title, orderId }) => {
    if (import_expo_server_sdk.Expo.isExpoPushToken(pushToken)) {
        const messages = [
            {
                to: pushToken,
                sound: "default",
                body,
                title,
                data: { url: `mealy://food/orders/${orderId}` },
            },
        ]
        const chunks = expo.chunkPushNotifications(messages)
        try {
            await expo.sendPushNotificationsAsync(chunks[0])
        } catch (error) {
            console.error(error)
        }
    }
}

// src/courier/orders/index.ts
var ordersRouter = router({
    acceptOrder: courierAuthProcedure
        .input(orderDetailsSchema)
        .mutation(async ({ ctx, input }) => {
            const { courier, prisma: prisma2 } = ctx
            const { id } = input
            try {
                const isAvailable = await prisma2.order.findFirst({
                    where: { id, status: "ReadyForPickUp", courierId: null },
                    include: { restaurant: { select: { name: true } } },
                })
                if (isAvailable) {
                    await prisma2.order.update({
                        where: { id },
                        data: { courierId: courier.id },
                    })
                    return {
                        message: "You have accepted to deliver this order",
                    }
                }
                throw new import_server5.TRPCError({
                    code: "NOT_FOUND",
                    message:
                        "This order does not exist or has been picked by another courier",
                })
            } catch (error) {
                throw new import_server5.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to accept the order",
                })
            }
        }),
    deliverOrder: courierAuthProcedure
        .input(orderDetailsSchema)
        .mutation(async ({ ctx, input }) => {
            const { prisma: prisma2 } = ctx
            const { id } = input
            try {
                const order = await prisma2.order.update({
                    where: { id },
                    data: { status: "Delivered" },
                    include: { restaurant: { select: { name: true } } },
                })
                await sendPushMessage({
                    pushToken: order.pushToken ?? "",
                    orderId: order.id,
                    title: order.restaurant.name,
                    body: `Your order has been delivered`,
                })
                return { message: "You have delivered this order" }
            } catch (error) {
                throw new import_server5.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to update the order",
                })
            }
        }),
    getAllOrders: courierAuthProcedure.query(async ({ ctx }) => {
        const { courier, prisma: prisma2 } = ctx
        const { country, state } = courier
        const restaurantWithOrders = await prisma2.restaurant.findMany({
            where: {
                state,
                country,
                orders: { some: { status: "ReadyForPickUp", courierId: null } },
            },
            include: {
                _count: {
                    select: {
                        orders: {
                            where: {
                                status: "ReadyForPickUp",
                                courierId: null,
                            },
                        },
                    },
                },
                orders: {
                    where: { status: "ReadyForPickUp", courierId: null },
                    select: {
                        deliveryAddress: true,
                        deliveryFee: true,
                        id: true,
                    },
                },
            },
        })
        return restaurantWithOrders
    }),
    getOrderDetails: courierAuthProcedure
        .input(orderDetailsSchema)
        .query(async ({ ctx, input }) => {
            const { prisma: prisma2, courier } = ctx
            const { id } = input
            const order = await prisma2.order.findFirst({
                where: {
                    id,
                    OR: [{ courierId: null }, { courierId: courier.id }],
                },
                include: {
                    restaurant: {
                        select: {
                            city: true,
                            state: true,
                            country: true,
                            street: true,
                            name: true,
                            lat: true,
                            lng: true,
                            phone: true,
                        },
                    },
                    foodOrdered: {
                        select: {
                            quantity: true,
                            food: { select: { name: true } },
                        },
                    },
                    user: {
                        select: { fullName: true, phone: true },
                    },
                },
            })
            return order
        }),
    pickUpOrder: courierAuthProcedure
        .input(orderDetailsSchema)
        .mutation(async ({ ctx, input }) => {
            const { prisma: prisma2 } = ctx
            const { id } = input
            try {
                const order = await prisma2.order.update({
                    where: { id },
                    data: { status: "OnRoute" },
                    include: { restaurant: { select: { name: true } } },
                })
                await sendPushMessage({
                    pushToken: order.pushToken ?? "",
                    orderId: order.id,
                    title: order.restaurant.name,
                    body: `Your order has been picked up by a courier`,
                })
                return { message: "Order status has successfully been update" }
            } catch (error) {
                throw new import_server5.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to update the order status",
                })
            }
        }),
    newPickUp: procedure.input(newOrderSub).subscription(({ ctx, input }) => {
        const { ee: ee2 } = ctx
        const { country, state } = input
        return (0, import_observable.observable)((emit) => {
            const send = (ResState, resCountry) => {
                if (ResState === state && resCountry === country)
                    emit.next({ message: "You have a new pick up order" })
            }
            ee2.on("ORDER_READY", send)
            return () => {
                ee2.off("ORDER_READY", send)
            }
        })
    }),
})

// src/courier/profile/index.ts
var import_server6 = require("@trpc/server")
var profileRouter = router({
    allDeliveries: courierAuthProcedure
        .input(allDeliveriesSchema)
        .query(async ({ ctx, input }) => {
            const { limit, cursor } = input
            const { prisma: prisma2, courier } = ctx
            try {
                const deliveries = await prisma2.order.findMany({
                    where: { courierId: courier.id, status: "Delivered" },
                    take: limit + 1,
                    cursor: cursor ? { id: cursor } : void 0,
                    select: {
                        id: true,
                        deliveryFee: true,
                        deliveryAddress: true,
                        restaurant: {
                            select: {
                                name: true,
                                logo: true,
                                state: true,
                                street: true,
                                country: true,
                                city: true,
                            },
                        },
                    },
                    orderBy: [{ createdAt: "desc" }],
                })
                let nextCursor
                if (deliveries.length > limit) {
                    const nextItem = deliveries.pop()
                    nextCursor = nextItem.id
                }
                return { deliveries, nextCursor }
            } catch (error) {
                throw new import_server6.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to get all your orders",
                })
            }
        }),
    changePassword: courierAuthProcedure
        .input(changeUserPasswordSchema)
        .mutation(async ({ ctx, input }) => {
            let { password } = input
            const { courier, prisma: prisma2 } = ctx
            password = await encryptPassword(password)
            try {
                await prisma2.courierAuth.update({
                    where: { courierId: courier.id },
                    data: { password },
                })
                return { message: "Your password has been change successfully" }
            } catch (error) {
                throw new import_server6.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to change your password",
                })
            }
        }),
    deleteAccount: courierAuthProcedure.mutation(async ({ ctx }) => {
        const { courier, prisma: prisma2 } = ctx
        try {
            await prisma2.courier.delete({ where: { id: courier.id } })
            return { message: "Your account has successfully been deleted" }
        } catch (error) {
            throw new import_server6.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to delete your account",
            })
        }
    }),
    editAddress: courierAuthProcedure
        .input(newAddressSchema)
        .mutation(async ({ ctx, input }) => {
            const { courier, prisma: prisma2 } = ctx
            const { city, country, lat, lng, state, street } = input
            try {
                await prisma2.courier.update({
                    where: { id: courier.id },
                    data: { city, country, lat, lng, state, street },
                })
                return { message: "Your address details were updated" }
            } catch (error) {
                throw new import_server6.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to update your details",
                })
            }
        }),
    editProfile: courierAuthProcedure
        .input(userProfileSchema)
        .mutation(async ({ ctx, input }) => {
            const { courier, prisma: prisma2 } = ctx
            const { fullName, phone } = input
            try {
                await prisma2.courier.update({
                    where: { id: courier.id },
                    data: { fullName, phone },
                })
                return { message: "Your account has successfully been updated" }
            } catch (error) {
                throw new import_server6.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to update your account",
                })
            }
        }),
})

// src/courier/index.ts
var courierRouter = router({
    auth: authRouter,
    location: locationRouter,
    orders: ordersRouter,
    profile: profileRouter,
})

// src/food/auth/index.ts
var import_crypto2 = require("crypto")
var import_server7 = require("@trpc/server")
var authRouter2 = router({
    changePassword: procedure
        .input(changePasswordSchema)
        .mutation(async ({ ctx, input }) => {
            const { email } = input
            let { password } = input
            password = await encryptPassword(password)
            try {
                await ctx.prisma.user.update({
                    where: { email },
                    data: {
                        userAuth: {
                            update: { password, OTP: null, OTPExpires: null },
                        },
                    },
                })
                return { message: passwordChangedMessage }
            } catch (error) {
                throw new import_server7.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: passwordNotChangedMessage,
                })
            }
        }),
    confirmOTP: procedure
        .input(confirmOTPSchema)
        .mutation(async ({ ctx, input }) => {
            var _a
            const { email, otp } = input
            const user = await ctx.prisma.user.findUnique({
                where: { email },
                include: { userAuth: true },
            })
            if (
                !user ||
                !((_a = user.userAuth) == null ? void 0 : _a.OTP) ||
                !user.userAuth.OTPExpires
            )
                throw new import_server7.TRPCError({
                    code: "BAD_REQUEST",
                    message: "OTP Error",
                })
            const hasExpired = hasOtpExpired(user.userAuth.OTPExpires)
            if (hasExpired)
                throw new import_server7.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "OTP has expired, please generate a new one",
                })
            const otpMatches = user.userAuth.OTP.toString() === otp
            if (!otpMatches)
                throw new import_server7.TRPCError({
                    code: "UNAUTHORIZED",
                    message:
                        "OTP does not match, please check the values provided",
                })
            return true
        }),
    emailExist: procedure
        .input(emailExistSchema)
        .mutation(async ({ ctx, input }) => {
            const emailUsed = await ctx.prisma.user.findUnique({
                where: { email: input.email },
            })
            if (emailUsed)
                throw new import_server7.TRPCError({
                    code: "UNAUTHORIZED",
                    message: emailAlreadyUsed,
                })
            return { emailUsed: false }
        }),
    login: procedure.input(loginSchema).mutation(async ({ input, ctx }) => {
        const user = await ctx.prisma.user.findFirst({
            where: { email: input.email },
            include: { addresses: true },
        })
        if (!user)
            throw new import_server7.TRPCError({
                code: "UNAUTHORIZED",
                message: wrongEmailOrPassword,
            })
        const userAuth = await ctx.prisma.userAuth.findUnique({
            where: { userId: user.id },
        })
        if (!userAuth || !userAuth.password)
            throw new import_server7.TRPCError({
                code: "NOT_FOUND",
                message: wrongEmailOrPassword,
            })
        const passwordMatches = await comparePasswords(
            input.password,
            userAuth.password
        )
        if (!passwordMatches)
            throw new import_server7.TRPCError({
                code: "NOT_FOUND",
                message: wrongEmailOrPassword,
            })
        const token = signJwt({ id: user.id })
        return { ...user, token }
    }),
    loginOauth: procedure
        .input(oauthSchema)
        .mutation(async ({ ctx, input }) => {
            const { email } = input
            const { prisma: prisma2 } = ctx
            const user = await prisma2.user.findFirst({
                where: { email },
                include: { addresses: true },
            })
            if (!user)
                throw new import_server7.TRPCError({
                    code: "NOT_FOUND",
                    message: "Account with that email does not exist",
                })
            const token = signJwt({ id: user.id })
            return { ...user, token }
        }),
    sendOTP: procedure.input(sendOTPSchema).mutation(async ({ ctx, input }) => {
        const { email } = input
        const user = await ctx.prisma.user.findUnique({ where: { email } })
        if (user) {
            const OTP = generateOTP()
            try {
                await ctx.prisma.userAuth.update({
                    where: {
                        userId: user.id,
                    },
                    data: {
                        OTP,
                        OTPExpires: otpExpires(),
                    },
                })
                await recoverPasswordMail({
                    from: "Mealy Food",
                    to: user.email,
                    otp: OTP,
                })
            } catch (error) {
                throw new import_server7.TRPCError({
                    code: "BAD_REQUEST",
                    message: failedOTP,
                })
            }
        }
    }),
    signUp: procedure.input(signUpSchema).mutation(async ({ input, ctx }) => {
        let { password } = input
        const {
            city,
            country,
            email,
            fullName,
            phone,
            state,
            street,
            lat,
            lng,
        } = input
        try {
            password = await encryptPassword(password)
            const user = await ctx.prisma.user.create({
                data: {
                    email,
                    fullName,
                    phone,
                    userAuth: { create: { password } },
                    addresses: {
                        create: {
                            city,
                            country,
                            state,
                            street,
                            isDefault: true,
                            lat,
                            lng,
                        },
                    },
                },
                include: { addresses: true },
            })
            const token = signJwt({ id: user.id })
            return { ...user, token }
        } catch (error) {
            throw new import_server7.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to create your account",
            })
        }
    }),
    signUpOauth: procedure
        .input(signUpOauthSchema)
        .mutation(async ({ ctx, input }) => {
            const {
                city,
                country,
                email,
                fullName,
                phone,
                state,
                street,
                lat,
                lng,
            } = input
            try {
                const randomString = (0, import_crypto2.randomBytes)(16)
                const password = await encryptPassword(
                    randomString.toString("hex")
                )
                const user = await ctx.prisma.user.create({
                    data: {
                        email,
                        fullName,
                        phone,
                        userAuth: { create: { password } },
                        addresses: {
                            create: {
                                city,
                                country,
                                state,
                                street,
                                isDefault: true,
                                lat,
                                lng,
                            },
                        },
                    },
                    include: { addresses: true },
                })
                if (!user)
                    throw new import_server7.TRPCError({
                        code: "INTERNAL_SERVER_ERROR",
                        message: "Failed to create your account",
                    })
                const token = signJwt({ id: user.id })
                return { ...user, token }
            } catch (error) {
                throw new import_server7.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to create your account",
                })
            }
        }),
})

// src/food/basket/index.ts
var import_server8 = require("@trpc/server")
var basketRouter = router({
    addToBasket: userAuthProcedure
        .input(addToBasketSchema)
        .mutation(async ({ ctx, input }) => {
            const { prisma: prisma2, user } = ctx
            const { quantity, restaurantId, foodId, basketId, basketFoodId } =
                input
            try {
                await prisma2.basket.upsert({
                    where: { id: basketId ?? "0" },
                    create: {
                        restaurantId,
                        userId: user.id,
                        basketFood: {
                            create: {
                                quantity,
                                food: { connect: { id: foodId } },
                            },
                        },
                    },
                    update: {
                        basketFood: {
                            upsert: {
                                where: { id: basketFoodId ?? "0" },
                                create: {
                                    quantity,
                                    food: { connect: { id: foodId } },
                                },
                                update: {
                                    quantity,
                                    food: { connect: { id: foodId } },
                                },
                            },
                        },
                    },
                })
                return { message: "Food Successfully added to basket" }
            } catch (error) {
                throw new import_server8.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to add food item to basket",
                })
            }
        }),
    getBasket: userAuthProcedure
        .input(getBasketSchema)
        .query(async ({ ctx, input }) => {
            const { restaurantId } = input
            const { prisma: prisma2, user } = ctx
            const basket = await prisma2.basket.findFirst({
                where: { restaurantId, userId: user.id },
                include: {
                    basketFood: {
                        include: {
                            food: {
                                select: {
                                    id: true,
                                    name: true,
                                    price: true,
                                    discountPrice: true,
                                    image: true,
                                },
                            },
                        },
                        orderBy: [{ createdAt: "asc" }],
                    },
                },
            })
            return basket
        }),
    removeFromBasket: userAuthProcedure
        .input(removeFromBasket)
        .mutation(async ({ ctx, input }) => {
            const { basketFoodId, basketId } = input
            const { prisma: prisma2 } = ctx
            try {
                const basketFoods = await prisma2.basketFood.findMany({
                    where: { basketId },
                })
                if (basketFoods.length > 1) {
                    await prisma2.basketFood.delete({
                        where: { id: basketFoodId },
                    })
                } else {
                    await prisma2.basket.delete({ where: { id: basketId } })
                }
                return { message: "Food Successfully removed from basket" }
            } catch (error) {
                throw new import_server8.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to remove food from basket",
                })
            }
        }),
})

// src/food/main/index.ts
var import_server9 = require("@trpc/server")
var mainRouter = router({
    addComment: userAuthProcedure
        .input(addCommentSchema)
        .mutation(async ({ ctx, input }) => {
            const { ratings, comment, id, restaurantId } = input
            const { prisma: prisma2, user } = ctx
            try {
                const data = await prisma2.$transaction(async (prim) => {
                    var _a
                    await prim.restaurantRatings.upsert({
                        where: { id: id ?? "" },
                        create: {
                            ratings,
                            comment,
                            restaurantId,
                            userId: user.id,
                        },
                        update: { ratings, comment },
                    })
                    const averageRating = await prim.restaurantRatings.groupBy({
                        where: { restaurantId },
                        by: ["restaurantId"],
                        _avg: { ratings: true },
                    })
                    let rating =
                        // eslint-disable-next-line no-underscore-dangle
                        (_a = averageRating[0]._avg.ratings) == null
                            ? void 0
                            : _a.toFixed(1)
                    if (rating) rating = +rating
                    else rating = 0
                    await prim.restaurant.update({
                        where: { id: restaurantId },
                        data: { ratings: rating },
                    })
                    return { message: "Your ratings been updated" }
                })
                return data
            } catch (error) {
                throw new import_server9.TRPCError({
                    code: "NOT_FOUND",
                    message: "Failed to add your rating",
                })
            }
        }),
    allRestaurants: userAuthProcedure
        .input(homeDataSchema)
        .query(async ({ ctx, input }) => {
            const { prisma: prisma2 } = ctx
            const { state, country } = input
            try {
                const restaurants = await prisma2.restaurant.findMany({
                    where: { country, state },
                })
                return restaurants
            } catch (error) {
                throw new import_server9.TRPCError({
                    code: "NOT_FOUND",
                    message: "Failed to get all Restaurants",
                })
            }
        }),
    deleteComment: userAuthProcedure
        .input(deleteCommentSchema)
        .mutation(async ({ ctx, input }) => {
            const { commentId, restaurantId } = input
            try {
                const data = await ctx.prisma.$transaction(async (prim) => {
                    var _a
                    await prim.restaurantRatings.delete({
                        where: { id: commentId },
                    })
                    const averageRating = await prim.restaurantRatings.groupBy({
                        where: { restaurantId },
                        by: ["restaurantId"],
                        _avg: { ratings: true },
                    })
                    let rating =
                        // eslint-disable-next-line no-underscore-dangle
                        (_a = averageRating[0]._avg.ratings) == null
                            ? void 0
                            : _a.toFixed(1)
                    if (rating) rating = +rating
                    else rating = 0
                    await prim.restaurant.update({
                        where: { id: restaurantId },
                        data: { ratings: rating },
                    })
                    return {
                        message: "Your comment has been deleted successfully",
                    }
                })
                return data
            } catch (error) {
                throw new import_server9.TRPCError({
                    code: "NOT_FOUND",
                    message: "Failed to delete your comment",
                })
            }
        }),
    getComment: userAuthProcedure
        .input(getUserComment)
        .query(async ({ ctx, input }) => {
            const comment = await ctx.prisma.restaurantRatings.findFirst({
                where: { id: input.commentId },
            })
            return comment
        }),
    getRestaurantRatings: userAuthProcedure
        .input(restaurantIdSchema)
        .query(async ({ ctx, input }) => {
            const { id } = input
            const { prisma: prisma2, user } = ctx
            try {
                const RestaurantRatings2 = await prisma2.restaurant.findFirst({
                    where: { id },
                    select: {
                        ratings: true,
                        _count: { select: { userRatings: true } },
                    },
                })
                const ratingsAndCount = await prisma2.restaurantRatings.groupBy(
                    {
                        by: ["ratings"],
                        where: { restaurantId: id },
                        _count: { ratings: true },
                        orderBy: { ratings: "desc" },
                    }
                )
                const userComment = await prisma2.restaurantRatings.findFirst({
                    where: { restaurantId: id, userId: user.id },
                    select: { id: true },
                })
                return {
                    RestaurantRatings: RestaurantRatings2,
                    ratingsAndCount,
                    userComment,
                }
            } catch (error) {
                throw new import_server9.TRPCError({
                    code: "NOT_FOUND",
                    message: "Failed to get all Restaurant Ratings",
                })
            }
        }),
    getUsersComments: userAuthProcedure
        .input(getRestaurantCommentsSchema)
        .query(async ({ ctx, input }) => {
            const { prisma: prisma2 } = ctx
            const { id, limit, cursor, rating } = input
            try {
                const comments = await prisma2.restaurantRatings.findMany({
                    where: {
                        restaurantId: id,
                        ratings: rating ?? void 0,
                    },
                    take: limit + 1,
                    cursor: cursor ? { id: cursor } : void 0,
                    orderBy: [{ updatedAt: "desc" }],
                    include: { user: { select: { fullName: true } } },
                })
                let nextCursor
                if (comments.length > limit) {
                    const nextItem = comments.pop()
                    nextCursor = nextItem.id
                }
                return { comments, nextCursor }
            } catch (error) {
                throw new import_server9.TRPCError({
                    code: "NOT_FOUND",
                    message: "Failed to get all Restaurant Comments",
                })
            }
        }),
    restaurantDetails: userAuthProcedure
        .input(restaurantIdSchema)
        .query(async ({ ctx, input }) => {
            const { id } = input
            try {
                const restaurant = await ctx.prisma.restaurant.findFirst({
                    where: { id },
                    include: { foods: true },
                })
                return restaurant
            } catch (error) {
                throw new import_server9.TRPCError({
                    code: "NOT_FOUND",
                    message: "Failed to get all restaurant details",
                })
            }
        }),
    specialOffers: userAuthProcedure
        .input(homeDataSchema)
        .query(async ({ ctx, input }) => {
            const { prisma: prisma2 } = ctx
            const { country, state } = input
            try {
                const offers = await prisma2.food.findMany({
                    where: {
                        discountPercentage: { gt: 0 },
                        restaurant: { country, state },
                    },
                    orderBy: [
                        { discountPercentage: "desc" },
                        { discountPrice: "desc" },
                    ],
                    take: 6,
                    include: { restaurant: true },
                })
                return offers
            } catch (error) {
                throw new import_server9.TRPCError({
                    code: "NOT_FOUND",
                    message: "Failed to find all special offers",
                })
            }
        }),
})

// src/food/orders/index.ts
var import_server10 = require("@trpc/server")
var orderRouter = router({
    allOrders: userAuthProcedure
        .input(allOrdersSchema)
        .query(async ({ ctx, input }) => {
            const { limit, cursor } = input
            const { prisma: prisma2, user } = ctx
            try {
                const orders = await prisma2.order.findMany({
                    where: { userId: user.id },
                    take: limit + 1,
                    cursor: cursor ? { id: cursor } : void 0,
                    select: {
                        id: true,
                        status: true,
                        createdAt: true,
                        restaurant: true,
                        totalPriceWithDelivery: true,
                    },
                    orderBy: [{ createdAt: "desc" }],
                })
                let nextCursor
                if (orders.length > limit) {
                    const nextItem = orders.pop()
                    nextCursor = nextItem.id
                }
                return { orders, nextCursor }
            } catch (error) {
                throw new import_server10.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to get all your orders",
                })
            }
        }),
    createOrder: userAuthProcedure
        .input(createOrderSchema)
        .mutation(async ({ ctx, input }) => {
            const {
                restaurantId,
                basketId,
                deliveryFee,
                totalPrice,
                paymentMethod,
                deliveryLat,
                deliveryLng,
                pushToken,
            } = input
            const { prisma: prisma2, user, ee: ee2 } = ctx
            const userAddress = await prisma2.addresses.findFirst({
                where: { userId: user.id, isDefault: true },
            })
            if (!userAddress) {
                throw new import_server10.TRPCError({
                    code: "NOT_FOUND",
                    message:
                        "Your address was not found, please select an address",
                })
            }
            const restaurant = await prisma2.restaurant.findFirst({
                where: { id: restaurantId },
            })
            if (!restaurant) {
                throw new import_server10.TRPCError({
                    code: "NOT_FOUND",
                    message:
                        "The restaurant you are ordering from does not exist",
                })
            }
            let isOpen = false
            if (restaurant.alwaysOpen) isOpen = true
            else if (restaurant.closingTime && restaurant.openingTime) {
                const timeNow = /* @__PURE__ */ new Date().getHours()
                const closingHour =
                    parseInt(restaurant.closingTime.split(":")[0], 10) + 12
                const openingHour = parseInt(
                    restaurant.openingTime.split(":")[0],
                    10
                )
                isOpen = openingHour <= timeNow && timeNow < closingHour
            } else isOpen = false
            if (!isOpen) {
                throw new import_server10.TRPCError({
                    code: "FORBIDDEN",
                    message:
                        "This restaurant is not open please try again during their open hours",
                })
            }
            const { city, country, state, street } = userAddress
            const theSameArea =
                userAddress.state === restaurant.state &&
                userAddress.country === restaurant.country
            if (!theSameArea) {
                throw new import_server10.TRPCError({
                    code: "FORBIDDEN",
                    message:
                        "This restaurant does not operate in the address you provided",
                })
            }
            const basket = await prisma2.basket.findFirst({
                where: { id: basketId },
                include: { basketFood: { include: { food: true } } },
            })
            if (!basket) {
                throw new import_server10.TRPCError({
                    code: "NOT_FOUND",
                    message: "Your basket details was not found",
                })
            }
            const createOrder = prisma2.order.create({
                data: {
                    restaurantId,
                    deliveryFee,
                    deliveryLat,
                    deliveryLng,
                    totalPriceWithDelivery: totalPrice,
                    totalPrice: totalPrice - deliveryFee,
                    isPaid: true,
                    pushToken,
                    deliveryAddress: `${street}, ${city}, ${state}, ${country}`,
                    payment: paymentMethod,
                    userId: user.id,
                    foodOrdered: {
                        createMany: {
                            data: basket.basketFood.map((basketItem) => {
                                const { quantity, foodId, food } = basketItem
                                const {
                                    price,
                                    discountPrice,
                                    discountPercentage,
                                } = food
                                const priceUsed =
                                    discountPrice > 0 ? discountPrice : price
                                return {
                                    quantity,
                                    foodId,
                                    orderDiscountPrice: discountPrice,
                                    orderPrice: price,
                                    orderDiscountPercentage: discountPercentage,
                                    foodTotalPrice: quantity * priceUsed,
                                }
                            }),
                        },
                    },
                },
            })
            const deleteBasket = prisma2.basket.delete({
                where: { id: basketId },
            })
            const tran = await prisma2.$transaction([createOrder, deleteBasket])
            ee2.emit("ORDER_CREATED", restaurant.id, tran[0].id)
            return { message: orderCreatedMessage }
        }),
    orderDetails: userAuthProcedure
        .input(orderDetailsSchema)
        .query(async ({ ctx, input }) => {
            const { id } = input
            const { prisma: prisma2, userId } = ctx
            try {
                const order = await prisma2.order.findFirst({
                    where: { id, userId },
                    include: {
                        foodOrdered: {
                            include: { food: { select: { name: true } } },
                        },
                        courier: { select: { fullName: true } },
                        user: { select: { fullName: true, phone: true } },
                        restaurant: true,
                    },
                })
                return order
            } catch (error) {
                throw new import_server10.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to get the order details",
                })
            }
        }),
})

// src/food/profile/index.ts
var import_server11 = require("@trpc/server")
var profileRouter2 = router({
    addAddress: userAuthProcedure
        .input(newAddressSchema)
        .mutation(async ({ ctx, input }) => {
            const { city, country, state, street, lat, lng } = input
            const { prisma: prisma2, user } = ctx
            try {
                const address = await prisma2.addresses.create({
                    data: {
                        city,
                        country,
                        state,
                        street,
                        lat,
                        lng,
                        user: { connect: { id: user.id } },
                    },
                })
                return { message: "Your new address has been saved", address }
            } catch (error) {
                throw new import_server11.TRPCError({
                    message: "Failed to add a new address",
                    code: "INTERNAL_SERVER_ERROR",
                })
            }
        }),
    changePassword: userAuthProcedure
        .input(changeUserPasswordSchema)
        .mutation(async ({ ctx, input }) => {
            let { password } = input
            const { prisma: prisma2, user } = ctx
            password = await encryptPassword(password)
            try {
                await prisma2.userAuth.update({
                    where: { userId: user.id },
                    data: { password },
                })
                return {
                    message: "Your password has been changed successfully",
                }
            } catch (error) {
                throw new import_server11.TRPCError({
                    message: "Failed to update your password",
                    code: "INTERNAL_SERVER_ERROR",
                })
            }
        }),
    defaultAddress: userAuthProcedure
        .input(defaultAddressSchema)
        .mutation(async ({ ctx, input }) => {
            const { id, defaultId } = input
            const { prisma: prisma2 } = ctx
            try {
                await prisma2.$transaction([
                    prisma2.addresses.update({
                        where: { id: defaultId },
                        data: { isDefault: false },
                    }),
                    prisma2.addresses.update({
                        where: { id },
                        data: { isDefault: true },
                    }),
                ])
                return { message: "Your default location has been updated" }
            } catch (error) {
                throw new import_server11.TRPCError({
                    message: "Failed to update your default address",
                    code: "INTERNAL_SERVER_ERROR",
                })
            }
        }),
    deleteAccount: userAuthProcedure.mutation(async ({ ctx }) => {
        const { prisma: prisma2, user } = ctx
        try {
            await prisma2.user.delete({ where: { id: user.id } })
            return { message: "Your account has been deleted successfully" }
        } catch (error) {
            throw new import_server11.TRPCError({
                message: "Failed to delete your account",
                code: "INTERNAL_SERVER_ERROR",
            })
        }
    }),
    deleteAddress: userAuthProcedure
        .input(deleteAddressSchema)
        .mutation(async ({ ctx, input }) => {
            const { id } = input
            const { prisma: prisma2 } = ctx
            try {
                const address = await prisma2.addresses.findFirst({
                    where: { id },
                })
                if (address == null ? void 0 : address.isDefault)
                    throw new import_server11.TRPCError({
                        message: "You cannot delete your default address",
                        code: "FORBIDDEN",
                    })
                await prisma2.addresses.delete({ where: { id } })
                return { message: "Your address has been deleted" }
            } catch (error) {
                throw new import_server11.TRPCError({
                    message: "Failed to delete your address",
                    code: "INTERNAL_SERVER_ERROR",
                })
            }
        }),
    editAddress: userAuthProcedure
        .input(editAddressSchema)
        .mutation(async ({ ctx, input }) => {
            const { city, country, id, state, street, lat, lng } = input
            const { prisma: prisma2 } = ctx
            try {
                const address = await prisma2.addresses.update({
                    where: { id },
                    data: { city, country, state, street, lat, lng },
                })
                return {
                    message: "Your address has successfully been updated",
                    address,
                }
            } catch (error) {
                throw new import_server11.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to update your address",
                })
            }
        }),
    editProfile: userAuthProcedure
        .input(userProfileSchema)
        .mutation(async ({ ctx, input }) => {
            const { fullName, phone } = input
            try {
                await ctx.prisma.user.update({
                    where: { id: ctx.user.id },
                    data: { fullName, phone },
                })
                return { message: "Your profile has been updated successfully" }
            } catch (error) {
                throw new import_server11.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to update your profile",
                })
            }
        }),
})

// src/food/index.ts
var foodRouter = router({
    auth: authRouter2,
    basket: basketRouter,
    main: mainRouter,
    order: orderRouter,
    profile: profileRouter2,
})

// src/restaurant/auth/index.ts
var import_crypto3 = require("crypto")
var import_server12 = require("@trpc/server")
var authRouter3 = router({
    changePassword: procedure
        .input(changePasswordSchema)
        .mutation(async ({ ctx, input }) => {
            const { email } = input
            let { password } = input
            password = await encryptPassword(password)
            try {
                await ctx.prisma.restaurant.update({
                    where: { email },
                    data: {
                        restaurantAuth: {
                            update: { password, OTP: null, OTPExpires: null },
                        },
                    },
                })
                return { message: "Your password has been changed" }
            } catch (error) {
                throw new import_server12.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Your new password was not saved",
                })
            }
        }),
    confirmOTP: procedure
        .input(confirmOTPSchema)
        .mutation(async ({ ctx, input }) => {
            var _a
            const { email, otp } = input
            try {
                const restaurant = await ctx.prisma.restaurant.findUnique({
                    where: { email },
                    include: { restaurantAuth: true },
                })
                if (
                    !restaurant ||
                    !((_a = restaurant.restaurantAuth) == null
                        ? void 0
                        : _a.OTP) ||
                    !restaurant.restaurantAuth.OTPExpires
                )
                    throw new import_server12.TRPCError({
                        code: "BAD_REQUEST",
                        message: "OTP Error",
                    })
                const hasExpired = hasOtpExpired(
                    restaurant.restaurantAuth.OTPExpires
                )
                if (hasExpired)
                    throw new import_server12.TRPCError({
                        code: "UNAUTHORIZED",
                        message: "OTP has expired, please generate a new one",
                    })
                const otpMatches =
                    restaurant.restaurantAuth.OTP.toString() === otp
                if (!otpMatches)
                    throw new import_server12.TRPCError({
                        code: "UNAUTHORIZED",
                        message:
                            "OTP does not match, please check the values provided",
                    })
                return true
            } catch (error) {
                throw new import_server12.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "OTP error",
                })
            }
        }),
    emailExist: procedure
        .input(emailExistSchema)
        .mutation(async ({ ctx, input }) => {
            try {
                const emailUsed = await ctx.prisma.restaurant.findUnique({
                    where: { email: input.email },
                })
                return { emailUsed: !!emailUsed }
            } catch (error) {
                throw new import_server12.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to login",
                })
            }
        }),
    login: procedure.input(loginSchema).mutation(async ({ input, ctx }) => {
        try {
            const restaurant = await ctx.prisma.restaurant.findFirst({
                where: { email: input.email },
            })
            if (!restaurant)
                throw new import_server12.TRPCError({
                    code: "UNAUTHORIZED",
                    message: "Wrong Email or Password",
                })
            const restaurantAuth = await ctx.prisma.restaurantAuth.findUnique({
                where: { restaurantId: restaurant.id },
            })
            if (!restaurantAuth || !restaurantAuth.password)
                throw new import_server12.TRPCError({
                    code: "NOT_FOUND",
                    message: "Wrong Email or Password",
                })
            const passwordMatches = await comparePasswords(
                input.password,
                restaurantAuth.password
            )
            if (!passwordMatches)
                throw new import_server12.TRPCError({
                    code: "NOT_FOUND",
                    message: "Wrong Email or Password",
                })
            const token = signJwt({ id: restaurant.id })
            return { ...restaurant, token }
        } catch (error) {
            throw new import_server12.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to login",
            })
        }
    }),
    loginOauth: procedure
        .input(oauthSchema)
        .mutation(async ({ ctx, input }) => {
            const { prisma: prisma2 } = ctx
            const { email } = input
            try {
                const restaurant = await prisma2.restaurant.findFirst({
                    where: { email },
                })
                if (!restaurant)
                    throw new import_server12.TRPCError({
                        code: "NOT_FOUND",
                        message: "An account with this email was not found",
                    })
                const token = signJwt({ id: restaurant.id })
                return { ...restaurant, token }
            } catch (error) {
                throw new import_server12.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to login",
                })
            }
        }),
    sendOTP: procedure.input(sendOTPSchema).mutation(async ({ ctx, input }) => {
        const { email } = input
        const restaurant = await ctx.prisma.restaurant.findUnique({
            where: { email },
        })
        if (restaurant) {
            const OTP = generateOTP()
            try {
                await ctx.prisma.restaurantAuth.update({
                    where: {
                        restaurantId: restaurant.id,
                    },
                    data: {
                        OTP,
                        OTPExpires: otpExpires(),
                    },
                })
                await recoverPasswordMail({
                    from: "Mealy Restaurant",
                    to: restaurant.email,
                    otp: OTP,
                })
            } catch (error) {
                throw new import_server12.TRPCError({
                    code: "BAD_REQUEST",
                    message: "Failed to generate OTP",
                })
            }
        }
    }),
    signUp: procedure
        .input(restaurantRegisterSchema)
        .mutation(async ({ ctx, input }) => {
            const {
                city,
                country,
                email,
                userFullName,
                password,
                phone,
                state,
                street,
                lat,
                lng,
                logo,
                name,
                alwaysOpen,
                closingTime,
                openingTime,
            } = input
            const { prisma: prisma2 } = ctx
            try {
                const encryptedPassword = await encryptPassword(password)
                const restaurant = await prisma2.restaurant.create({
                    data: {
                        city,
                        country,
                        email,
                        phone,
                        state,
                        street,
                        lat,
                        lng,
                        userFullName,
                        logo,
                        name,
                        alwaysOpen,
                        closingTime,
                        openingTime,
                        ratings: 0,
                        restaurantAuth: {
                            create: { password: encryptedPassword },
                        },
                    },
                })
                const token = signJwt({ id: restaurant.id })
                return { ...restaurant, token }
            } catch (error) {
                throw new import_server12.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to create your account",
                })
            }
        }),
    signUpOauth: procedure
        .input(restaurantRegisterOauth)
        .mutation(async ({ ctx, input }) => {
            const {
                city,
                country,
                email,
                userFullName,
                phone,
                state,
                street,
                lat,
                lng,
                logo,
                name,
                alwaysOpen,
                closingTime,
                openingTime,
            } = input
            const { prisma: prisma2 } = ctx
            try {
                const randomString = (0, import_crypto3.randomBytes)(16)
                const password = await encryptPassword(
                    randomString.toString("hex")
                )
                const restaurant = await prisma2.restaurant.create({
                    data: {
                        city,
                        country,
                        email,
                        phone,
                        state,
                        street,
                        lat,
                        lng,
                        userFullName,
                        logo,
                        name,
                        alwaysOpen,
                        closingTime,
                        openingTime,
                        ratings: 0,
                        restaurantAuth: { create: { password } },
                    },
                })
                const token = signJwt({ id: restaurant.id })
                return { ...restaurant, token }
            } catch (error) {
                throw new import_server12.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to create your account",
                })
            }
        }),
})

// src/restaurant/customers/index.ts
var customersRouter = router({
    allCustomers: restaurantAuthProcedure
        .input(allCustomersSchema)
        .query(async ({ ctx, input }) => {
            const { prisma: prisma2, restaurant } = ctx
            const { page, limit } = input
            const customers = await prisma2.user.findMany({
                where: { orders: { some: { restaurantId: restaurant.id } } },
                select: {
                    createdAt: true,
                    fullName: true,
                    addresses: {
                        where: { isDefault: true },
                        select: {
                            city: true,
                            country: true,
                            state: true,
                            street: true,
                        },
                    },
                    orders: {
                        orderBy: { createdAt: "desc" },
                        where: { restaurantId: restaurant.id },
                        select: { createdAt: true, totalPrice: true },
                    },
                },
            })
            const formattedCustomers = customers.map((customer) => {
                const { addresses, createdAt, fullName, orders } = customer
                const { city, country, state, street } = addresses[0]
                const totalSpending = +orders
                    .reduce((prev, curr) => prev + curr.totalPrice, 0)
                    .toFixed(2)
                const location = `${street} ${city} ${state} ${country}`
                return {
                    totalSpending,
                    location,
                    joinDate: createdAt,
                    name: fullName,
                    lastOrder: orders[0],
                }
            })
            formattedCustomers.sort((a, b) => {
                const first = a.lastOrder.createdAt
                const second = b.lastOrder.createdAt
                if (first < second) return 1
                if (first > second) return -1
                return 0
            })
            const currentPage = page - 1
            const customerPerPage = formattedCustomers.slice(
                currentPage * limit,
                page * limit
            )
            const total = formattedCustomers.length
            const numberOfPages = Math.ceil(total / limit)
            return { customerPerPage, numberOfPages, total }
        }),
})

// ../../packages/db/src/seed/index.ts
var seed_exports = {}
__export(seed_exports, {
    allCouriers: () => allCouriers,
    allFood: () => allFood,
    allFoodOrdered: () => allFoodOrdered,
    allOrders: () => allOrders,
    allRatings: () => allRatings,
    allRestaurants: () => allRestaurants,
    allUserRatings: () => allUserRatings,
    allUsers: () => allUsers,
    averageRestaurantRating: () => averageRestaurantRating,
    clearData: () => clearData,
    drink: () => drink,
    getDeliveryFee: () => getDeliveryFee,
    getTotalPrice: () => getTotalPrice,
    meal: () => meal,
    randomFloatBy2_5: () => randomFloatBy2_5,
    randomItem: () => randomItem,
    randomNumber: () => randomNumber,
    randomNumber1N0: () => randomNumber1N0,
    restaurantData: () => restaurantData,
    shuffleAndReduce: () => shuffleAndReduce,
    shuffleArray: () => shuffleArray,
    subtractAYear: () => subtractAYear,
})

// ../../packages/db/src/seed/clearData.ts
var clearData_exports = {}
__export(clearData_exports, {
    clearData: () => clearData,
})
__reExport(clearData_exports, require("@prisma/client"))
var clearData = async (prisma2) => {
    await prisma2.restaurant.deleteMany({})
    await prisma2.courier.deleteMany({})
    await prisma2.user.deleteMany({})
    await prisma2.images.deleteMany({})
}

// ../../packages/db/src/seed/index.ts
__reExport(seed_exports, clearData_exports)

// ../../packages/db/src/seed/courier.ts
var import_faker = require("@faker-js/faker")

// ../../packages/db/src/seed/location.ts
var import_random_location = __toESM(require("random-location"))

// ../../packages/db/src/seed/env.ts
var z13 = __toESM(require("zod"))
var formatErrors2 = (errors) =>
    Object.entries(errors)
        .map(([name, value]) => {
            if (value && "_errors" in value)
                return `${name}: ${value._errors.join(", ")}
`
            return
        })
        .filter(Boolean)
var variables2 = z13.object({
    DATABASE_URL: z13.string().url(),
    RANDOM_LAT: z13.string(),
    RANDOM_LNG: z13.string(),
})
var getVariables2 = variables2.safeParse(process.env)
if (!getVariables2.success) {
    console.error(
        "Environment Variables missing:\n",
        ...formatErrors2(getVariables2.error.format())
    )
    throw new Error("Invalid Environment Variables")
}
var env2 = getVariables2.data

// ../../packages/db/src/seed/location.ts
var { RANDOM_LAT, RANDOM_LNG } = env2
var range = 1e4
var getRandomLocation = (data) => {
    const location = {
        latitude: parseFloat(RANDOM_LAT),
        longitude: parseFloat(RANDOM_LNG),
    }
    return import_random_location.default.randomCircumferencePoint(
        data ?? location,
        range
    )
}

// ../../packages/db/src/seed/courier.ts
var allCouriers = async (password, prisma2) =>
    await Promise.all(
        Array.from({ length: 5 }, async (_, i) => {
            const name = import_faker.faker.name.fullName()
            const { latitude, longitude } = getRandomLocation()
            const courier = await prisma2.courier.create({
                data: {
                    city: "ikeja",
                    country: "nigeria",
                    state: "lagos",
                    street: import_faker.faker.address.streetAddress(),
                    phone: import_faker.faker.phone.number(),
                    fullName: name,
                    email:
                        i === 0
                            ? "test@mail.com"
                            : import_faker.faker.internet.email(name),
                    lat: latitude,
                    lng: longitude,
                    createdAt: subtractAYear(/* @__PURE__ */ new Date()),
                    courierAuth: { create: { password } },
                    isDummyAccount: { create: { isDummy: true } },
                },
            })
            return courier
        })
    )

// ../../packages/db/src/seed/data.ts
var restaurantData = [
    {
        name: "Eatery works",
        logo: "/image/upload/v1670077269/Mealy/logo/logo5_antpcr.png",
    },
    {
        name: "Feast Palace",
        logo: "/image/upload/v1670077269/Mealy/logo/logo8_s3fz9x.png",
    },
    {
        name: "Deli Divine",
        logo: "/image/upload/v1670077268/Mealy/logo/logo4_gir0vo.png",
    },
    {
        name: "Grub lord",
        logo: "/image/upload/v1670077267/Mealy/logo/logo2_zzg8yb.png",
    },
    {
        name: "Island Grill",
        logo: "/image/upload/v1670077267/Mealy/logo/logo3_zkcxdh.png",
    },
    {
        name: "Pancake World",
        logo: "/image/upload/v1670077267/Mealy/logo/logo7_mbz4rt.png",
    },
    {
        name: "Munchies",
        logo: "/image/upload/v1670077267/Mealy/logo/logo6_rnywea.png",
    },
    {
        name: "Mediterra Seafood",
        logo: "/image/upload/v1670077267/Mealy/logo/logo1_rvjrvm.png",
    },
]
var meal = [
    {
        name: "Timmy Pizza",
        price: 13.5,
        type: "Food",
        description:
            "This infused arrowroot stew is a shade slimy with a crispy texture. It is balanced with chia with cicely and has a large amount of sorrel. It smells foul with limes. It is piquant and spicy. You can really feel how high in vitamin B and how all natural it is.",
        image: "/image/upload/v1669934272/Mealy/food/timmy_pizza_nvj4yy.png",
    },
    {
        name: "Beefy Burger",
        price: 33,
        type: "Food",
        description:
            "This charred grapes eggroll is thoroughly honeyed with a delicate texture. It is similar to hemp seeds with cotula and has a touch of laurel. It smells fusty with tapioca. It is peculiarly grateful. You can really feel how locally sourced and how paleo-friendly it is.",
        image: "/image/upload/v1669934273/Mealy/food/wepik-photo-mode-2022927-145744_llitra.png",
    },
    {
        name: "Full Shrimps",
        price: 25,
        type: "Food",
        description:
            "This seared soybeans quiche is barely blackened with a crunchy texture. It resembles corn (maize) with garlic and has a great deal of chamomile. It smells like a cemetery with a bit of chrysanthemum leaves. It is decidedly literary. You can really feel how naturally sourced and how certified organic it is.",
        image: "/image/upload/v1669934272/Mealy/food/full_shrimps_a5pupe.png",
    },
    {
        name: "Spagetti with Tomato Stew",
        price: 15,
        type: "Food",
        description:
            "This fermented butternut squash lasagna is a shade syrupy with a hearty texture. It has an undertone of kumquat with aralia and has an overwhelming amount of hibiscus. It smells savory with an overwhelming amount of melon balls. It is pleasantly seductive. You can really feel how high fiber and how free trade it is.",
        image: "/image/upload/v1669934271/Mealy/food/spagetti_with_tomato_stew_evpyay.png",
    },
    {
        name: "Jollof Rice with Beef",
        price: 23,
        type: "Food",
        description:
            "This sauteed ac\xE9rola tapas is sort of dense with a silky texture. It bursts with the flavor of mulberries with southernwood and has a great deal of coriander-blair. It smells like licorice with just the right amount of prairie turnips. It is slightly scorched. You can really feel how no artificial colors and how vegan it is.",
        image: "/image/upload/v1669934272/Mealy/food/jollof_rice_with_beef_kx2tde.png",
    },
    {
        name: "Lunch Combo",
        price: 40,
        type: "Food",
        description:
            "This breaded pistachios hummus is a touch fluffy with a gooey texture. It is enhanced by pearl millet with lemon balm and has cumin. It smells like cotton candy with a touch of sugar-apples. It is healthy and hearty. You can really feel how gluten free and how third party tested it is.",
        image: "/image/upload/v1669934271/Mealy/food/wepik-photo-mode-2022927-145125_lonzng.png",
    },
    {
        name: "Macaroni Delight",
        price: 17.5,
        type: "Food",
        description:
            "This smoked iceberg lettuce eggroll is utterly juicy with a burned texture. It has an undertone of pomme with star anise and has a large amount of dill. It smells rank with a lot of peaches. It is vaguely Moorish. You can really feel how organic and how whole food it is.",
        image: "/image/upload/v1669934270/Mealy/food/wepik-photo-mode-2022927-145916_b649ab.png",
    },
    {
        name: "Full Chicken With Stew",
        price: 13.5,
        type: "Food",
        description:
            "This fried sugar snap peas paella is sort of electric with a dense texture. It is infused with black-eyed peas with southernwood and has arnika. It smells like acetone with a dash of peanuts. It is bitter and acrid. You can really feel how high in calcium and how antioxidant rich it is.",
        image: "/image/upload/v1669934270/Mealy/food/full_chicken_with_stew_lwe9so.png",
    },
    {
        name: "Veggies Buffet",
        price: 35,
        type: "Food",
        description:
            "This sauteed broccoli lunch is a shade grainy with a silky texture. It builds depth and complexity with cannellini beans with black pepper and has a touch of cardamom. It smells like a hospital with cherimoya. It is metallic and medicinal. You can really feel how fresh cut and how nutrient rich it is.",
        image: "/image/upload/v1669934270/Mealy/food/veggies_buffet_nxy2ep.png",
    },
    {
        name: "Pasta Frivoli",
        price: 11,
        type: "Food",
        description:
            "This whipped buckwheat quesadilla is quite blackened with an airy texture. It is redolent with blackberries with chamomile and has a hint of horseradish. It smells like perfume with pokeberry shoots. It is particularly new. You can really feel how high in probiotics and how antioxidant rich it is.",
        image: "/image/upload/v1669933261/Mealy/food/pasta-frivoli_jssrig.png",
    },
    {
        name: "Chocolate Pancakes",
        price: 25.5,
        type: "Food",
        description:
            "This blackened onion roast is a little sugary with a syrupy texture. It tastes strongly of sweet red pepper with mullein and has a dash of tarragon. It smells acrid with an overwhelming amount of nuts. It is strongly literary. You can really feel how high in omega 3s and how locally sourced it is.",
        image: "/image/upload/v1669933261/Mealy/food/chocolate_pancake_njtvf1.png",
    },
    {
        name: "Seasoned Stake",
        price: 26,
        type: "Food",
        description:
            "This blackened finger millet taco is wholly mushy with a crispy texture. It has characteristics of olives with bee balm and has angelica. It smells ambrosial with a dash of butterbur. It is especially luscious. You can really feel how organic and how third party tested it is.",
        image: "/image/upload/v1669933260/Mealy/food/seasoned_stake_lbskbo.png",
    },
    {
        name: "Potato Soup",
        price: 20,
        type: "Food",
        description:
            "This charred guava brunch is sort of hearty with a gooey texture. It embodies the essence of wheat with savory and has a dash of cuban oregano. It smells like scented soap with a great deal of vermicelli. It is racy and delicious. You can really feel how high in omega 3s and how all natural it is.",
        image: "/image/upload/v1669933259/Mealy/food/potato_soup_d8iekj.png",
    },
    {
        name: "Pork Stew",
        price: 13.5,
        type: "Food",
        description:
            "This broiled Asian rice stew is scarcely mushy with a dense texture. It has a tinge of papaya with scullcap and has cinnamon. It smells like it is decomposing with a hint of balsam-pear (bitter gourd). It is faintly sweet. You can really feel how all natural and how high in magnesium it is.",
        image: "/image/upload/v1669933259/Mealy/food/pork_stew_czgzuz.jpg",
    },
    {
        name: "Bread and Egg Breakfast",
        price: 13.5,
        type: "Food",
        description:
            "This fermented quinoa dessert is altogether hearty with a succulent texture. It is similar to sorana beans with rosemary and has a dash of bayberry. It smells delicate with a dash of fireweed. It is syrupy sweet. You can really feel how high in probiotics and how high in iron it is.",
        image: "/image/upload/v1669933258/Mealy/food/bread_and_egg_breakfast_hw78sy.png",
    },
    {
        name: "Dumplins",
        price: 16,
        type: "Food",
        description:
            "This breaded mangosteen eggroll is absolutely intoxicating with a sticky texture. It has an undertone of carrot with paprika and has mugwort. It smells like mold with a lot of jew's ear. It is has variety. You can really feel how premium and how high in riboflavin it is",
        image: "/image/upload/v1669933257/Mealy/food/dumplins_w5rqdj.jpg",
    },
    {
        name: "Noodles and Shrimp",
        price: 13.5,
        type: "Food",
        description:
            "This breaded Asian rice wrap is comparatively honeyed with a buttery texture. It compares to pomme with hyssop and has a touch of rue. It smells like chicken broth with a lot of melon balls. It is slightly minerally. You can really feel how paleo-friendly and how high in probiotics it is.",
        image: "/image/upload/v1669822848/Mealy/food/noodles_and_shrimp_vothmr.jpg",
    },
    {
        name: "Freevolies",
        price: 10,
        type: "Food",
        description:
            "This blanched banana appetizer is a little velvety with a fizzy texture. It has characteristics of finger millet with nettle and has just the right amount of celery seed. It smells clean with a large amount of ginger root. It is truly German. You can really feel how high in omega 3s and how fresh cut it is.",
        image: "/image/upload/v1669933258/Mealy/food/freevolies_lrdxtw.jpg",
    },
    {
        name: "Grilled Ribs",
        price: 35,
        type: "Food",
        description:
            "This glazed apple wrap is totally succulent with a tough texture. It is redolent with goji berries with mace and has an overwhelming amount of dill. It smells squalid with a tiny bit of longans. It is pleasantly sweet. You can really feel how third party tested and how high in healthy fats it is.",
        image: "/image/upload/v1669822847/Mealy/food/grilled_ribs_djpixw.jpg",
    },
    {
        name: "Meat and Veggie Bowl",
        price: 30.5,
        type: "Food",
        description:
            "This marinated black-eyed peas sushi is fairly soft with an intoxicating texture. It is infused with adzuki beans with mullein and has just the right amount of arnika. It smells like maple sugar with just the right amount of plums. It is acrid and pungent. You can really feel how fresh cut and how soy free it is.",
        image: "/image/upload/v1669822847/Mealy/food/meat_and_veggie_bowl_ety9di.jpg",
    },
]
var drink = [
    {
        name: "Chocolate Milkshake",
        price: 18,
        type: "Drink",
        description: "A very rich in content chocolate milkshake",
        image: "/image/upload/v1669935762/Mealy/drink/chocolate_milkshake_m3omy9.png",
    },
    {
        name: "Lemonade",
        price: 10,
        type: "Drink",
        description: "Lemonade drink made with 100% lemon",
        image: "/image/upload/v1669935760/Mealy/drink/lemonade_b2n3bp.png",
    },
    {
        name: "Creme crame",
        price: 15,
        type: "Drink",
        description: "Cold creammy malt",
        image: "/image/upload/v1669935758/Mealy/drink/creme_crame_bfwcpz.png",
    },
    {
        name: "Coca cola",
        price: 13,
        type: "Drink",
        description: "A coca cola drink",
        image: "/image/upload/v1669935758/Mealy/drink/coca_cola_g7alx5.png",
    },
    {
        name: "Coffee",
        price: 15,
        type: "Drink",
        description: "Coffee drink to boost energy",
        image: "/image/upload/v1669935760/Mealy/drink/lemonade_b2n3bp.png",
    },
    {
        name: "Iced Tea",
        price: 16,
        type: "Drink",
        description: "Chilled ice tea",
        image: "/image/upload/v1669935756/Mealy/drink/iced_tea_wjzfi8.png",
    },
    {
        name: "Orange Juice",
        price: 20,
        type: "Drink",
        description: "Freshly squeezed orange juice",
        image: "/image/upload/v1669935756/Mealy/drink/orange_juice_kxaubc.png",
    },
    {
        name: "Chupa Chups",
        price: 12,
        type: "Drink",
        description: "Chupa chubs flavored drink",
        image: "/image/upload/v1669935755/Mealy/drink/chupa_chups_xfezk5.png",
    },
    {
        name: "Mundet",
        price: 15,
        type: "Drink",
        description: "Cool mundet drink",
        image: "/image/upload/v1669935755/Mealy/drink/mundet_bk6ed9.png",
    },
    {
        name: "Fanta",
        price: 14,
        type: "Drink",
        description: "Chilled fanta drink",
        image: "/image/upload/v1669935755/Mealy/drink/fanta_gick5i.png",
    },
    {
        name: "Tomato Juice",
        price: 11,
        type: "Drink",
        description: "Freshly squeezed tomato",
        image: "/image/upload/v1669822847/Mealy/drink/tomato_juice_wg7cfn.jpg",
    },
]

// ../../packages/db/src/seed/food.ts
var allFood = async (restaurants, prisma2) => {
    const foods = []
    const discountPercentages = [5, 10, 15, 20, 25, 30, 35, 40]
    await Promise.all(
        // loop through all restaurants
        restaurants.map(async (restaurant) => {
            const restaurantFoods = [
                ...shuffleAndReduce(meal, 10),
                ...shuffleAndReduce(drink, 5),
            ]
            await Promise.all(
                // add all foods and drinks to data base
                restaurantFoods.map(async (restaurantFood) => {
                    let discountPercentage = 0
                    let discountPrice = 0
                    const addDiscount = randomNumber() === 1
                    if (addDiscount) {
                        const discountPercent = randomItem(discountPercentages)
                        discountPercentage = discountPercent
                        const discount = parseFloat(
                            (
                                restaurantFood.price *
                                (discountPercent / 100)
                            ).toFixed(2)
                        )
                        discountPrice = +(
                            restaurantFood.price - discount
                        ).toFixed(2)
                    }
                    const food = await prisma2.food.create({
                        data: {
                            description: restaurantFood.description,
                            image: restaurantFood.image,
                            name: restaurantFood.name,
                            price: restaurantFood.price,
                            type: restaurantFood.type,
                            restaurantId: restaurant.id,
                            discountPercentage,
                            discountPrice,
                            createdAt: new Date(2022, 1, 1, 0, 0, 0),
                        },
                    })
                    foods.push(food)
                })
            )
        })
    )
    return foods
}

// ../../packages/db/src/seed/order.ts
var import_faker2 = require("@faker-js/faker")
var allOrders = async (restaurants, couriers, users, prisma2) => {
    const orders = []
    await Promise.all(
        // loop through all restaurants created
        restaurants.map(async (restaurant) => {
            return await Promise.all(
                Array.from({ length: 100 }, async () => {
                    const userId = randomItem(users).id
                    const UserAddress = await prisma2.addresses.findFirst({
                        where: { userId, isDefault: true },
                    })
                    let deliveryAddress = ""
                    if (UserAddress) {
                        const { city, country, street, state } = UserAddress
                        deliveryAddress = `${street}, ${city}, ${state}, ${country}`
                    }
                    const order = await prisma2.order.create({
                        data: {
                            isPaid: true,
                            courierId: randomItem(couriers).id,
                            restaurantId: restaurant.id,
                            payment: randomItem(["Card", "Ondelivery"]),
                            status: "Delivered",
                            userId,
                            deliveryAddress,
                            deliveryLat:
                                (UserAddress == null
                                    ? void 0
                                    : UserAddress.lat) ?? 0,
                            deliveryLng:
                                (UserAddress == null
                                    ? void 0
                                    : UserAddress.lng) ?? 0,
                            deliveryFee: 0,
                            totalPriceWithDelivery: 0,
                            totalPrice: 0,
                            createdAt: import_faker2.faker.date.between(
                                subtractAYear(/* @__PURE__ */ new Date()),
                                Date.now()
                            ),
                        },
                    })
                    orders.push(order)
                })
            )
        })
    )
    return orders
}
var allFoodOrdered = async (orders, foods, prisma2) => {
    await Promise.all(
        orders.map(async (order) => {
            const n = randomNumber(8)
            const foodFromRestaurant = foods.filter(
                (food) => food.restaurantId === order.restaurantId
            )
            const restaurantFoods = shuffleArray(foodFromRestaurant)
            const orderItems = await Promise.all(
                Array.from({ length: n }, async (_, i) => {
                    const food = restaurantFoods[i]
                    const numOfFood = randomNumber(3)
                    let priceUsed = food.price
                    const discountPrice = food.discountPrice
                    if (discountPrice && discountPrice > 0)
                        priceUsed = discountPrice
                    return await prisma2.foodOrdered.create({
                        data: {
                            quantity: numOfFood,
                            orderId: order.id,
                            foodTotalPrice: numOfFood * priceUsed,
                            orderDiscountPercentage:
                                food.discountPercentage ?? 0,
                            orderDiscountPrice: food.discountPrice ?? 0,
                            orderPrice: food.price ?? 0,
                            foodId: food.id,
                            createdAt: order.createdAt,
                        },
                    })
                })
            )
            await prisma2.order.update({
                where: {
                    id: order.id,
                },
                data: {
                    totalPrice: getTotalPrice(orderItems),
                    totalPriceWithDelivery:
                        getTotalPrice(orderItems) + getDeliveryFee(orderItems),
                    deliveryFee: getDeliveryFee(orderItems),
                },
            })
        })
    )
}

// ../../packages/db/src/seed/ratings.ts
var import_faker3 = require("@faker-js/faker")
var allUserRatings = async (users, restaurants, prisma2) => {
    await Promise.all(
        users.map(async (user) => {
            await Promise.all(
                restaurants.map(async (restaurant) => {
                    const showComment = randomNumber1N0()
                    const sentenceNumber = Math.round(Math.random() * 5) + 2
                    let comment
                    if (showComment)
                        comment =
                            import_faker3.faker.lorem.sentences(sentenceNumber)
                    const date = import_faker3.faker.date.between(
                        subtractAYear(/* @__PURE__ */ new Date()),
                        Date.now()
                    )
                    await prisma2.restaurantRatings.create({
                        data: {
                            user: { connect: { id: user.id } },
                            restaurant: { connect: { id: restaurant.id } },
                            ratings: randomFloatBy2_5(),
                            comment,
                            createdAt: date,
                            updatedAt: date,
                        },
                    })
                })
            )
        })
    )
}
var allRatings = async (prisma2, defaultRest) => {
    let restaurants
    if (defaultRest) {
        restaurants = [defaultRest]
    } else {
        restaurants = await prisma2.restaurant.findMany({
            include: { userRatings: true },
        })
    }
    await Promise.all(
        restaurants.map(async (restaurant) => {
            await prisma2.restaurant.update({
                where: { id: restaurant.id },
                data: {
                    ratings: averageRestaurantRating(restaurant.userRatings),
                },
            })
        })
    )
}

// ../../packages/db/src/seed/restaurant.ts
var import_faker4 = require("@faker-js/faker")
var allRestaurants = async (password, shuffleRestaurants, prisma2) =>
    await Promise.all(
        Array.from({ length: 5 }, async (_, i) => {
            const { latitude, longitude } = getRandomLocation()
            const restaurant = await prisma2.restaurant.create({
                data: {
                    name: shuffleRestaurants[i].name,
                    city: "ikeja",
                    country: "nigeria",
                    state: "lagos",
                    street: import_faker4.faker.address.streetAddress(),
                    phone: import_faker4.faker.phone.number(),
                    userFullName: import_faker4.faker.name.fullName(),
                    closingTime: "05:00 PM",
                    openingTime: "09:00 AM",
                    alwaysOpen: i === 0,
                    ratings: 0,
                    email:
                        i === 0
                            ? "test@mail.com"
                            : import_faker4.faker.internet.email(
                                  shuffleRestaurants[i].name
                              ),
                    lat: latitude,
                    lng: longitude,
                    logo: shuffleRestaurants[i].logo,
                    createdAt: subtractAYear(/* @__PURE__ */ new Date()),
                    restaurantAuth: { create: { password } },
                },
            })
            return restaurant
        })
    )

// ../../packages/db/src/seed/user.ts
var import_faker5 = require("@faker-js/faker")
var allUsers = async (password, prisma2) =>
    await Promise.all(
        Array.from({ length: 21 }, async (_, i) => {
            const { latitude, longitude } = getRandomLocation()
            const first_name = import_faker5.faker.name.firstName()
            const last_name = import_faker5.faker.name.lastName()
            const user = await prisma2.user.create({
                data: {
                    fullName: `${first_name} ${last_name}`,
                    email:
                        i === 0
                            ? "test@mail.com"
                            : import_faker5.faker.internet
                                  .email(first_name, last_name)
                                  .toLowerCase(),
                    phone: import_faker5.faker.phone.number(),
                    createdAt: subtractAYear(/* @__PURE__ */ new Date()),
                    addresses: {
                        create: {
                            city: "ikeja",
                            country: "nigeria",
                            state: "lagos",
                            street: import_faker5.faker.address.streetAddress(),
                            isDefault: true,
                            lat: latitude,
                            lng: longitude,
                        },
                    },
                    userAuth: {
                        create: {
                            password,
                        },
                    },
                    isDummyAccount: { create: { isDummy: true } },
                },
            })
            return user
        })
    )

// ../../packages/db/src/seed/utils.ts
var shuffleArray = (array) => {
    for (let i = 0; i < array.length; i++) {
        const j = Math.round(Math.random() * (1 + i))
        const temp = array[i]
        array[i] = array[j]
        array[j] = temp
    }
    return array
}
var shuffleAndReduce = (array, n) => shuffleArray(array).slice(0, n)
var randomItem = (array) => array[Math.floor(Math.random() * array.length)]
var randomNumber = (n) => Math.floor(Math.random() * (n || 5)) + 1
var randomNumber1N0 = () => Math.round(Math.random() * 10)
var randomFloatBy2_5 = (n) => {
    let rating = Math.floor(Math.random() * (n || 5)) + 2
    if (rating > 5) rating = 5
    return rating
}
var averageRestaurantRating = (array) =>
    parseFloat(
        (
            array.reduce((prev, curr) => prev + curr.ratings, 0) / array.length
        ).toFixed(1)
    )
var subtractAYear = (date) => {
    const dateCopy = new Date(date)
    dateCopy.setFullYear(date.getFullYear() - 1)
    return dateCopy
}
var getTotalPrice = (data) =>
    parseFloat(
        data.reduce((prev, curr) => prev + curr.foodTotalPrice, 0).toFixed(2)
    )
var getDeliveryFee = (data) =>
    Math.floor(data.reduce((prev, curr) => prev + curr.foodTotalPrice, 0) * 0.1)

// src/restaurant/dashboard/index.ts
var import_server13 = require("@trpc/server")

// src/utils/dates.ts
var import_dayjs = __toESM(require("dayjs"))
var betweenDates = (period) => {
    if (period === "Today") {
        const endOfDay = (0, import_dayjs.default)().endOf("day").toDate()
        const startOfDay = (0, import_dayjs.default)().startOf("day").toDate()
        return { startOfDay, endOfDay }
    }
    if (period === "This Week") {
        const endOfDay = (0, import_dayjs.default)().endOf("day").toDate()
        const startOfDay = (0, import_dayjs.default)()
            .startOf("week")
            .startOf("day")
            .toDate()
        return { startOfDay, endOfDay }
    }
    if (period === "This Month") {
        const endOfDay = (0, import_dayjs.default)().endOf("day").toDate()
        const startOfDay = (0, import_dayjs.default)()
            .startOf("month")
            .startOf("day")
            .toDate()
        return { startOfDay, endOfDay }
    }
    if (period === "This Year") {
        const endOfDay = (0, import_dayjs.default)()
            .endOf("year")
            .endOf("day")
            .toDate()
        const startOfDay = (0, import_dayjs.default)()
            .startOf("year")
            .startOf("day")
            .toDate()
        return { startOfDay, endOfDay }
    }
    if (period === "Last Year") {
        const endOfDay = (0, import_dayjs.default)()
            .subtract(1, "year")
            .endOf("year")
            .endOf("day")
            .toDate()
        const startOfDay = (0, import_dayjs.default)()
            .subtract(1, "year")
            .startOf("year")
            .startOf("day")
            .toDate()
        return { startOfDay, endOfDay }
    }
    return void 0
}

// src/restaurant/dashboard/index.ts
var dashboardRouter = router({
    getData: restaurantAuthProcedure
        .input(dashboardDataSchema)
        .query(async ({ ctx, input }) => {
            const { prisma: prisma2, restaurant } = ctx
            const { period } = input
            const dates = betweenDates(period)
            const orderFilter = {
                gte: dates == null ? void 0 : dates.startOfDay,
                lte: dates == null ? void 0 : dates.endOfDay,
            }
            const restaurantData2 = await prisma2.restaurant.findFirst({
                where: { id: restaurant.id },
                select: {
                    orders: {
                        where: {
                            createdAt: orderFilter,
                        },
                        select: {
                            totalPrice: true,
                            createdAt: true,
                            userId: true,
                        },
                    },
                    _count: {
                        select: {
                            foods: true,
                            orders: {
                                where: {
                                    createdAt: orderFilter,
                                },
                            },
                        },
                    },
                },
            })
            if (!restaurantData2)
                throw new import_server13.TRPCError({
                    code: "NOT_FOUND",
                    message: "Restaurant details not found",
                })
            let totalRevenue = 0
            let uniqueCustomers = []
            const revenueAndCustomers = restaurantData2.orders.reduce(
                (prev, curr) => {
                    const { totalPrice, userId, createdAt } = curr
                    const month = createdAt.getMonth()
                    totalRevenue = +(totalRevenue + totalPrice).toFixed(2)
                    uniqueCustomers.push(userId)
                    const newData = { ...prev }
                    const revenue = newData.revenue[month]
                    const customers = newData.customers[month]
                    if (revenue)
                        newData.revenue[month] = +(
                            revenue + totalPrice
                        ).toFixed(2)
                    else newData.revenue[month] = totalPrice
                    if (customers) newData.customers[month] += 1
                    else newData.customers[month] = 1
                    return newData
                },
                { revenue: [], customers: [] }
            )
            const middleData = {
                ...revenueAndCustomers,
                total: {
                    revenue: totalRevenue,
                    customers: uniqueCustomers.length,
                },
            }
            uniqueCustomers = [...new Set(uniqueCustomers)]
            const topData = {
                totalOrders: restaurantData2._count.orders,
                totalRevenue,
                uniqueCustomers: uniqueCustomers.length,
                totalMenu: restaurantData2._count.foods,
            }
            const allTrending = await prisma2.food.findMany({
                orderBy: { foodOrdered: { _count: "desc" } },
                include: {
                    _count: {
                        select: {
                            foodOrdered: { where: { createdAt: orderFilter } },
                        },
                    },
                },
                where: { restaurantId: restaurant.id },
            })
            const trending = allTrending
                .sort((a, b) => b._count.foodOrdered - a._count.foodOrdered)
                .slice(0, 4)
            const orderStatusGroup = await prisma2.order.groupBy({
                by: ["status"],
                where: { restaurantId: restaurant.id, createdAt: orderFilter },
                _count: { status: true },
            })
            const orderStatus = orderStatusGroup.reduce(
                (prev, curr) => {
                    const { _count, status: status2 } = curr
                    if (
                        status2 === "Accepted" ||
                        status2 === "Cooking" ||
                        status2 === "New" ||
                        status2 === "ReadyForPickUp"
                    ) {
                        return {
                            ...prev,
                            Current: prev.Current + _count.status,
                        }
                    }
                    return { ...prev, [status2]: prev[status2] + _count.status }
                },
                { Current: 0, OnRoute: 0, Delivered: 0, Rejected: 0 }
            )
            const bottomData = { trending, orderStatus }
            return {
                middleData,
                bottomData,
                topData,
            }
        }),
    randomData: restaurantAuthProcedure.mutation(async ({ ctx }) => {
        const { prisma: prisma2, restaurant } = ctx
        if (restaurant.usedData)
            throw new import_server13.TRPCError({
                code: "NOT_FOUND",
                message: "This restaurant has already used the dummy data",
            })
        const restaurants = [restaurant]
        try {
            await prisma2.$transaction(async (prx) => {
                const users = await prx.user.findMany({
                    where: { isDummyAccount: { isDummy: true } },
                })
                const couriers = await prx.courier.findMany({
                    where: { isDummyAccount: { isDummy: true } },
                })
                const foods = await allFood(restaurants, prx)
                const orders = await allOrders(
                    restaurants,
                    couriers,
                    users,
                    prx
                )
                await allFoodOrdered(orders, foods, prx)
                await allUserRatings(users, restaurants, prx)
                const newRestaurant = await prx.restaurant.findFirst({
                    where: { id: restaurant.id },
                    include: { userRatings: true },
                })
                if (newRestaurant) {
                    await allRatings(prx, newRestaurant)
                    await prx.restaurant.update({
                        where: { id: restaurant.id },
                        data: { usedData: true },
                    })
                } else
                    throw new import_server13.TRPCError({
                        code: "INTERNAL_SERVER_ERROR",
                        message: "Failed to use dummy data",
                    })
            })
            return { message: "Dummy data has successfully been used" }
        } catch (error) {
            throw new import_server13.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to use dummy data",
            })
        }
    }),
})

// src/restaurant/location/index.ts
var import_axios2 = __toESM(require("axios"))
var import_server14 = require("@trpc/server")
var { MAP_BOX_TOKEN: MAP_BOX_TOKEN2 } = env
var locationRouter2 = router({
    getGeocode: procedure.input(gpsSchema).mutation(async ({ input }) => {
        const { city, country, state, street } = input
        const address = encodeURI(`${street} ${city} ${state} ${country}`)
        try {
            const { data } = await import_axios2.default.get(
                `https://api.mapbox.com/geocoding/v5/mapbox.places/${address}.json?access_token=${MAP_BOX_TOKEN2}`
            )
            const [lat, lng] = data.features[0].geometry.coordinates
            const { latitude, longitude } = getRandomLocation({
                latitude: lat,
                longitude: lng,
            })
            return { lat: latitude, lng: longitude }
        } catch (error) {
            throw new import_server14.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to fetch restaurant location",
            })
        }
    }),
})

// src/restaurant/menu/index.ts
var import_server15 = require("@trpc/server")
var menuRouter = router({
    allMenu: restaurantAuthProcedure
        .input(allMenuSchema)
        .query(async ({ ctx, input }) => {
            const { prisma: prisma2, restaurant } = ctx
            const { page, limit } = input
            const currentPage = page - 1
            const total = await prisma2.food.count({
                where: { restaurantId: restaurant.id },
            })
            const menu = await prisma2.food.findMany({
                where: { restaurantId: restaurant.id },
                orderBy: [{ updatedAt: "desc" }],
                take: limit,
                skip: currentPage * limit,
            })
            const numberOfPages = Math.ceil(total / limit)
            return { menu, total, numberOfPages }
        }),
    createMenu: restaurantAuthProcedure
        .input(createMenuSchema)
        .mutation(async ({ ctx, input }) => {
            const {
                description,
                discountPercentage,
                image,
                name,
                price,
                type,
            } = input
            const { prisma: prisma2, restaurant } = ctx
            let discountPrice = 0
            if (discountPercentage) {
                const discount = +((price * discountPercentage) / 100).toFixed(
                    2
                )
                discountPrice = +(price - discount).toFixed(2)
            }
            try {
                await prisma2.food.create({
                    data: {
                        description,
                        image,
                        name,
                        price,
                        type,
                        discountPercentage,
                        discountPrice,
                        restaurantId: restaurant.id,
                    },
                })
                return { message: "Your menu has been created successfully" }
            } catch (error) {
                throw new import_server15.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to create your menu",
                })
            }
        }),
    deleteMenu: restaurantAuthProcedure
        .input(deleteMenuSchema)
        .mutation(async ({ ctx, input }) => {
            const { prisma: prisma2 } = ctx
            const { id } = input
            if (!id)
                throw new import_server15.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to delete menu item",
                })
            try {
                await prisma2.food.delete({ where: { id } })
                return { message: "Menu has been deleted successfully" }
            } catch (error) {
                throw new import_server15.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to delete menu item",
                })
            }
        }),
    editMenu: restaurantAuthProcedure
        .input(editMenuSchema)
        .mutation(async ({ ctx, input }) => {
            const {
                description,
                discountPercentage,
                id,
                image,
                name,
                price,
                type,
            } = input
            const { prisma: prisma2 } = ctx
            let discountPrice = 0
            if (discountPercentage) {
                const discount = +((price * discountPercentage) / 100).toFixed(
                    2
                )
                discountPrice = +(price - discount).toFixed(2)
            }
            try {
                await prisma2.food.update({
                    where: { id },
                    data: {
                        name,
                        description,
                        discountPercentage,
                        discountPrice,
                        image,
                        price,
                        type,
                    },
                })
                return { message: "Menu has been updated successfully" }
            } catch (error) {
                throw new import_server15.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to create your menu",
                })
            }
        }),
    getMenu: restaurantAuthProcedure
        .input(getMenuSchema)
        .query(async ({ ctx, input }) => {
            const { prisma: prisma2 } = ctx
            const { id } = input
            if (!id)
                throw new import_server15.TRPCError({
                    code: "NOT_FOUND",
                    message: "The menu you requested for does not exist",
                })
            try {
                const menu = await prisma2.food.findFirst({ where: { id } })
                if (!menu)
                    throw new import_server15.TRPCError({
                        code: "NOT_FOUND",
                        message: "The menu you requested for does not exist",
                    })
                return menu
            } catch (error) {
                throw new import_server15.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed get your menu",
                })
            }
        }),
})

// src/restaurant/orders/index.ts
var import_server16 = require("@trpc/server")
var import_observable2 = require("@trpc/server/observable")
var ordersRouter2 = router({
    changeOrderStatus: restaurantAuthProcedure
        .input(changeOrderStatusSchema)
        .mutation(async ({ ctx, input }) => {
            const { id, status: status2 } = input
            const { prisma: prisma2, ee: ee2, restaurant } = ctx
            try {
                const order = await prisma2.order.update({
                    where: { id },
                    data: { status: status2 },
                })
                if (status2 === "Accepted") {
                    await sendPushMessage({
                        pushToken: order.pushToken ?? "",
                        orderId: order.id,
                        title: restaurant.name,
                        body: `Your order has been accepted`,
                    })
                    return { message: "Order has been accepted" }
                }
                if (status2 === "Rejected") {
                    await sendPushMessage({
                        pushToken: order.pushToken ?? "",
                        orderId: order.id,
                        title: restaurant.name,
                        body: `Your order has been rejected`,
                    })
                    return { message: "Order has been rejected" }
                }
                if (status2 === "Cooking") {
                    await sendPushMessage({
                        pushToken: order.pushToken ?? "",
                        orderId: order.id,
                        title: restaurant.name,
                        body: `Your order is cooking`,
                    })
                    return { message: "Order has been marked as cooking" }
                }
                if (status2 === "ReadyForPickUp") {
                    await sendPushMessage({
                        pushToken: order.pushToken ?? "",
                        orderId: order.id,
                        title: restaurant.name,
                        body: `Your order is ready`,
                    })
                    ee2.emit(
                        "ORDER_READY",
                        restaurant.state,
                        restaurant.country
                    )
                    return { message: "Order has been made ready for pick up" }
                }
                return { message: "Order status has been updated" }
            } catch (error) {
                throw new import_server16.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to update the order",
                })
            }
        }),
    getAllOrders: restaurantAuthProcedure
        .input(allOrdersRestaurantSchema)
        .query(async ({ ctx, input }) => {
            const { limit, period, status: status2, page } = input
            const { prisma: prisma2, restaurant } = ctx
            const dates = betweenDates(period)
            const currentPage = page - 1
            const total = await prisma2.order.count({
                where: {
                    restaurantId: restaurant.id,
                    status: status2 ?? void 0,
                    createdAt: dates
                        ? { gte: dates.startOfDay, lte: dates.endOfDay }
                        : void 0,
                },
            })
            const orders = await prisma2.order.findMany({
                where: {
                    restaurantId: restaurant.id,
                    status: status2 ?? void 0,
                    createdAt: dates
                        ? { gte: dates.startOfDay, lte: dates.endOfDay }
                        : void 0,
                },
                include: { user: { select: { fullName: true } } },
                take: limit + 1,
                skip: currentPage * limit,
                orderBy: { createdAt: "desc" },
            })
            const numberOfPages = Math.ceil(total / limit)
            return { orders, numberOfPages, total }
        }),
    getOrderDetails: restaurantAuthProcedure
        .input(orderDetailsSchema)
        .query(async ({ ctx, input }) => {
            const { id } = input
            const { prisma: prisma2 } = ctx
            try {
                const order = await prisma2.order.findFirst({
                    where: { id },
                    include: {
                        courier: { select: { fullName: true, phone: true } },
                        foodOrdered: {
                            include: {
                                food: { select: { image: true, name: true } },
                            },
                        },
                        user: { select: { fullName: true, phone: true } },
                    },
                })
                if (!order)
                    throw new import_server16.TRPCError({
                        code: "NOT_FOUND",
                        message: "The order you are looking for does not exist",
                    })
                return order
            } catch (error) {
                throw new import_server16.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to get your header",
                })
            }
        }),
    newOrder: procedure
        .input(restaurantIdSchema)
        .subscription(({ ctx, input }) => {
            const { ee: ee2 } = ctx
            const { id } = input
            return (0, import_observable2.observable)((emit) => {
                const send = (restaurantId, orderId) => {
                    if (id === restaurantId)
                        emit.next({ message: "You have a new order", orderId })
                }
                ee2.on("ORDER_CREATED", send)
                return () => {
                    ee2.off("ORDER_CREATED", send)
                }
            })
        }),
})

// src/restaurant/review/index.ts
var import_server17 = require("@trpc/server")
var reviewRouter = router({
    getAllReviews: restaurantAuthProcedure
        .input(allRestaurantReviewsSchema)
        .query(async ({ ctx, input }) => {
            const { prisma: prisma2, restaurant } = ctx
            const { limit, rating, page } = input
            const currentPage = page - 1
            try {
                const reviews = await prisma2.restaurantRatings.findMany({
                    where: {
                        restaurantId: restaurant.id,
                        ratings: rating ?? void 0,
                    },
                    include: { user: { select: { fullName: true } } },
                    take: limit,
                    skip: currentPage * limit,
                    orderBy: [{ updatedAt: "desc" }],
                })
                const reviewsCount = await prisma2.restaurantRatings.count({
                    where: {
                        restaurantId: restaurant.id,
                        ratings: rating ?? void 0,
                    },
                })
                const numberOfPages = Math.ceil(reviewsCount / limit)
                return { reviews, reviewsCount, numberOfPages }
            } catch (error) {
                throw new import_server17.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to get all reviews",
                })
            }
        }),
})

// src/restaurant/settings/index.ts
var import_server18 = require("@trpc/server")
var settingsRouter = router({
    deleteAccount: restaurantAuthProcedure.mutation(async ({ ctx }) => {
        const { prisma: prisma2, restaurant } = ctx
        try {
            await prisma2.restaurant.delete({ where: { id: restaurant.id } })
            return { message: "Your account has been deleted" }
        } catch (error) {
            throw new import_server18.TRPCError({
                code: "INTERNAL_SERVER_ERROR",
                message: "Failed to delete your account",
            })
        }
    }),
    updateAccount: restaurantAuthProcedure
        .input(restaurantDetailsSchema)
        .mutation(async ({ ctx, input }) => {
            const {
                alwaysOpen,
                city,
                country,
                lat,
                lng,
                logo,
                name,
                phone,
                state,
                street,
                userFullName,
                closingTime,
                openingTime,
            } = input
            const { prisma: prisma2, restaurant } = ctx
            try {
                await prisma2.restaurant.update({
                    where: { id: restaurant.id },
                    data: {
                        alwaysOpen,
                        city,
                        country,
                        logo,
                        lat,
                        lng,
                        name,
                        phone,
                        state,
                        street,
                        userFullName,
                        closingTime,
                        openingTime,
                    },
                })
                return { message: "Your account has successfully been updated" }
            } catch (error) {
                throw new import_server18.TRPCError({
                    code: "INTERNAL_SERVER_ERROR",
                    message: "Failed to update your account",
                })
            }
        }),
})

// src/restaurant/index.ts
var restaurantRouter = router({
    auth: authRouter3,
    customers: customersRouter,
    dashboard: dashboardRouter,
    location: locationRouter2,
    menu: menuRouter,
    orders: ordersRouter2,
    review: reviewRouter,
    settings: settingsRouter,
})

// src/index.ts

var app = (0, import_express.default)()
app.use(import_express.default.json())
app.use((0, import_cors.default)())
var server = import_http.default.createServer(app)
var wss = new import_ws.default.Server({ server })
var appRouter = router({
    courier: courierRouter,
    food: foodRouter,
    home: procedure.query(() => ({ message: "Welcome to Mealy" })),
    image: imageRouter,
    restaurant: restaurantRouter,
})
var handler = (0, import_ws2.applyWSSHandler)({
    wss,
    router: appRouter,
    createContext,
})
wss.on("connection", (wSock) => {
    console.log(`++ Connection (${wss.clients.size})`)
    wSock.once("close", () => {
        console.log(`-- Connection (${wss.clients.size})`)
    })
})
process.on("SIGTERM", () => {
    handler.broadcastReconnectNotification()
    wss.close()
})
if (process.env.NODE_ENV !== "production")
    app.use((0, import_morgan.default)("dev"))
app.get("/", (_req, res) => {
    res.status(200).json({ message: "Welcome to Mealy" })
})
app.use(
    "/api/trpc",
    trpcExpress.createExpressMiddleware({ router: appRouter, createContext })
)
app.post("/api/image/upload", upload.single("image"), imageUploader)
server.listen(env.PORT, () => {
    console.log(`api is listening on port ${env.PORT}`)
})
// Annotate the CommonJS export names for ESM import in node:
0 &&
    (module.exports = {
        appRouter,
    })
