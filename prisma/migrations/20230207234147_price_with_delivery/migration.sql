/*
  Warnings:

  - Added the required column `totalPriceWithDelivery` to the `Order` table without a default value. This is not possible if the table is not empty.

*/
-- AlterEnum
ALTER TYPE "Status" ADD VALUE 'Rejected';

-- AlterTable
ALTER TABLE "Order" ADD COLUMN     "totalPriceWithDelivery" DOUBLE PRECISION NOT NULL;
