-- CreateTable
CREATE TABLE "UserDummyAccount" (
    "id" TEXT NOT NULL,
    "isDummy" BOOLEAN NOT NULL,
    "userId" TEXT NOT NULL,

    CONSTRAINT "UserDummyAccount_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CourierDummyAccount" (
    "id" TEXT NOT NULL,
    "isDummy" BOOLEAN NOT NULL,
    "courierId" TEXT NOT NULL,

    CONSTRAINT "CourierDummyAccount_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "UserDummyAccount_userId_key" ON "UserDummyAccount"("userId");

-- CreateIndex
CREATE UNIQUE INDEX "CourierDummyAccount_courierId_key" ON "CourierDummyAccount"("courierId");

-- AddForeignKey
ALTER TABLE "UserDummyAccount" ADD CONSTRAINT "UserDummyAccount_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CourierDummyAccount" ADD CONSTRAINT "CourierDummyAccount_courierId_fkey" FOREIGN KEY ("courierId") REFERENCES "Courier"("id") ON DELETE CASCADE ON UPDATE CASCADE;
