/*
  Warnings:

  - The `lat` column on the `Addresses` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - The `lng` column on the `Addresses` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - The `lat` column on the `Courier` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - The `lng` column on the `Courier` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - The `lat` column on the `Restaurant` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - The `lng` column on the `Restaurant` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "Addresses" DROP COLUMN "lat",
ADD COLUMN     "lat" DOUBLE PRECISION,
DROP COLUMN "lng",
ADD COLUMN     "lng" DOUBLE PRECISION;

-- AlterTable
ALTER TABLE "Courier" DROP COLUMN "lat",
ADD COLUMN     "lat" DOUBLE PRECISION,
DROP COLUMN "lng",
ADD COLUMN     "lng" DOUBLE PRECISION;

-- AlterTable
ALTER TABLE "Restaurant" DROP COLUMN "lat",
ADD COLUMN     "lat" DOUBLE PRECISION,
DROP COLUMN "lng",
ADD COLUMN     "lng" DOUBLE PRECISION;
