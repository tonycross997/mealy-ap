/*
  Warnings:

  - You are about to drop the `_BasketFoodToFood` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_BasketFoodToFood" DROP CONSTRAINT "_BasketFoodToFood_A_fkey";

-- DropForeignKey
ALTER TABLE "_BasketFoodToFood" DROP CONSTRAINT "_BasketFoodToFood_B_fkey";

-- AlterTable
ALTER TABLE "BasketFood" ADD COLUMN     "foodId" TEXT;

-- DropTable
DROP TABLE "_BasketFoodToFood";

-- AddForeignKey
ALTER TABLE "BasketFood" ADD CONSTRAINT "BasketFood_foodId_fkey" FOREIGN KEY ("foodId") REFERENCES "Food"("id") ON DELETE SET NULL ON UPDATE CASCADE;
