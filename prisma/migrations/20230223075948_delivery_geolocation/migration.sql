/*
  Warnings:

  - Added the required column `deliveryLat` to the `Order` table without a default value. This is not possible if the table is not empty.
  - Added the required column `deliveryLng` to the `Order` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Order" ADD COLUMN     "deliveryLat" DOUBLE PRECISION NOT NULL,
ADD COLUMN     "deliveryLng" DOUBLE PRECISION NOT NULL;
