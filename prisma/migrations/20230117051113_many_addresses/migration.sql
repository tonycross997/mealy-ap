/*
  Warnings:

  - You are about to drop the column `userId` on the `Addresses` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Addresses" DROP CONSTRAINT "Addresses_userId_fkey";

-- DropIndex
DROP INDEX "Addresses_userId_key";

-- AlterTable
ALTER TABLE "Addresses" DROP COLUMN "userId";

-- CreateTable
CREATE TABLE "_AddressesToUser" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_AddressesToUser_AB_unique" ON "_AddressesToUser"("A", "B");

-- CreateIndex
CREATE INDEX "_AddressesToUser_B_index" ON "_AddressesToUser"("B");

-- AddForeignKey
ALTER TABLE "_AddressesToUser" ADD CONSTRAINT "_AddressesToUser_A_fkey" FOREIGN KEY ("A") REFERENCES "Addresses"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_AddressesToUser" ADD CONSTRAINT "_AddressesToUser_B_fkey" FOREIGN KEY ("B") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
