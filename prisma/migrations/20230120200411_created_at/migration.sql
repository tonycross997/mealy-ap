/*
  Warnings:

  - You are about to drop the `_RestaurantRatingsToUser` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_RestaurantRatingsToUser" DROP CONSTRAINT "_RestaurantRatingsToUser_A_fkey";

-- DropForeignKey
ALTER TABLE "_RestaurantRatingsToUser" DROP CONSTRAINT "_RestaurantRatingsToUser_B_fkey";

-- AlterTable
ALTER TABLE "RestaurantRatings" ADD COLUMN     "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
ADD COLUMN     "userId" TEXT;

-- DropTable
DROP TABLE "_RestaurantRatingsToUser";

-- AddForeignKey
ALTER TABLE "RestaurantRatings" ADD CONSTRAINT "RestaurantRatings_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;
