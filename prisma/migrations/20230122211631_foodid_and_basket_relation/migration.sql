/*
  Warnings:

  - Made the column `foodId` on table `BasketFood` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "BasketFood" DROP CONSTRAINT "BasketFood_foodId_fkey";

-- DropIndex
DROP INDEX "Basket_restaurantId_key";

-- DropIndex
DROP INDEX "Basket_userId_key";

-- AlterTable
ALTER TABLE "BasketFood" ALTER COLUMN "foodId" SET NOT NULL;

-- AddForeignKey
ALTER TABLE "BasketFood" ADD CONSTRAINT "BasketFood_foodId_fkey" FOREIGN KEY ("foodId") REFERENCES "Food"("id") ON DELETE CASCADE ON UPDATE CASCADE;
