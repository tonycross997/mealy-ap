/*
  Warnings:

  - Added the required column `updatedAt` to the `RestaurantRatings` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "RestaurantRatings" ADD COLUMN     "updatedAt" TIMESTAMP(3) NOT NULL;
