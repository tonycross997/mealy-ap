/*
  Warnings:

  - You are about to drop the `_AddressesToUser` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_RestaurantToRestaurantRatings` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `userId` to the `Addresses` table without a default value. This is not possible if the table is not empty.
  - Added the required column `restaurantId` to the `RestaurantRatings` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "_AddressesToUser" DROP CONSTRAINT "_AddressesToUser_A_fkey";

-- DropForeignKey
ALTER TABLE "_AddressesToUser" DROP CONSTRAINT "_AddressesToUser_B_fkey";

-- DropForeignKey
ALTER TABLE "_RestaurantToRestaurantRatings" DROP CONSTRAINT "_RestaurantToRestaurantRatings_A_fkey";

-- DropForeignKey
ALTER TABLE "_RestaurantToRestaurantRatings" DROP CONSTRAINT "_RestaurantToRestaurantRatings_B_fkey";

-- AlterTable
ALTER TABLE "Addresses" ADD COLUMN     "userId" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "RestaurantRatings" ADD COLUMN     "restaurantId" TEXT NOT NULL;

-- DropTable
DROP TABLE "_AddressesToUser";

-- DropTable
DROP TABLE "_RestaurantToRestaurantRatings";

-- AddForeignKey
ALTER TABLE "Addresses" ADD CONSTRAINT "Addresses_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "RestaurantRatings" ADD CONSTRAINT "RestaurantRatings_restaurantId_fkey" FOREIGN KEY ("restaurantId") REFERENCES "Restaurant"("id") ON DELETE CASCADE ON UPDATE CASCADE;
