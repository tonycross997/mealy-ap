/*
  Warnings:

  - You are about to drop the `_FoodToFoodOrdered` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `deliveryFee` to the `Order` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "_FoodToFoodOrdered" DROP CONSTRAINT "_FoodToFoodOrdered_A_fkey";

-- DropForeignKey
ALTER TABLE "_FoodToFoodOrdered" DROP CONSTRAINT "_FoodToFoodOrdered_B_fkey";

-- AlterTable
ALTER TABLE "FoodOrdered" ADD COLUMN     "foodId" TEXT;

-- AlterTable
ALTER TABLE "Order" ADD COLUMN     "deliveryFee" INTEGER NOT NULL;

-- DropTable
DROP TABLE "_FoodToFoodOrdered";

-- AddForeignKey
ALTER TABLE "FoodOrdered" ADD CONSTRAINT "FoodOrdered_foodId_fkey" FOREIGN KEY ("foodId") REFERENCES "Food"("id") ON DELETE SET NULL ON UPDATE CASCADE;
