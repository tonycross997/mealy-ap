/*
  Warnings:

  - Made the column `lat` on table `Addresses` required. This step will fail if there are existing NULL values in that column.
  - Made the column `lng` on table `Addresses` required. This step will fail if there are existing NULL values in that column.
  - Made the column `lat` on table `Courier` required. This step will fail if there are existing NULL values in that column.
  - Made the column `lng` on table `Courier` required. This step will fail if there are existing NULL values in that column.
  - Made the column `lat` on table `Restaurant` required. This step will fail if there are existing NULL values in that column.
  - Made the column `lng` on table `Restaurant` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "Addresses" ALTER COLUMN "lat" SET NOT NULL,
ALTER COLUMN "lng" SET NOT NULL;

-- AlterTable
ALTER TABLE "Courier" ALTER COLUMN "lat" SET NOT NULL,
ALTER COLUMN "lng" SET NOT NULL;

-- AlterTable
ALTER TABLE "Restaurant" ALTER COLUMN "lat" SET NOT NULL,
ALTER COLUMN "lng" SET NOT NULL;
