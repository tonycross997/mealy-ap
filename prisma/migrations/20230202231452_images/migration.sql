-- CreateEnum
CREATE TYPE "ImageType" AS ENUM ('Logo', 'Food', 'Drink');

-- CreateEnum
CREATE TYPE "ImageCategory" AS ENUM ('Logo', 'Foods');

-- CreateTable
CREATE TABLE "Images" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "type" "ImageType" NOT NULL,
    "category" "ImageCategory" NOT NULL,
    "link" TEXT NOT NULL,

    CONSTRAINT "Images_pkey" PRIMARY KEY ("id")
);
